﻿using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Infrastructure.Interfaces;


namespace FPT_Alumni.Infrastructure.Repositories
{
    public class NewsCategoryRepository : BaseRepository<NewsCategory>, INewsCategoryRepository
    {
        public NewsCategoryRepository(IDBContext dbContext) : base(dbContext)
        {
        }

    }
}
