﻿using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Infrastructure.Repositories
{
    public class AlumniFieldRepository : BaseRepository<AlumniField>, IAlumniFieldRepository
    {
        private readonly IDBContext _dBContext;
        public AlumniFieldRepository(IDBContext dbContext) : base(dbContext)
        {
            _dBContext = dbContext;
        }
    }
}
