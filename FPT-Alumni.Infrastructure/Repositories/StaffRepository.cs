﻿using Dapper;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Infrastructure.Interfaces;
using FPT_Alumni.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Services
{
    public class StaffRepository : BaseRepository<Staff>, IStaffRepository
    {

        private readonly IDBContext _dBContext;
        public StaffRepository(IDBContext dbContext) : base(dbContext)
        {
            _dBContext = dbContext;
        }

        public async Task<List<StaffWithUserModelView>> GetStaffListAsync()
        {

            var sql = @"SELECT u.Id as UserId, u.MainEmail, u.Password, u.RoleId, r.RoleName,  a.FullName,  a.Campus 
                         FROM [User] u
                      left JOIN Staff  a ON u.Id = a.UserId 
                      JOIN Role r ON u.RoleId = r.Id 
                        where r.RoleName = N'Staff' and u.Status != 'DELETED' ";

            var result = await _dBContext.Connection.QueryAsync<StaffWithUserModelView>(sql);

            return result!.ToList();
        }

        public async Task<List<StaffWithUserModelView>> GetStaffsByStatusAsync(string? status)
        {
            var sql = @"SELECT u.Id as UserId, u.MainEmail, u.Password, u.RoleId, r.RoleName,  a.FullName,  a.Campus 
                         FROM [User] u
                      left JOIN Staff  a ON u.Id = a.UserId 
                      JOIN Role r ON u.RoleId = r.Id 
                        where r.RoleName = 'Staff' and u.Status = @StatusValue";

            var parameters = new { StatusValue = status };

            var result = await _dBContext.Connection.QueryAsync<StaffWithUserModelView>(sql, parameters, _dBContext.Transaction);

            return result!.ToList();
        }

        public async Task<int> UpdateStaffByUserIdAsync(Staff updateStaffDto)
        {
            var updateFields = new List<string>();
            var parameters = new DynamicParameters();

            void AddParameterIfNotNull<T>(string fieldName, T value)
            {
                if (value != null)
                {
                    updateFields.Add($"{fieldName} = @{fieldName}Value");
                    parameters.Add($"{fieldName}Value", value);
                }
            }

            AddParameterIfNotNull("FullName", updateStaffDto.FullName);
            AddParameterIfNotNull("Campus", updateStaffDto.Campus);


            if (updateFields.Count == 0)
            {
                return -1; // Không có trường nào cần cập nhật
            }

            parameters.Add("UserIdValue", updateStaffDto.UserId);

            var updateSql = $"UPDATE Staff SET {string.Join(", ", updateFields)} WHERE UserId = @UserIdValue";

            var result = await _dBContext.Connection.ExecuteAsync(updateSql, parameters, _dBContext.Transaction);

            return result;
        }

        public async Task<int> DeleteStaffByUserIdAsync(Guid userId)
        {
            string sql = @"DELETE FROM Staff WHERE UserId = @UserIdValue";

            var parameters = new { UserIdValue = userId };

            var result = await _dBContext.Connection.ExecuteAsync(sql, parameters, _dBContext.Transaction);

            return result;
        }

        public async Task<UserModelView> GetStaffByUserIdAsync(Guid userId)
        {
            var sql = @"SELECT u.Id as UserId, u.MainEmail, u.Password, u.RoleId, r.RoleName,  a.FullName, a.Campus 
                         FROM [User] u
                      left JOIN Staff  a ON u.Id = a.UserId 
                      JOIN Role r ON u.RoleId = r.Id 
                     WHERE r.RoleName = 'Staff' and u.Id = @UserId";

            var parameters = new { UserId = userId };
            var result = await _dBContext.Connection.QueryFirstOrDefaultAsync<UserModelView>(sql, parameters, _dBContext.Transaction);


            return result;
        }

        public async Task<int> AddAlumniAsync(Staff createAlumniDto)
        {
            var sql = @"INSERT INTO Alumni (Id, FullName, Campus, UserId)
                        VALUES (@Id, @FullName, @Campus, @UserId)";

            var parameters = new
            {
                createAlumniDto.Id,
                createAlumniDto.FullName,
                createAlumniDto.Campus,
                createAlumniDto.UserId
            };

            var result = await _dBContext.Connection.ExecuteAsync(sql, parameters, _dBContext.Transaction);
            return result;
        }
    }
}
