﻿using Dapper;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Infrastructure.Repositories
{
    public class PostFiledRepository : BaseRepository<PostField>, IPostFiledRepository
    {
        private readonly IDBContext _dBContext;
        public PostFiledRepository(IDBContext dbContext) : base(dbContext)
        {
            _dBContext = dbContext;
        }

        public async Task<int> DeleteByPostIdAsync(Guid postId)
        {
            string sql = @" DELETE FROM PostField WHERE PostId = @postId";
            var parameters = new { postId = postId };
            var res = await _dBContext.Connection.ExecuteAsync(sql, parameters, _dBContext.Transaction);
            return res;
        }
    }
}
