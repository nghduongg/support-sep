﻿using Dapper;
using FPT_Alumni.Core.Constants;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Enums;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Infrastructure.Repositories
{
    public class PostCategoryRepository : BaseRepository<PostCategory>, IPostCategoryRepository
    {
        private readonly IDBContext _dBContext;
        public PostCategoryRepository(IDBContext dbContext) : base(dbContext)
        {
            _dBContext = dbContext;
        }



        public async Task<List<PostCategory>> GetAllBaseViewModeAsync(ViewModeEnum viewMode)
        {
            string sql = "";
            switch (viewMode)
            {
                case ViewModeEnum.ALUMNI_VIEW:
                    sql = @"SELECT * FROM  [PostCategory] WHERE Id != @statusId ";
                    break;
                case ViewModeEnum.STAFF_VIEW:
                    sql = @"SELECT * FROM [PostCategory]";
                    break;
            }
            var parameters = new { statusId = PostCategoryConstant.STATUS };
            var data = await _dBContext.Connection.QueryAsync<PostCategory>(sql, parameters, _dbContext.Transaction);
            return data.ToList();
        }
    }
}
