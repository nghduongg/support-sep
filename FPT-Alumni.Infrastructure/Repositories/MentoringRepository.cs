﻿using Dapper;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Infrastructure.Repositories
{
    public class MentoringRepository : BaseRepository<Mentoring>, IMentoringRepository
    {
        private readonly IDBContext _dBContext;
        public MentoringRepository(IDBContext dbContext) : base(dbContext)
        {
            _dBContext = dbContext;
        }

        public async Task<int> SaveMentoringFieldsAsync(Guid mentoringId, List<Guid> fields)
        {
            var deleteSql = "DELETE FROM MentoringField WHERE MentoringId = @MentoringId";
            var deleteParameters = new DynamicParameters();
            deleteParameters.Add("MentoringId", mentoringId);

            await _dbContext.Connection.ExecuteAsync(deleteSql, deleteParameters, _dBContext.Transaction);


            var insertSql = new StringBuilder("INSERT INTO MentoringField (MentoringId, FieldId) VALUES ");
            var insertParameters = new DynamicParameters();

            for (int i = 0; i < fields.Count; i++)
            {
                insertSql.Append($"(@MentoringId, @FieldId{i}),");
                insertParameters.Add($"FieldId{i}", fields[i]);
            }

            insertSql.Length--; // Remove the last comma
            insertParameters.Add("MentoringId", mentoringId);

            var result = await _dbContext.Connection.ExecuteAsync(insertSql.ToString(), insertParameters, _dBContext.Transaction);
            return result;
        }


        public async Task<int> SaveMentoringAsync(Mentoring mentoring, string? action)
        {
            var updateFields = new List<string>();
            var insertFields = new List<string>();
            var insertValues = new List<string>();
            var parameters = new DynamicParameters();

            void AddParameterIfNotNullOrEmpty<T>(string fieldName, T value)
            {
                if (value is string strValue)
                {
                    if (!string.IsNullOrEmpty(strValue))
                    {
                        updateFields.Add($"{fieldName} = @{fieldName}");
                        insertFields.Add(fieldName);
                        insertValues.Add($"@{fieldName}");
                        parameters.Add(fieldName, value);
                    }
                }
                else if (value != null)
                {
                    updateFields.Add($"{fieldName} = @{fieldName}");
                    insertFields.Add(fieldName);
                    insertValues.Add($"@{fieldName}");
                    parameters.Add(fieldName, value);
                }
            }

            AddParameterIfNotNullOrEmpty("UserId", mentoring.UserId);
            AddParameterIfNotNullOrEmpty("BasicInformation", mentoring.BasicInformation);
            AddParameterIfNotNullOrEmpty("CurrentCountry", mentoring.CurrentCountry);
            AddParameterIfNotNullOrEmpty("CurrentCity", mentoring.CurrentCity);
            AddParameterIfNotNullOrEmpty("WorkAndEducation", mentoring.WorkAndEducation);
            AddParameterIfNotNullOrEmpty("OtherPreferences", mentoring.OtherPreferences);
            AddParameterIfNotNullOrEmpty("MentoringRole", mentoring.MentoringRole);

            string sql;

            if (action == "insert")
            {
                if (insertFields.Count == 0)
                {
                    throw new ArgumentException("No valid fields to insert.");
                }
                sql = $"INSERT INTO Mentoring (Id, {string.Join(", ", insertFields)}) VALUES (@Id, {string.Join(", ", insertValues)})";
                parameters.Add("Id", mentoring.Id);
            }
            else if (action == "update" && mentoring.Id != Guid.Empty)
            {
                if (updateFields.Count == 0)
                {
                    throw new ArgumentException("No valid fields to update.");
                }
                sql = $"UPDATE Mentoring SET {string.Join(", ", updateFields)} WHERE Id = @Id";
                parameters.Add("Id", mentoring.Id);
            }
            else
            {
                throw new ArgumentException("Invalid action specified or missing Id for update operation.");
            }

            var result = await _dBContext.Connection.ExecuteAsync(sql, parameters, _dBContext.Transaction);
            return result;
        }


        public async Task<Mentoring> GetMentoringByIdAsync(Guid Id)
        {
            var sql = "SELECT * FROM Mentoring WHERE Id = @Id ";
            var parameters = new DynamicParameters();
            parameters.Add("Id", Id);

            var mentoring = await _dBContext.Connection.QueryFirstOrDefaultAsync<Mentoring>(sql, parameters);
            return mentoring!;
        }

        public async Task<int> DeleteMentoringFieldsByMentoringId(Guid id)
        {
            var sql = "DELETE FROM MentoringField WHERE MentoringId = @MentoringId";
            var parameters = new DynamicParameters();
            parameters.Add("MentoringId", id);

            var result = await _dBContext.Connection.ExecuteAsync(sql, parameters, _dBContext.Transaction);
            return result;
        }

        public async Task<List<ConnectionAndWorking>> GetTypesByMentoringId(Guid id)
        {
            var sql = @"
        SELECT mt.*
        FROM MentoringConnectionAndWorking mbt
        INNER JOIN ConnectionAndWorking mt ON mt.Id = mbt.ConnectionAndWorkingId
        WHERE mbt.MentoringId = @MentoringId";

            var parameters = new DynamicParameters();
            parameters.Add("MentoringId", id);

            var types = await _dBContext.Connection.QueryAsync<ConnectionAndWorking>(sql, parameters, _dBContext.Transaction);
            return types.ToList();
        }

        public async Task<List<Language>> GetLanguagesByMentoringId(Guid id)
        {
            var sql = @"
        SELECT ls.*
        FROM MentoringLanguage ml
        INNER JOIN Language ls ON ls.Id = ml.LanguageId
        WHERE ml.MentoringId = @MentoringId";

            var parameters = new DynamicParameters();
            parameters.Add("MentoringId", id);

            var types = await _dBContext.Connection.QueryAsync<Language>(sql, parameters, _dBContext.Transaction);
            return types.ToList();
        }

        public async Task<int> SaveMentoringTypesAsync(Guid id, List<Guid> mentoringTypes)
        {
            var deleteSql = "DELETE FROM MentoringConnectionAndWorking WHERE MentoringId = @MentoringId";
            var deleteParameters = new DynamicParameters();
            deleteParameters.Add("MentoringId", id);

            await _dbContext.Connection.ExecuteAsync(deleteSql, deleteParameters, _dBContext.Transaction);

            var sql = new StringBuilder("INSERT INTO MentoringConnectionAndWorking (MentoringId, ConnectionAndWorkingId) VALUES ");
            var parameters = new DynamicParameters();

            for (int i = 0; i < mentoringTypes.Count; i++)
            {
                sql.Append($"(@MentoringId, @TypeId{i}),");
                parameters.Add($"TypeId{i}", mentoringTypes[i]);
            }

            sql.Length--; // Remove the last comma
            parameters.Add("MentoringId", id);

            var result = await _dBContext.Connection.ExecuteAsync(sql.ToString(), parameters, _dBContext.Transaction);
            return result;
        }

        public async Task<int> SaveMentoringLanguagesAsync(Guid id, List<Guid> mentoringLanguages)
        {
            var deleteSql = "DELETE FROM MentoringLanguage WHERE MentoringId = @MentoringId";
            var deleteParameters = new DynamicParameters();
            deleteParameters.Add("MentoringId", id);

            await _dbContext.Connection.ExecuteAsync(deleteSql, deleteParameters, _dBContext.Transaction);

            var sql = new StringBuilder("INSERT INTO MentoringLanguage (MentoringId, LanguageId) VALUES ");
            var parameters = new DynamicParameters();

            for (int i = 0; i < mentoringLanguages.Count; i++)
            {
                sql.Append($"(@MentoringId, @Languages{i}),");
                parameters.Add($"Languages{i}", mentoringLanguages[i]);
            }

            sql.Length--; // Remove the last comma
            parameters.Add("MentoringId", id);

            var result = await _dBContext.Connection.ExecuteAsync(sql.ToString(), parameters, _dBContext.Transaction);
            return result;
        }

        public async Task<int> DeleteMentoringLanguagesByMentoringId(Guid id)
        {
            var sql = "DELETE FROM MentoringLanguage WHERE MentoringId = @MentoringId";
            var parameters = new DynamicParameters();
            parameters.Add("MentoringId", id);

            var result = await _dBContext.Connection.ExecuteAsync(sql, parameters, _dBContext.Transaction);
            return result;
        }

        public async Task<int> DeleteMentoringByMentoringId(Guid id)
        {
            var sql = "DELETE FROM Mentoring WHERE Id = @MentoringId";
            var parameters = new DynamicParameters();
            parameters.Add("MentoringId", id);

            var result = await _dBContext.Connection.ExecuteAsync(sql, parameters, _dBContext.Transaction);
            return result;
        }

        public async Task<List<Mentoring>> FilterMentoringsAsync(MentoringFilterDTO filter, string mentoringRole)
        {
            var sql = new StringBuilder();
            sql.AppendLine("SELECT DISTINCT m.*");
            sql.AppendLine("FROM Mentoring m");
            sql.AppendLine("LEFT JOIN MentoringField mf ON m.Id = mf.MentoringId");
            sql.AppendLine("LEFT JOIN MentoringLanguage ml ON m.Id = ml.MentoringId");
            sql.AppendLine("LEFT JOIN MentoringConnectionAndWorking mt ON m.Id = mt.MentoringId");
            sql.AppendLine("WHERE m.MentoringRole = 'mentor'");

            if (filter.FieldIds != null && filter.FieldIds.Any())
            {
                sql.AppendLine("AND mf.FieldId IN @FieldIds");
            }

            if (filter.LanguageIds != null && filter.LanguageIds.Any())
            {
                sql.AppendLine("AND ml.LanguageId IN @LanguageIds");
            }

            if (filter.ConnectionAndWorkings != null && filter.ConnectionAndWorkings.Any())
            {
                sql.AppendLine("AND mt.ConnectionAndWorkingId IN @TypeIds");
            }

            var parameters = new DynamicParameters();
            parameters.Add("FieldIds", filter.FieldIds);
            parameters.Add("LanguageIds", filter.LanguageIds);
            parameters.Add("TypeIds", filter.ConnectionAndWorkings);

            var profiles = await _dBContext.Connection.QueryAsync<Mentoring>(sql.ToString(), parameters);
            return profiles.ToList();

        }

        public async Task<Mentoring> GetMentoringByUserIdAndRoleAsync(Guid userId, string mentoringRole)
        {
            var sql = new StringBuilder();
            sql.AppendLine("SELECT m.*");
            sql.AppendLine("FROM Mentoring m");
            sql.AppendLine(" JOIN MentoringField mf ON m.Id = mf.MentoringId");
            sql.AppendLine(" JOIN MentoringLanguage ml ON m.Id = ml.MentoringId");
            sql.AppendLine(" JOIN MentoringConnectionAndWorking mt ON m.Id = mt.MentoringId");
            sql.AppendLine("WHERE UserId = @UserId and  m.MentoringRole = @MentoringRole");

          
            var parameters = new DynamicParameters();
            parameters.Add("UserId", userId);
            parameters.Add("MentoringRole", mentoringRole);

            var mentoring = await _dBContext.Connection.QueryFirstOrDefaultAsync<Mentoring>(sql.ToString(), parameters, _dBContext.Transaction);
            return mentoring!;
        }

        public async Task<int> DeleteMentoringConnectionAndWorkingByMentoringId(Guid id)
        {
            var sql = "DELETE FROM MentoringConnectionAndWorking WHERE MentoringId = @MentoringId";
            var parameters = new DynamicParameters();
            parameters.Add("MentoringId", id);

            var result = await _dBContext.Connection.ExecuteAsync(sql, parameters, _dBContext.Transaction);
            return result;
        }

        public async Task<List<Language>> GetLanguageListAsync()
        {
            var sql = "SELECT * FROM Language";
            var languages = await _dBContext.Connection.QueryAsync<Language>(sql);
            return languages.ToList();
        }

        public async Task<List<ConnectionAndWorking>> GetConnectionAndWorkingsAsync()
        {
            var sql = "SELECT * FROM ConnectionAndWorking";
            var connectionAndWorkings = await _dBContext.Connection.QueryAsync<ConnectionAndWorking>(sql);
            return connectionAndWorkings.ToList();
        }

        public async Task<int> SaveMentoringReportAsync(MentoringReport report)
        {
            var sql = new StringBuilder();

            var parameters = new DynamicParameters();
            parameters.Add("Id", report.Id);
            parameters.Add("ReporterId", report.ReporterId);
            parameters.Add("VictimId", report.VictimId);
            parameters.Add("Description", report.Description);
            parameters.Add("AdditionalInformation", report.AdditionalInformation);
            parameters.Add("CreatedAt", report.CreatedAt);

            sql.AppendLine("INSERT INTO MentoringReport (Id, ReporterId, VictimId, Description, AdditionalInformation, CreatedAt)");
            sql.AppendLine("VALUES (@Id, @ReporterId, @VictimId, @Description, @AdditionalInformation, @CreatedAt)");

            var result = await _dBContext.Connection.ExecuteAsync(sql.ToString(), parameters, _dBContext.Transaction);
            return result;
        }

        public async Task<List<MentoringReport>> GetReportListAsync()
        {
            var sql = "SELECT * FROM MentoringReport";
            var reports = await _dBContext.Connection.QueryAsync<MentoringReport>(sql);
            return reports.ToList();
        }

        public async Task<MentoringReportDetails> GetReportDetailsAsync(Guid id)
        {
            var sql = @"
            SELECT 
                mr.Id,
                mr.ReporterId,
                reporter.BasicInformation AS ReporterBasicInformation,
                mr.VictimId,
                victim.BasicInformation AS VictimBasicInformation,
                mr.Description,
                mr.AdditionalInformation,
                mr.CreatedAt
            FROM MentoringReport mr
            JOIN Mentoring reporter ON mr.ReporterId = reporter.Id
            JOIN Mentoring victim ON mr.VictimId = victim.Id
            WHERE mr.Id = @Id";

            var parameters = new DynamicParameters();
            parameters.Add("Id", id);

            var result = await _dBContext.Connection.QueryFirstOrDefaultAsync<MentoringReportDetails>(sql, parameters);
            return result!;
        }
    }
}
