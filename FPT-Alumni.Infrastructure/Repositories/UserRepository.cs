﻿using Dapper;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Infrastructure.Interfaces;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Core.DTOs;
using System.Text;

namespace FPT_Alumni.Infrastructure.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        private readonly IDBContext _dBContext;
        public UserRepository(IDBContext dbContext) : base(dbContext)
        {
            _dBContext = dbContext;
        }

        public async Task<List<User>> GetAllAsync()
        {
            var sql = @"SELECT * FROM [User]";
            var data = await _dBContext.Connection.QueryAsync<User>(sql);
            return data.ToList();
        }

        public async Task<UserReturnDTO?> GetByEmailAsync(string email)
        {
            var sql = @"
        SELECT u.Id, 
               COALESCE(s.FullName, a.FullName) AS FullName,
               u.MainEmail,
               r.RoleName,
        u.Password, u.RoleId, u.EmailLogged, u.Status
        FROM [User] u
        left JOIN [Role] r ON u.RoleId = r.Id
         left JOIN [Staff] s ON u.Id = s.UserId AND r.RoleName = 'Staff'
         left JOIN [Alumni] a ON u.Id = a.UserId AND r.RoleName = 'Alumni'
        WHERE u.MainEmail = @Email OR u.SubEmail = @Email and Status != 'DELETED'";

            var parameters = new { Email = email };
            var user = await _dBContext.Connection.QueryFirstOrDefaultAsync<UserReturnDTO>(sql, parameters, _dBContext.Transaction);
            return user;
        }

        public async Task<User> GetUserByTokenAsync(string? refreshToken)
        {
            string sql = "SELECT * FROM [User] where RefreshToken = @RefreshTokenValue";
            object parameters = new { RefreshTokenValue = refreshToken };

            var user = await _dBContext.Connection.QueryFirstOrDefaultAsync<User>(sql, parameters, _dbContext.Transaction);
            return user;
        }

        public async Task<int> UpdateByUserIdAsync(User updateUser)
        {
            var updateFields = new List<string>();
            var parameters = new DynamicParameters();

            void AddParameterIfNotNull<T>(string fieldName, T value)
            {
                if (value != null)
                {
                    updateFields.Add($"{fieldName} = @{fieldName}");
                    parameters.Add(fieldName, value);
                }
            }

            AddParameterIfNotNull("MainEmail", updateUser.MainEmail);
            AddParameterIfNotNull("Password", updateUser.Password);
            AddParameterIfNotNull("RefreshToken", updateUser.RefreshToken);
            AddParameterIfNotNull("SubEmail", updateUser.SubEmail);
            AddParameterIfNotNull("EmailLogged", updateUser.EmailLogged);
            AddParameterIfNotNull("ExperiedDateToken", updateUser.ExperiedDateToken);
            AddParameterIfNotNull("Status", updateUser.Status);
            if (updateFields.Count == 0)
            {
                return -1; // Không có trường nào cần cập nhật
            }

            parameters.Add("UserId", updateUser.Id);

            var updateSql = $"UPDATE [User] SET {string.Join(", ", updateFields)} WHERE Id = @UserId";

            var result = await _dBContext.Connection.ExecuteAsync(updateSql, parameters, _dbContext.Transaction);
            return result;
        }

        public async Task<bool> CheckPasswordAsync(string currentPassword, Guid Id)
        {
            string sql = "SELECT Password FROM [User] WHERE Id = @UserId";
            object parameters = new { UserId = Id };

            var passwordInDb = await _dBContext.Connection.QueryFirstOrDefaultAsync<string>(sql, parameters, _dBContext.Transaction);

            return BCrypt.Net.BCrypt.Verify(currentPassword, passwordInDb);
        }

        public async Task<bool> IsEmailTakenByAnotherUserAsync(string mainEmail, Guid? userId)
        {
            string sql = "SELECT COUNT(1) FROM [User] WHERE MainEmail = @Email";
            object parameters = new { Email = mainEmail };

            if (userId.HasValue)
            {
                sql += " AND Id <> @UserId";
                parameters = new { Email = mainEmail, UserId = userId.Value };
            }

            var count = await _dBContext.Connection.ExecuteScalarAsync<int>(sql, parameters, _dBContext.Transaction);

            return count > 0;
        }

        public async Task<bool> ApproveRole(UserStatusUpdateDTO userStatusUpdateDTO)
        {
            string sql = "UPDATE [User] SET Status = @NewStatus WHERE Id = @UserId";
            object parameters = new { NewStatus = userStatusUpdateDTO.Status, UserId = userStatusUpdateDTO.Id };

            var count = await _dBContext.Connection.ExecuteAsync(sql, parameters, _dBContext.Transaction);

            return count > 0;
        }

        public async Task<bool> ApproveRoles(List<UserStatusUpdateDTO> userStatusUpdateDTOs)
        {
            if (userStatusUpdateDTOs == null || !userStatusUpdateDTOs.Any())
            {
                return false; // Không có bản ghi nào để cập nhật
            }

            var sqlBuilder = new StringBuilder();
            var parameters = new DynamicParameters();

            for (int i = 0; i < userStatusUpdateDTOs.Count; i++)
            {
                var userStatusUpdateDTO = userStatusUpdateDTOs[i];
                var parameterIndex = i + 1; // Đảm bảo các tham số là duy nhất

                sqlBuilder.AppendLine($"UPDATE [User] SET Status = @NewStatus{parameterIndex} WHERE Id = @UserId{parameterIndex};");
                parameters.Add($"@NewStatus{parameterIndex}", userStatusUpdateDTO.Status);
                parameters.Add($"@UserId{parameterIndex}", userStatusUpdateDTO.Id);
            }

            var result = await _dBContext.Connection.ExecuteAsync(sqlBuilder.ToString(), parameters, _dBContext.Transaction);

            return result > 0;
        }

        public async Task<bool> DeleteUserAsync(Guid userId)
        {
            string sql = "UPDATE [User] SET Status = 'DELETED' WHERE Id = @UserId";
            object parameters = new {  UserId = userId };

            var count = await _dBContext.Connection.ExecuteAsync(sql, parameters, _dBContext.Transaction);

            return count > 0;
        }

        public async Task<string> GetEmailByIdAsync(Guid id)
        {
            string sql = "SELECT u.MainEmail FROM [User] u WHERE u.Id = @id";
            object parameters = new { id = id };
            var email = await _dBContext.Connection.QuerySingleOrDefaultAsync<string>(sql, parameters, _dBContext.Transaction);
            return email!;
        }

        public async Task<User> GetUserByIdAsync(Guid userId)
        {
            const string sql = "SELECT * FROM [User] WHERE Id = @UserId";
            var parameters = new { UserId = userId };

            var user = await _dbContext.Connection.QueryFirstOrDefaultAsync<User>(sql, parameters, _dbContext.Transaction);
            return user!;
        }
    }
}

