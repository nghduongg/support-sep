﻿using Dapper;
using FPT_Alumni.Core.Constants;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Enums;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Core.Interfaces.Services;
using FPT_Alumni.Core.Services;
using FPT_Alumni.Infrastructure.Interfaces;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FPT_Alumni.Infrastructure.Repositories
{
    public class PostRepository : BaseRepository<Post>, IPostRepository
    {
        private readonly IDBContext _dBContext;
        readonly IUnitOfWork _unitOfWork;
        public PostRepository(IDBContext dbContext) : base(dbContext)
        {
            _dBContext = dbContext;

        }

        public async Task<PostDTO> GetPostDTOByIdAsync(Guid id, int option)
        {
            string postSql = "";
            switch (option)
            {
                //  get created user id to check when delete or update
                case 1:
                    postSql = @"
             SELECT DISTINCT p.Id, p.Title, p.Content, p.IsPublic,a.Avartar, p.CategoryId, pc.CategoryName, p.CreatedDate, p.CreatedBy as CreatedId, a.FullName as CreatedBy, g.GroupName
             FROM Post p INNER JOIN PostUser pu ON PU.PostId = p.Id
             INNER JOIN [User] u ON p.CreatedBy = u.Id  Inner Join Alumni a on a.UserId = u.Id
             Inner JOin [Group] g on p.GroupId = g.Id
             Inner Join PostCategory pc on p.CategoryId = pc.Id
             WHERE p.IsPublic = 1 AND P.Id = @postId;";
                    break;
                case 2:
                // user view
                default:
                    postSql = @" SELECT DISTINCT p.Id, p.Title, p.Content, p.IsPublic,a.Avartar, p.CategoryId, pc.CategoryName, p.CreatedDate, p.CreatedBy as CreatedId, a.FullName as CreatedBy, g.GroupName
             FROM Post p INNER JOIN PostUser pu ON PU.PostId = p.Id
             INNER JOIN [User] u ON p.CreatedBy = u.Id  Inner Join Alumni a on a.UserId = u.Id
             Inner JOin [Group] g on p.GroupId = g.Id
             Inner Join PostCategory pc on p.CategoryId = pc.Id
             WHERE p.Id = @postId;";
                    break;
            }

            var tagSql = @"
        SELECT t.Id, t.TagName
        FROM PostTag pt
        JOIN Tag t ON pt.TagId = t.Id
        WHERE pt.PostId = @postId;";

            var mediaSql = @"
        SELECT m.Id, m.MediaUrl 
        FROM Media m 
        WHERE m.PostId = @postId;";

            var userTaggedSql = @"
        SELECT pu.UserId, a.FullName 
        FROM PostUser pu
        JOIN [User] u ON pu.UserId = u.Id
        JOIN Alumni a ON u.Id = a.UserId
        WHERE pu.PostId = @postId;";

            var fieldsSql = @"
                SELECT f.* FROM Post p INNER JOIN PostField pf
         ON p.Id = pf.PostId
         INNER JOIN Field f
         ON pf.FieldId = f.Id
         WHERE p.Id = @postId";

            var parameters = new
            {
                postId = id
            };

            if (_dBContext == null || _dBContext.Connection == null)
            {
                throw new InvalidOperationException("Database context or connection is not initialized.");
            }
            var post = await _dBContext.Connection.QueryFirstOrDefaultAsync<PostDTO>(postSql, parameters, _dBContext.Transaction);
            if (post != null)
            {
                var postIdParameter = new { PostId = post.Id };
                List<MediaDTO> mediaDTO = (await _dBContext.Connection.QueryAsync<MediaDTO>(mediaSql, postIdParameter, _dBContext.Transaction)).ToList();
                List<Tag> tags = (await _dBContext.Connection.QueryAsync<Tag>(tagSql, postIdParameter, _dBContext.Transaction)).ToList();
                List<UserTaggedInPostDTO> userTaggeds = (await _dBContext.Connection.QueryAsync<UserTaggedInPostDTO>(userTaggedSql, postIdParameter, _dBContext.Transaction)).ToList();
                List<Field> fields = (await _dBContext.Connection.QueryAsync<Field>(fieldsSql, postIdParameter, _dBContext.Transaction)).ToList();
                post.Medias = mediaDTO;
                post.Tags = tags;
                post.UserTaggedInPostDTOs = userTaggeds;
                post.Fields = fields;
                post.NumberOfComment = _unitOfWork.Comment.GetNumberOfCommentsByPostIdAsync(post.Id);
            }
            return post;
        }

        public async Task<int> UpdatePostStatusAsync(Guid postId, int status)
        {
            var sql = "UPDATE POST SET IsPublic = @Status WHERE Id = @Id";
            var parameters = new
            {
                Id = postId,
                Status = status
            };
            var affectedRows = await _dbContext.Connection.ExecuteAsync(sql, parameters, _dBContext.Transaction);
            return affectedRows;
        }

        public async Task<int> SavePostAsync(Post post, string action)
        {
            var parameters = new DynamicParameters();

            void AddParameterIfNotNull<T>(string fieldName, T value)
            {
                if (value != null)
                {
                    parameters.Add(fieldName, value);
                }
            }

            AddParameterIfNotNull("Id", post.Id);
            AddParameterIfNotNull("CategoryId", post.CategoryId);
            AddParameterIfNotNull("Title", post.Title);
            AddParameterIfNotNull("Content", post.Content);
            AddParameterIfNotNull("IsPublic", post.IsPublic);
            AddParameterIfNotNull("CreatedDate", post.CreatedDate);
            AddParameterIfNotNull("CreatedBy", post.CreatedBy);
            AddParameterIfNotNull("ModifiedDate", post.ModifiedDate);
            AddParameterIfNotNull("ModifiedBy", post.ModifiedBy);

            string sql;
            if (action == "insert")
            {
                sql = @"INSERT INTO Post (Id, UserId, CategoryPostId, Title, Content, IsPublic, CreatedDate, ModifiedDate, CreatedBy, ModifiedBy)
                VALUES (@Id, @UserId, @CategoryPostId, @Title, @Content, @IsPublic, @CreatedDate, @ModifiedDate, @CreatedBy, @ModifiedBy)";
            }
            else if (action == "update" && post.Id != Guid.Empty)
            {
                sql = @"UPDATE Post SET UserId = @UserId, CategoryPostId = @CategoryPostId, Title = @Title, 
                Content = @Content, IsPublic = @IsPublic, CreatedDate = @CreatedDate, ModifiedDate = @ModifiedDate, CreatedBy = @CreatedBy, ModifiedBy = @ModifiedBy 
                WHERE Id = @Id";
            }
            else
            {
                throw new ArgumentException("Invalid action specified or missing Id for update operation.");
            }

            var result = await _dBContext.Connection.ExecuteAsync(sql, parameters, _dBContext.Transaction);

            return result;
        }

        public async Task<List<PostDTO>> FilterPagingInUserPorfile(int currentPage, int pageSize, Guid userId, Guid[]? fieldIds, string key, DateTime? fromDate, DateTime? toDate)
        {
            string postSql = @"
        SELECT DISTINCT p.Id, p.Title, p.Content, p.IsPublic, a.Avartar, p.CreatedDate,
        a.FullName as CreatedBy, p.CreatedBy as CreatedId
        FROM Post p 
        INNER JOIN PostUser pu ON pu.PostId = p.Id 
        INNER JOIN [User] u ON p.CreatedBy = u.Id 
        INNER JOIN Alumni a ON a.UserId = u.Id
        INNER JOIN PostField pf ON p.Id = pf.PostId
        WHERE pu.UserId = @UserId 
        AND p.Title LIKE @Key
        AND ( pf.FieldId IN @FieldIds";

            if (fromDate.HasValue && toDate.HasValue)
            {
                postSql += " OR p.CreatedDate BETWEEN @StartDate AND @EndDate";
            }
            else if (fromDate.HasValue)
            {
                postSql += " OR p.CreatedDate >= @StartDate";
            }
            else if (toDate.HasValue)
            {
                postSql += " OR p.CreatedDate <= @EndDate";
            }

            postSql += @")
        ORDER BY p.CreatedDate 
        OFFSET @Offset ROWS
        FETCH NEXT @PageSize ROWS ONLY;";

            var tagSql = @"
        SELECT t.Id, t.TagName
        FROM PostTag pt
        JOIN Tag t ON pt.TagId = t.Id
        WHERE pt.PostId = @PostId;";

            var mediaSql = @"
        SELECT m.Id, m.MediaUrl 
        FROM Media m 
        WHERE m.PostId = @PostId;";

            var userTaggedSql = @"
        SELECT pu.UserId, a.FullName 
        FROM PostUser pu
        JOIN [User] u ON pu.UserId = u.Id
        JOIN Alumni a ON u.Id = a.UserId
        WHERE pu.PostId = @PostId;";

            var fieldSql = @"SELECT f.Id, f.FieldName 
         FROM Field f INNER JOIN PostField pf
         ON f.Id = pf.FieldId WHERE pf.PostId = @PostId ";

            var parameters = new DynamicParameters();
            parameters.Add("@UserId", userId);
            parameters.Add("@Key", $"%{key}%");
            parameters.Add("@FieldIds", fieldIds);
            parameters.Add("@Offset", (currentPage - 1) * pageSize);
            parameters.Add("@PageSize", pageSize);

            if (fromDate.HasValue)
            {
                parameters.Add("@StartDate", fromDate.Value);
            }
            if (toDate.HasValue)
            {
                parameters.Add("@EndDate", toDate.Value);
            }

            var posts = (await _dBContext.Connection.QueryAsync<PostDTO>(postSql, parameters, _dBContext.Transaction)).ToList();


            foreach (var post in posts)
            {
                var postIdParameter = new { PostId = post.Id };
                List<MediaDTO> mediaDTO = (await _dBContext.Connection.QueryAsync<MediaDTO>(mediaSql, postIdParameter, _dBContext.Transaction)).ToList();
                List<Tag> tags = (await _dBContext.Connection.QueryAsync<Tag>(tagSql, postIdParameter, _dBContext.Transaction)).ToList();
                List<UserTaggedInPostDTO> userTaggeds = (await _dBContext.Connection.QueryAsync<UserTaggedInPostDTO>(userTaggedSql, postIdParameter, _dBContext.Transaction)).ToList();
                List<Field> fields = (await _dBContext.Connection.QueryAsync<Field>(fieldSql, postIdParameter, _dBContext.Transaction)).ToList();
                post.Medias = mediaDTO;
                post.Tags = tags;
                post.UserTaggedInPostDTOs = userTaggeds;
                post.Fields = fields;
            }
            return posts;

        }

        public async Task<List<PostDTO>> FilterPagingInGroup(int currentPage, int pageSize, Guid groupId, Guid[]? fieldIds, string key, DateTime? startDate, DateTime? endDate)
        {
            string postSql = @"SELECT DISTINCT p.Id, p.Title, p.Content, p.IsPublic, a.Avartar, p.CreatedDate, a.FullName as CreatedBy, p.CreatedBy as CreatedId, p.CategoryId
            FROM Post p INNER JOIN PostUser pu ON PU.PostId = p.Id 
            INNER JOIN [User] u ON p.CreatedBy = u.Id 
            Inner Join Alumni a on a.UserId = u.Id
            INNER JOIN PostField pf ON p.Id = pf.PostId
            WHERE P.GroupId = @GroupId 
            And p.Title Like @Key 
            AND ( pf.FieldId in @FieldIds 
            OR p.CategoryId in  @categoryIds)
            AND p.IsPublic = 1
            ORDER BY p.CreatedDate 
            OFFSET @Offset ROWS
            FETCH NEXT @PageSize ROWS ONLY;";

            var tagSql = @"
        SELECT t.Id, t.TagName
        FROM PostTag pt
        JOIN Tag t ON pt.TagId = t.Id
        WHERE pt.PostId = @PostId;";

            var mediaSql = @"
        SELECT m.Id, m.MediaUrl 
        FROM Media m 
        WHERE m.PostId = @PostId;";

            var userTaggedSql = @"
        SELECT pu.UserId, a.FullName 
        FROM PostUser pu
        JOIN [User] u ON pu.UserId = u.Id
        JOIN Alumni a ON u.Id = a.UserId
        WHERE pu.PostId = @PostId;";

            var fieldSql = @"SELECT f.Id, f.FieldName 
         FROM Field f INNER JOIN PostField pf
         ON f.Id = pf.FieldId WHERE pf.PostId = @PostId ";

            var parameters = new
            {
                Offset = (currentPage - 1) * pageSize,
                PageSize = pageSize,
                GroupId = groupId,
                StatusType = PostCategoryConstant.STATUS,
                JobOfferType = PostCategoryConstant.JOB_OFFER,
                MentoringType = PostCategoryConstant.MENTORING,
                CouponType = PostCategoryConstant.COUPON,
                FieldIds = fieldIds,
                Key = "%" + key + "%",
                StartDate = startDate,
                EndDate = endDate,
            };

            var posts = (await _dBContext.Connection.QueryAsync<PostDTO>(postSql, parameters, _dBContext.Transaction)).ToList();

            foreach (var post in posts)
            {
                var postIdParameter = new { PostId = post.Id };
                List<MediaDTO> mediaDTO = (await _dBContext.Connection.QueryAsync<MediaDTO>(mediaSql, postIdParameter, _dBContext.Transaction)).ToList();
                List<Tag> tags = (await _dBContext.Connection.QueryAsync<Tag>(tagSql, postIdParameter, _dBContext.Transaction)).ToList();
                List<UserTaggedInPostDTO> userTaggeds = (await _dBContext.Connection.QueryAsync<UserTaggedInPostDTO>(userTaggedSql, postIdParameter, _dBContext.Transaction)).ToList();
                List<Field> fields = (await _dBContext.Connection.QueryAsync<Field>(fieldSql, postIdParameter, _dBContext.Transaction)).ToList();
                post.Medias = mediaDTO;
                post.Tags = tags;
                post.UserTaggedInPostDTOs = userTaggeds;
                post.Fields = fields;
                post.NumberOfComment = _unitOfWork.Comment.GetNumberOfCommentsByPostIdAsync(post.Id);
            }
            return posts;
        }
    }
}
