﻿using Dapper;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Infrastructure.Repositories
{
    public class TagRepository : BaseRepository<Tag>, ITagRepository
    {
        private readonly IDBContext _dBContext;
        public TagRepository(IDBContext dbContext) : base(dbContext)
        {
            _dBContext = dbContext;
        }

        public async Task<Tag?> FindByTagNameAsync(string tagName)
        {
            string sql = "SELECT * FROM Tag t where t.TagName = @tagname";
            object parameters = new { tagname = tagName };
            var tag = await _dBContext.Connection.QuerySingleOrDefaultAsync<Tag>(sql, parameters, _dBContext.Transaction);
            return tag;
        }

        public async Task<List<Tag>> FindByTagNameForTagingAsync(string key)
        {
            const string sql = "SELECT * FROM Tag t WHERE t.TagName LIKE @key";
            var parameters = new { key = "%" + key + "%" };
            var tags = await _dBContext.Connection.QueryAsync<Tag>(sql, parameters, _dBContext.Transaction);
            return tags.ToList();
        }

        public async Task<List<Tag>> GetAllAsync()
        {
            const string sql = "SELECT * FROM Tag ";
            var tags = await _dBContext.Connection.QueryAsync<Tag>(sql, _dBContext.Transaction);
            return tags.ToList();
        }

        public async Task<int> MultipleDeleteById(List<Guid> ids)
        {
            string sql = "DELETE FROM Tag WHERE Id IN @Ids";
            int res = _dbContext.Connection.Execute(sql, new { Ids = ids });
            return res;
        }

    }
}
