﻿using Dapper;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Infrastructure.Repositories
{
    public class PostTagRepository : BaseRepository<PostTag>, IPostTagRepository
    {
        IDBContext _dBContext;
        public PostTagRepository(IDBContext dbContext, IDBContext dBContext) : base(dbContext)
        {
            _dBContext = dBContext;
        }

        public async Task<List<PostTag>> GetByPostIdAsync(Guid postId)
        {
            var sql = "SELECT * FROM PostTag WHERE PostId = @postId";
            var parameters = new { postId = postId };
            var medias = await _dBContext.Connection.QueryAsync<PostTag>(sql, parameters, _dBContext.Transaction);
            return medias.ToList();
        }

        public async Task<int> MultipleDeleteByPostIdAndTagId(Guid postId, List<Guid> tagIds)
        {
            string sql = "DELETE FROM PostTag  WHERE PostId = @postId AND TagId IN @tagIds";
            int res = await _dbContext.Connection.ExecuteAsync(sql, new { postId = postId, tagIds = tagIds }, _dBContext.Transaction);
            return res;
        }
    }
}
