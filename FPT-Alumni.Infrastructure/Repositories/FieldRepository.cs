﻿using Dapper;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Infrastructure.Repositories
{
    public class FieldRepository : BaseRepository<Field>, IFieldRepository
    {
        private readonly IDBContext _dBContext;
        public FieldRepository(IDBContext dbContext) : base(dbContext)
        {
            _dBContext = dbContext;
        }

        public async Task<List<Field>> GetAllFieldAsync()
        {
            var sql = @"SELECT * FROM [Field]";
            var data = await _dBContext.Connection.QueryAsync<Field>(sql, _dbContext.Transaction);
            return data.ToList();
        }

        public async Task<List<Field>> SearchFieldByNameAsync(string fieldName)
        {
            var sql = "SELECT * FROM Field WHERE FieldName LIKE @FieldName";
            var parameters = new { FieldName = "%" + fieldName + "%" };
            var field = await _dBContext.Connection.QueryAsync<Field>(sql, parameters, _dBContext.Transaction);
            return field.ToList();
        }

        public async Task<List<Field>> GetFieldsByMentoringId(Guid id)
        {
            var sql = @"
        SELECT f.* 
        FROM MentoringField mf
        INNER JOIN Field f ON mf.FieldId = f.Id
        WHERE mf.MentoringId = @MentoringId";

            var parameters = new { MentoringId = id };
            var data = await _dBContext.Connection.QueryAsync<Field>(sql, parameters, _dBContext.Transaction);
            return data.ToList();
        }
    }
}
