﻿using Dapper;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Infrastructure.Repositories
{
    public class PostUserRepository : BaseRepository<PostUser>, IPostUserRepository
    {
        IDBContext _dBContext;
        public PostUserRepository(IDBContext dbContext, IDBContext dBContext) : base(dbContext)
        {
            _dBContext = dBContext;
        }

        public async Task<List<PostUser>> GetByPostIdAsync(Guid postId)
        {
            var sql = "SELECT * FROM PostUser WHERE PostId = @postId";
            var parameters = new { postId = postId };
            var medias = await _dBContext.Connection.QueryAsync<PostUser>(sql, parameters, _dBContext.Transaction);
            return medias.ToList();
        }

        public async Task<int> MultipleDeleteByPostIdAndTagId(Guid postId, List<Guid> userIds)
        {
            string sql = "DELETE FROM PostUser WHERE PostId = @postId AND UserId IN @userIds";
            int res = await _dbContext.Connection.ExecuteAsync(sql, new { postId = postId, userIds = userIds }, _dbContext.Transaction);
            return res;
        }

        public async Task<int> DeleteByPostIdAsync(Guid postId)
        {
            var sql = "DELETE FROM PostUser WHERE PostId = @postId";
            var parameters = new { postId = postId };
            var res = await _dBContext.Connection.ExecuteAsync(sql, parameters, _dBContext.Transaction);
            return res;
        }
    }
}
