﻿using FireSharp.Response;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Repositories;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPT_Alumni.Core.Constants;

namespace FPT_Alumni.Infrastructure.Repositories
{
    public class CommentRepository : ICommentRepository
    {
        public CommentRepository() { }

        public int GetNumberOfCommentsByPostIdAsync(Guid postId)
        {
            FirebaseResponse response = FirebaseConstants.client.Get("Comments");
            dynamic data = JsonConvert.DeserializeObject<dynamic>(response.Body);
            int count = 0;
            if (data != null)
            {
                foreach (var item in data)
                {
                    Comment cmt = JsonConvert.DeserializeObject<Comment>(((JProperty)item).Value.ToString());
                    if (cmt.PostId.Equals(postId))
                    {
                        count++;
                    }
                }
            }
            return count;
        }
    }
}
