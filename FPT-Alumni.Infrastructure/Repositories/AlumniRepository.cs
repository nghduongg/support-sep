﻿using Dapper;
using Domain.Exceptions;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.JsonModels;
using FPT_Alumni.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;


namespace FPT_Alumni.Infrastructure.Repositories
{
    public class AlumniRepository : BaseRepository<Alumni>, IAlumniRepository
    {
        private readonly IDBContext _dBContext;

        public AlumniRepository(IDBContext dbContext) : base(dbContext)
        {
            _dBContext = dbContext;
        }

        public async Task<UserModelView> GetAlumniByUserIdAsync(Guid userId)
        {
            var sql = @"SELECT u.Id as UserId, u.MainEmail, u.Password, u.RoleId, r.RoleName, a.Avartar, a.StudentCode, a.PhoneNumber, 
                   a.Class, a.Major, a.IsGraduated, a.Job, a.Company, a.LinkedUrl, a.WorkExperience, 
                   a.EducationExperience, a.PrivacySettings, a.FullName, a.DateOfBirth, a.GraduationYear, 
                   a.Gender, a.Country, a.City
                     FROM [User] u
                     left JOIN Alumni a ON u.Id = a.UserId 
                     INNER JOIN Role r ON u.RoleId = r.Id 
                     WHERE r.RoleName = 'Alumni' and a.UserId = @UserId";

            var parameters = new { UserId = userId };
            var result = await _dBContext.Connection.QueryFirstOrDefaultAsync<UserModelView>(sql, parameters, _dBContext.Transaction);

            if (result != null)
            {
                if (!string.IsNullOrEmpty(result.WorkExperience))
                {
                    result.DecodeWorkExperienceJsonFields();
                }
                if (!string.IsNullOrEmpty(result.EducationExperience))
                {
                    result.DecodeEducationExperienceJsonFields();
                }

                if (!string.IsNullOrEmpty(result.PrivacySettings))
                {
                    result.DecodePrivacySettingsJsonFields();
                }

                // Truy vấn lấy danh sách các Field dựa trên alumniId
                var fieldSql = @"SELECT f.Id, f.FieldName 
                         FROM Field f
                         INNER JOIN AlumniField uf ON f.Id = uf.FieldId
                         INNER JOIN Alumni a ON a.UserId = uf.UserId
                         WHERE a.UserId = @AlumniId";

                var fieldParameters = new { AlumniId = result.UserId };
                var fields = await _dBContext.Connection.QueryAsync<Field>(fieldSql, fieldParameters, _dBContext.Transaction);
                result.Fields = fields.ToList();
            }
            return result;
        }

        public async Task<int> UpdateAlumniByUserIdAsync(Alumni updateAlumniDto)
        {
            var updateFields = new List<string>();
            var parameters = new DynamicParameters();

            void AddParameterIfNotNull<T>(string fieldName, T value)
            {
                if (value != null)
                {
                    updateFields.Add($"{fieldName} = @{fieldName}Value");
                    parameters.Add($"{fieldName}Value", value);
                }
            }
            AddParameterIfNotNull("Avartar", updateAlumniDto.Avartar);
            AddParameterIfNotNull("StudentCode", updateAlumniDto.StudentCode);
            AddParameterIfNotNull("PhoneNumber", updateAlumniDto.PhoneNumber);
            AddParameterIfNotNull("Class", updateAlumniDto.Class);
            AddParameterIfNotNull("Major", updateAlumniDto.Major);
            AddParameterIfNotNull("IsGraduated", updateAlumniDto.IsGraduated);
            AddParameterIfNotNull("Job", updateAlumniDto.Job);
            AddParameterIfNotNull("Company", updateAlumniDto.Company);
            AddParameterIfNotNull("LinkedUrl", updateAlumniDto.LinkedUrl);
            AddParameterIfNotNull("WorkExperience", updateAlumniDto.WorkExperience);
            AddParameterIfNotNull("EducationExperience", updateAlumniDto.EducationExperience);
            AddParameterIfNotNull("PrivacySettings", updateAlumniDto.PrivacySettings);
            AddParameterIfNotNull("FullName", updateAlumniDto.FullName);
            AddParameterIfNotNull("DateOfBirth", updateAlumniDto.DateOfBirth);
            AddParameterIfNotNull("Gender", updateAlumniDto.Gender);
            AddParameterIfNotNull("Country", updateAlumniDto.Country);
            AddParameterIfNotNull("City", updateAlumniDto.City);
            AddParameterIfNotNull("GraduationYear", updateAlumniDto.GraduationYear);
            AddParameterIfNotNull("ModifiedDate", updateAlumniDto.ModifiedDate);
            AddParameterIfNotNull("ModifiedBy", updateAlumniDto.ModifiedBy);

            if (updateFields.Count == 0)
            {
                return -1; // Không có trường nào cần cập nhật
            }

            parameters.Add("UserIdValue", updateAlumniDto.UserId);
            var updateSql = $"UPDATE Alumni SET {string.Join(", ", updateFields)} WHERE UserId = @UserIdValue";
            var result = await _dBContext.Connection.ExecuteAsync(updateSql, parameters, _dBContext.Transaction);
            return result;
        }

        public async Task<int> UpdateWorkExperienceByUserIdAsync(string newWorkExperience, Guid userId)
        {
            var sql = @"UPDATE Alumni 
                SET WorkExperience = @NewWorkExperience 
                WHERE UserId = @UserId";
            var parameters = new { NewWorkExperience = newWorkExperience, UserId = userId };
            var result = await _dBContext.Connection.ExecuteAsync(sql, parameters, _dBContext.Transaction);
            return result;
        }

        public async Task<int> UpdateEducationExperienceByUserIdAsync(string newEducationExperience, Guid userId)
        {
            var sql = @"UPDATE Alumni 
                SET EducationExperience = @NewEducationExperience 
                WHERE UserId = @UserId";
            var parameters = new { NewEducationExperience = newEducationExperience, UserId = userId };
            var result = await _dBContext.Connection.ExecuteAsync(sql, parameters, _dBContext.Transaction);
            return result;
        }

        public async Task<List<UserModelView>> GetAlumniListAsync()
        {
            var sql = @"SELECT u.Id as UserId, u.MainEmail, u.Password, u.RoleId, r.RoleName, a.Avartar, a.StudentCode, a.PhoneNumber, 
                   a.Class, a.Major, a.IsGraduated, a.Job, a.Company, a.LinkedUrl, a.WorkExperience, 
                   a.EducationExperience, a.PrivacySettings, a.FullName, a.DateOfBirth, a.GraduationYear, 
                   a.Gender, a.Country, a.City
                     FROM [User] u
                     left JOIN Alumni a ON u.Id = a.UserId 
                     INNER JOIN Role r ON u.RoleId = r.Id 
                     WHERE r.RoleName = 'Alumni' and u.Status != 'DELETED' ";

            var result = await _dBContext.Connection.QueryAsync<UserModelView>(sql);

            if (result != null)
            {
                foreach (var r in result)
                {
                    if (!string.IsNullOrEmpty(r.WorkExperience))
                    {
                        r.DecodeWorkExperienceJsonFields();
                    }
                    if (!string.IsNullOrEmpty(r.EducationExperience))
                    {
                        r.DecodeEducationExperienceJsonFields();
                    }

                    if (!string.IsNullOrEmpty(r.PrivacySettings))
                    {
                        r.DecodePrivacySettingsJsonFields();
                    }
                }
            }
            return result!.ToList();
        }

        public async Task<List<UserModelView>> GetAlumniesByStatusAsync(string? status)
        {
            var sql = @"SELECT u.Id as UserId, u.MainEmail, u.Password, u.RoleId, r.RoleName, a.Avartar, a.StudentCode, a.PhoneNumber, 
                   a.Class, a.Major, a.IsGraduated, a.Job, a.Company, a.LinkedUrl, a.WorkExperience, 
                   a.EducationExperience, a.PrivacySettings, a.FullName, a.DateOfBirth, a.GraduationYear, 
                   a.Gender, a.Country, a.City
                     FROM [User] u
                     left JOIN Alumni a ON u.Id = a.UserId 
                     INNER JOIN Role r ON u.RoleId = r.Id 
                     WHERE r.RoleName = 'Alumni' and  u.Status = @StatusValue";

            var parameters = new { StatusValue = status };

            var result = await _dBContext.Connection.QueryAsync<UserModelView>(sql, parameters, _dBContext.Transaction);

            if (result != null)
            {
                foreach (var r in result)
                {
                    if (!string.IsNullOrEmpty(r.WorkExperience))
                    {
                        r.DecodeWorkExperienceJsonFields();
                    }
                    if (!string.IsNullOrEmpty(r.EducationExperience))
                    {
                        r.DecodeEducationExperienceJsonFields();
                    }
                    if (!string.IsNullOrEmpty(r.PrivacySettings))
                    {
                        r.DecodePrivacySettingsJsonFields();
                    }
                }
            }
            return result!.ToList();
        }

        public async Task<int> UpdateAlumniFieldsAsync(Guid userId, List<Guid> fieldIds)
        {
            var deleteSql = "DELETE FROM AlumniField WHERE UserId = @UserId";
            await _dBContext.Connection.ExecuteAsync(deleteSql, new { UserId = userId }, _dbContext.Transaction);
            int insertCount = 0;
            // Thêm các liên kết mới
            var insertSql = "INSERT INTO AlumniField (UserId, FieldId) VALUES (@UserId, @FieldId)";
            foreach (var fieldId in fieldIds)
            {
                insertCount += await _dBContext.Connection.ExecuteAsync(insertSql, new { UserId = userId, FieldId = fieldId }, _dbContext.Transaction);
            }
            return insertCount;
        }

        public async Task<int> UpdatePrivacySettingsByUserIdAsync(string updatedPrivacySettingsJson, Guid userId)
        {
            var sql = @"UPDATE Alumni 
                SET PrivacySettings = @NewPrivacySettings
                WHERE UserId = @UserId";

            var parameters = new { NewPrivacySettings = updatedPrivacySettingsJson, UserId = userId };
            var result = await _dBContext.Connection.ExecuteAsync(sql, parameters, _dBContext.Transaction);
            return result;
        }

        public async Task<List<AlumniSearchToTagDTO>> FindAlumniByKeyWordsAsync(string key)
        {
            var sql = @"
            SELECT u.Id, a.FullName, a.Avartar, a.StudentCode, u.MainEmail
            FROM Alumni a
            INNER JOIN [User] u ON a.UserId = u.Id
            WHERE a.FullName LIKE @Key OR a.StudentCode LIKE @Key OR u.MainEmail LIKE @Key";
            var result = await _dbContext.Connection.QueryAsync<AlumniSearchToTagDTO>(sql, new { Key = $"%{key}%" });
            return result.AsList();
        }

        public async Task<AlumniProfileDTO> GetAlumniProfileByUserIdAsync(Guid id)
        {
            var sql = @"
            SELECT a.Avartar, a.FullName,a.StudentCode, a.Major, a.Job, a.GraduationYear,a.PrivacySettings, a.City, a.Country, a.LinkedUrl, a.Class, a.Company,
            a.EducationExperience,a.DateOfBirth,a.IsGraduated, u.MainEmail as Email, a.PhoneNumber,a.WorkExperience 
            FROM [User] u INNER JOIN Alumni a
            ON u.Id = a.UserId WHERE u.Id = @Id";

            var parameters = new { Id = id };
            var result = await _dBContext.Connection.QueryFirstOrDefaultAsync<AlumniProfileDTO>(sql, parameters, _dBContext.Transaction);
            if (result == null)
                throw new NotFoundException("Alumni không tồn tại");
            var fieldSql = @"SELECT f.*
                         FROM Field f
                         INNER JOIN AlumniField uf ON f.Id = uf.FieldId
                         INNER JOIN Alumni a ON a.UserId = uf.UserId
                         WHERE a.UserId = @Id";
            var fields = await _dBContext.Connection.QueryAsync<Field>(fieldSql, parameters, _dBContext.Transaction);
            result.Fields = fields.ToList();
            return result;
        }
    }
}

