﻿using FPT_Alumni.Core.Interfaces.Repositories;
using Microsoft.Extensions.Caching.Memory;


namespace FPT_Alumni.Infrastructure.Repositories
{
    public class CacheRepository : ICacheRepository
    {
        private IMemoryCache _memoryCache;

        public CacheRepository(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        public T GetCache<T>(string key)
        {
            return _memoryCache.Get<T>(key);
        }

        public bool IsCacheExist(string key)
        {
            return _memoryCache.TryGetValue(key, out _);
        }

        public void SetCache<T>(string key, T value, TimeSpan expirationTime)
        {
            _memoryCache.Set(key, value, expirationTime);
        }

        public void UpdateCache<T>(string key, T value)
        {
            if (IsCacheExist(key))
            {
                _memoryCache.Remove(key);
            }
            _memoryCache.Set(key, value);
        }
    }
}
