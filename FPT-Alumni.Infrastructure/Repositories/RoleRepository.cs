﻿using Dapper;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Infrastructure.Interfaces;


namespace FPT_Alumni.Infrastructure.Repositories
{
    public class RoleRepository : BaseRepository<Role>, IRoleRepository
    {
        IDBContext _dBContext;

        public RoleRepository(IDBContext dbContext) : base(dbContext)
        {
            _dBContext = dbContext;
        }

        public async Task<List<Role>> GetAllAsync()
        {
            var sql = @"SELECT * FROM Role";
            var data = await _dBContext.Connection.QueryAsync<Role>(sql);
            return data.ToList();
        }
    }
}
