﻿using Dapper;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Infrastructure.Repositories
{
    public class ApplicationRepository : BaseRepository<Application>, IApplicationRepository
    {
        private readonly IDBContext _dBContext;

        public ApplicationRepository(IDBContext dbContext) : base(dbContext)
        {
            _dBContext = dbContext;
        }

        public async Task<List<Application>> GetAllCandidateByJobId(Guid jobId, int currentPage, int pageSize)
        {
            var sql = "SELECT * FROM Application  WHERE JobId = @JobId ORDER BY Name OFFSET @Offset ROWS FETCH NEXT @PageSize ROWS ONLY;";
            var parameters = new DynamicParameters();
            parameters.Add("JobId", jobId);
            parameters.Add("Offset", (currentPage - 1) * pageSize);
            parameters.Add("PageSize", pageSize);

            var applications = await _dBContext.Connection.QueryAsync<Application>(sql, parameters);
            return applications.ToList();
        }

        public async Task<bool> DeleteApplicationByJobIdAsync(Guid jobId)
        {
            string sql = "DELETE FROM Application WHERE JobId = @JobId";
            object parameters = new { JobId = jobId };

            var count = await _dBContext.Connection.ExecuteAsync(sql, parameters, _dBContext.Transaction);

            return count > 0;
        }
    }
}
