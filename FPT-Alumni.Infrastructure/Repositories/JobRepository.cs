﻿using Dapper;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Infrastructure.Repositories
{
    public class JobRepository : BaseRepository<Job>, IJobRepository
    {
        private readonly IDBContext _dBContext;

        public JobRepository(IDBContext dbContext) : base(dbContext)
        {
            _dBContext = dbContext;
        }

        public async Task<List<JobDTO>> GetMyJob(Guid userId, int currentPage, int pageSize)
        {
            var sql = "SELECT j.Id as Id, Title, Content, Position, Salary, Requirement, Benefit, Deadline, Email, FieldId, CompanyDescription, Address, p.Id as PostId"
                + " FROM Job j INNER JOIN Post p on j.PostId = p .id"
                + " WHERE p.CreatedBy = @UserId ORDER BY p.CreatedDate OFFSET @Offset ROWS FETCH NEXT @PageSize ROWS ONLY;";
            var parameters = new DynamicParameters();
            parameters.Add("UserId", userId);
            parameters.Add("Offset", (currentPage - 1) * pageSize);
            parameters.Add("PageSize", pageSize);

            var job = await _dBContext.Connection.QueryAsync<JobDTO>(sql, parameters);
            return job.ToList();
        }

        public async Task<List<JobDTO>> GetJobNeedApprove(int currentPage, int pageSize)
        {
            var sql = "SELECT j.Id as Id, Title, Content, Position, Salary, Requirement, Benefit, Deadline, Email, FieldId, CompanyDescription, Address, p.Id as PostId"
                + " FROM Job j INNER JOIN Post p on j.PostId = p .id AND p.IsPublic = 0"
                + " ORDER BY p.CreatedDate OFFSET @Offset ROWS FETCH NEXT @PageSize ROWS ONLY;";
            var parameters = new DynamicParameters();
            parameters.Add("Offset", (currentPage - 1) * pageSize);
            parameters.Add("PageSize", pageSize);

            var job = await _dBContext.Connection.QueryAsync<JobDTO>(sql, parameters);
            return job.ToList();
        }

        public async Task<List<JobDTO>> GetOtherJob(Guid userId, int currentPage, int pageSize)
        {
            var sql = "SELECT j.Id as Id, Title, Content, Position, Salary, Requirement, Benefit, Deadline, Email, FieldId, CompanyDescription, Address, p.Id as PostId"
                + " FROM Job j INNER JOIN Post p on j.PostId = p .id"
                + " WHERE p.CreatedBy != @UserId ORDER BY p.CreatedDate OFFSET @Offset ROWS FETCH NEXT @PageSize ROWS ONLY;";
            var parameters = new DynamicParameters();
            parameters.Add("UserId", userId);
            parameters.Add("Offset", (currentPage - 1) * pageSize);
            parameters.Add("PageSize", pageSize);

            var job = await _dBContext.Connection.QueryAsync<JobDTO>(sql, parameters);
            return job.ToList();
        }

        public async Task<bool> ApproveJobAsync(Guid postId)
        {
            string sql = "UPDATE [Post] SET IsPublic = 1 WHERE Id = @PostId";
            object parameters = new { PostId = postId };

            var count = await _dBContext.Connection.ExecuteAsync(sql, parameters, _dBContext.Transaction);

            return count > 0;
        }
    }
}
