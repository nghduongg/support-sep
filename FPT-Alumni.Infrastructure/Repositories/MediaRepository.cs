﻿using Dapper;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Infrastructure.Repositories
{
    public class MediaRepository : BaseRepository<Media>, IMediaRepository
    {
        private readonly IDBContext _dBContext;
        public MediaRepository(IDBContext dbContext, IDBContext dBContext) : base(dbContext)
        {
            _dBContext = dBContext;
        }

        public async Task<int> DeleteByPostIdAsync(Guid postId)
        {
            var sql = "DELETE FROM Media WHERE PostId = @postId";
            var parameters = new { postId = postId };
            var res = await _dBContext.Connection.ExecuteAsync(sql, parameters, _dBContext.Transaction);
            return res;
        }

        public async Task<List<Media>> GetByPostIdAsync(Guid postId)
        {
            var sql = "SELECT * FROM Media WHERE PostId = @postId";
            var parameters = new { postId = postId };
            var medias = await _dBContext.Connection.QueryAsync<Media>(sql, parameters, _dBContext.Transaction);
            return medias.ToList();
        }
    }
}
