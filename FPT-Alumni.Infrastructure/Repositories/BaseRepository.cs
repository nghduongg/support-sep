﻿using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Infrastructure.Interfaces;


namespace FPT_Alumni.Infrastructure.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        protected IDBContext _dbContext;
        public BaseRepository(IDBContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public virtual async Task<int> DeleteAsync(Guid id)
        {
            return await _dbContext.DeleteAsync<TEntity>(id);
        }

        public virtual async Task<TEntity?> FindByIdAsync(Guid id)
        {
            return await _dbContext.FindByIdAsync<TEntity>(id);
        }

        public virtual async Task<int> InsertAsync(TEntity entity)
        {
            return await _dbContext.InsertAsync<TEntity>(entity);
        }

        public async Task<bool> IsParamValueExistAsync<T>(TEntity enity, string collumName, T value)
        {
            return await _dbContext.IsParamValueExistAsync(enity, collumName, value);
        }

        public virtual async Task<int> MultipleDeleteAsync(List<TEntity> entities)
        {
            return await _dbContext.MultipleDeleteAsync<TEntity>(entities);
        }

        public virtual async Task<int> MultipleInsertAsync(List<TEntity> entities)
        {
            return await _dbContext.MultipleInsertAsync<TEntity>(entities);
        }

        public virtual async Task<int> MultipleUpdateAsync(List<TEntity> entities)
        {
            return await _dbContext.MultipleUpdateAsync<TEntity>(entities);
        }

        public virtual async Task<int> UpdateAsync(TEntity entity)
        {
            return await _dbContext.UpdateAsync<TEntity>(entity);
        }

    }
}
