﻿using System.Data;


namespace FPT_Alumni.Infrastructure.Interfaces
{
    public interface IDBContext
    {
        IDbConnection Connection { get; }
        IDbTransaction Transaction { get; set; }
        /// <summary>
		/// Find entity by id
		/// </summary>
		/// <param name="id">Entity's id to find </param>
		/// <returns>An Entity with type T or null if not found</returns>
		///  created by: Nguyễn Thiện Thắng
		///  created at: 2023/12/2
        Task<TEntity?> FindByIdAsync<TEntity>(Guid id);

        /// <summary>
		/// Inser entity by Id
		/// </summary>
		/// <param name="entity">Entity to Update </param>
		/// <returns>nummber of record affected</returns>
		///  created by: Nguyễn Thiện Thắng
		///  created at: 05/12/2024
        Task<int> InsertAsync<TEntity>(TEntity entity);

        /// <summary>
        ///  Multiple Insert entity by List of Entity
        /// </summary>
        /// <param name="entities">Entity to Insert </param>
        /// <returns>nummber of record affected</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/12/2024
        Task<int> MultipleInsertAsync<TEntity>(List<TEntity> entities);

        /// <summary>
        /// Update entity by id
        /// </summary>
        /// <param name="entity">Entity to update </param>
        /// <returns>nummber of record affected</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/12/2024
        Task<int> UpdateAsync<TEntity>(TEntity entity);

        /// <summary>
        ///  Multiple update entity by List of Entity
        /// </summary>
        /// <param name="entities">Entity to update </param>
        /// <returns>nummber of record affected</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/12/2024
        Task<int> MultipleUpdateAsync<TEntity>(List<TEntity> entities);

        /// <summary>
        ///  Delete entity by Id
        /// </summary>
        /// <param name="entity">Entity to Delete </param>
        /// <returns>nummber of record affected</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/12/2024#
        Task<int> DeleteAsync<TEntity>(Guid id);

        /// <summary>
        ///  Multiple Delete entity by List of Entity
        /// </summary>
        /// <param name="entity">Entity to Delete </param>
        /// <returns>nummber of record affected</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/12/2024
        Task<int> MultipleDeleteAsync<TEntity>(List<TEntity> entities);

        /// <summary>
        /// Check is value exist in db
        /// </summary>
        /// <param name="enity">entity to get table name</param>
        /// <param name="collumName"> Collumn Name in db</param>
        /// <param name="value">entity's attribute</param>
        /// <returns>
        /// True - value exits
        /// False - value do not exist
        /// </returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/26/2024  
        Task<bool> IsParamValueExistAsync<TEntity, T>(TEntity enity, string collumName, T value);

    }
}
