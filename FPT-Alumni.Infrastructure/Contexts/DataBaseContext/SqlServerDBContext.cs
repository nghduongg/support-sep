﻿using Dapper;
using FPT_Alumni.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace FPT_Alumni.Infrastructure.Contexts.DataBaseContext
{
    public class SqlServerDBContext : IDBContext
    {
        public IDbConnection Connection { get; }
        public IDbTransaction Transaction { get; set; }

        public SqlServerDBContext(IConfiguration config)
        {
            var connectionString = config.GetConnectionString("SqlServer");
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new InvalidOperationException("The ConnectionString property has not been initialized.aa");
            }
            Connection = new SqlConnection(connectionString);
        }

        public async Task<TEntity?> FindByIdAsync<TEntity>(Guid id)
        {
            var tableName = typeof(TEntity).Name;
            var sql = $"SELECT * FROM [{tableName}] WHERE Id = @Id";
            return await this.Connection.QueryFirstOrDefaultAsync<TEntity>(sql, new { Id = id }, Transaction);
        }

        public async Task<int> InsertAsync<TEntity>(TEntity entity)
        {
            var tableName = typeof(TEntity).Name;
            var properties = typeof(TEntity).GetProperties();

            // Đưa thuộc tính Id lên đầu
            var orderedProperties = properties.OrderBy(p => p.Name != "Id").ToArray();

            var columnNames = string.Join(", ", orderedProperties.Select(p => p.Name));
            var parameterNames = string.Join(", ", orderedProperties.Select(p => "@" + p.Name));

            var sql = $"INSERT INTO [{tableName}] ({columnNames}) VALUES ({parameterNames})";
            var res = await this.Connection.ExecuteAsync(sql, entity, Transaction);
            return res;
        }

        public Task<int> MultipleInsertAsync<TEntity>(List<TEntity> entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> UpdateAsync<TEntity>(TEntity entity)
        {
            var tableName = typeof(TEntity).Name;
            var props = typeof(TEntity).GetProperties();
            var propId = props.First(p => p.Name.Equals("Id", StringComparison.OrdinalIgnoreCase));
            var propName = propId.Name;

            var updateProps = props.Where(p => p != propId && p.GetValue(entity) != null);

            if (!updateProps.Any())
            {
                return 0; // Không có thuộc tính nào để cập nhật
            }

            var sql = new StringBuilder($"UPDATE [{tableName}] SET ");

            foreach (var prop in updateProps)
            {
                sql.Append($"{prop.Name} = @{prop.Name}, ");
            }

            // Remove the last comma and space
            sql.Remove(sql.Length - 2, 2);

            // Add WHERE clause for the ID
            sql.Append($" WHERE {propName} = @{propName}");

            return await this.Connection.ExecuteAsync(sql.ToString(), entity, Transaction);
        }

        public Task<int> MultipleUpdateAsync<TEntity>(List<TEntity> entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> DeleteAsync<TEntity>(Guid id)
        {

            // Define the foreign key tables and columns
            var foreignKeyTables = new Dictionary<string, string[]>
            {
                { "Alumni", new[] { "UserId", "CreatedBy", "ModifiedBy" } },
            };

            // Set foreign keys to null
            foreach (var table in foreignKeyTables)
            {
                foreach (var column in table.Value)
                {
                    var updateSql = $"UPDATE [{table.Key}] SET {column} = NULL WHERE {column} = @Id";
                    await this.Connection.ExecuteAsync(updateSql, new { Id = id }, Transaction);
                }
            }

            var tableName = typeof(TEntity).Name;
            var sql = $"DELETE FROM [{tableName}] WHERE Id = @Id";
            return await this.Connection.ExecuteAsync(sql, new { Id = id }, Transaction);
        }

        public Task<int> MultipleDeleteAsync<TEntity>(List<TEntity> entities)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> IsParamValueExistAsync<TEntity, T>(TEntity enity, string collumName, T value)
        {
            var tableName = typeof(TEntity).Name;

            var sql = $@"
        IF EXISTS (SELECT 1 FROM [{tableName}] WHERE {collumName} = '{value}')
        BEGIN
            SELECT 1
        END
        ELSE
        BEGIN
            SELECT 0
        END";
            var result = await this.Connection.QueryFirstOrDefaultAsync<int>(sql, enity, Transaction);

            return result == 1;
        }
    }
}
