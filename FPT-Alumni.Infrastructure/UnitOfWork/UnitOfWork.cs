﻿using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Core.Interfaces.Services;
using FPT_Alumni.Infrastructure.Interfaces;
using FPT_Alumni.Infrastructure.Repositories;


namespace FPT_Alumni.Infrastructure.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        IDBContext _dbContext;

        public IUserRepository User { get; }
        public IRoleRepository Role { get; }
        public IAlumniRepository Alumni { get; }
        public INewsRepository News { get; }
        public INewsCategoryRepository NewsCategory { get; }
        public IFieldRepository Field { get; }
        public IAlumniFieldRepository AlumniField { get; }

        public IPostRepository Post { get; }

        public IMediaRepository Media { get; }

        public ITagRepository Tag { get; }

        public IPostTagRepository PostTag { get; }

        public IPostUserRepository PostUser { get; }

        public IStaffRepository Staff { get; }

        public IMentoringRepository Mentoring { get; }

        public IPostFiledRepository PostFiled { get; }

        public IPostCategoryRepository PostCategory { get; }

        public ICommentRepository Comment { get; }

        public IJobRepository Job { get; }

        public IApplicationRepository Application { get; }

        public UnitOfWork(IDBContext dBContex, IUserRepository userRepository,
            IRoleRepository roleRepository, IAlumniRepository alumniRepository,
            INewsRepository news, INewsCategoryRepository newsCategoryRepository,
            IFieldRepository field, IAlumniFieldRepository alumniFieldRepository, IStaffRepository staffRepository,
            IPostRepository post, IMediaRepository media, ITagRepository tag,
            IPostTagRepository postTag, IPostUserRepository postUser, IMentoringRepository mentoring, IPostFiledRepository postFiled,
            IPostCategoryRepository postCategory, ICommentRepository comment, IJobRepository job, IApplicationRepository application)
        {
            this._dbContext = dBContex;
            User = userRepository;
            Role = roleRepository;
            Alumni = alumniRepository;
            News = news;
            NewsCategory = newsCategoryRepository;
            Field = field;
            AlumniField = alumniFieldRepository;
            Staff = staffRepository;
            Post = post;
            Media = media;
            Tag = tag;
            PostTag = postTag;
            PostUser = postUser;
            Mentoring = mentoring;
            PostFiled = postFiled;
            PostCategory = postCategory;
            Comment = comment;
            Job = job;
            Application = application;
        }

        /// <summary>
        /// Begin a Trasaction
        /// created by: Nguyễn Thiện Thắng
        /// created date: 2023/11/9
        /// </summary>
        public void BeginTransaction()
        {
            _dbContext.Connection.Open();
            _dbContext.Transaction = _dbContext.Connection.BeginTransaction();
        }

        /// <summary>
        /// commit Trasaction's data
        /// created by: Nguyễn Thiện Thắng
        /// created date: 2023/11/9
        /// </summary>
        public void Commit()
        {
            _dbContext.Transaction.Commit();
        }

        /// <summary>
        /// Close connection to db
        /// created by: Nguyễn Thiện Thắng
        /// created date: 2023/11/9
        /// </summary>
        public void Dispose()
        {
            _dbContext.Connection.Close();
        }

        /// <summary>
        /// Roll back Trasaction's data
        /// created by: Nguyễn Thiện Thắng
        /// created date: 2023/11/9
        /// </summary>
        public void RollBack()
        {
            _dbContext.Transaction.Rollback();
        }
    }
}
