﻿using Domain.Exceptions;
using FPT_Alumni.Core.Exceptions;
using Google.Apis.Auth;
using Newtonsoft.Json;

namespace Controller.Middleware
{
    public class ExceptionMiddeware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddeware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            context.Response.Headers.Add("Content-Type", "application/json");
            try
            {
                await _next(context);
            }
            catch (BadRequestException ex)
            {
                context.Response.StatusCode = StatusCodes.Status400BadRequest;
                BaseException serviceResult = new BaseException()
                {
                    ErrorCode = StatusCodes.Status400BadRequest,
                    UserMessage = ex.Message,
                    DevMessage = ex.Message,
                    TraceId = context.TraceIdentifier,
                    MoreInfo = ex.HelpLink,
                };
                var res = JsonConvert.SerializeObject(serviceResult);
                await context.Response.WriteAsync(res);
            }
            catch (NotFoundException ex)
            {
                context.Response.StatusCode = StatusCodes.Status404NotFound;
                BaseException serviceResult = new BaseException()
                {
                    ErrorCode = StatusCodes.Status404NotFound,
                    UserMessage = ex.Message,
                    DevMessage = ex.Message,
                    TraceId = context.TraceIdentifier,
                    MoreInfo = ex.HelpLink,
                };
                var res = JsonConvert.SerializeObject(serviceResult);
                await context.Response.WriteAsync(res);
            }
            catch (ConflictException ex)
            {
                context.Response.StatusCode = StatusCodes.Status409Conflict;
                BaseException serviceResult = new BaseException()
                {
                    ErrorCode =StatusCodes.Status409Conflict,
                    UserMessage = ex.Message,
                    DevMessage = ex.Message,
                    TraceId = context.TraceIdentifier,
                    MoreInfo = ex.HelpLink,
                };
                var res = JsonConvert.SerializeObject(serviceResult);
                await context.Response.WriteAsync(res);
            }
            catch (InternalServerException ex)
            {
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                BaseException serviceResult = new BaseException()
                {
                    ErrorCode = StatusCodes.Status500InternalServerError,
                    DevMessage = ex.Message,
                    UserMessage = "Internal server error.",
                };
                var res = JsonConvert.SerializeObject(serviceResult);
                await context.Response.WriteAsync(res);
            }
            catch (InvalidJwtException ex)
            {
                context.Response.StatusCode = StatusCodes.Status400BadRequest;
                BaseException serviceResult = new BaseException()
                {
                    ErrorCode = StatusCodes.Status400BadRequest,
                    DevMessage = ex.Message,
                    UserMessage = "Invalid Google cridential token.",
                };
                var res = JsonConvert.SerializeObject(serviceResult);
                await context.Response.WriteAsync(res);
            }
            catch (Exception ex)
            {
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                BaseException serviceResult = new BaseException()
                {
                    ErrorCode = StatusCodes.Status500InternalServerError,
                    UserMessage = "Server error",
                    DevMessage = ex.Message,
                    TraceId = context.TraceIdentifier,
                    MoreInfo = ex.HelpLink,
                };
                var res = JsonConvert.SerializeObject(serviceResult);
                await context.Response.WriteAsync(res);
            }

        }
    }
}
