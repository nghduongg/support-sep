﻿using Domain.Exceptions;
using FPT_Alumni.Core.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
namespace FPT_Alumni.API.MiddleWare
{
    public class TokenFromCookies
    {
        private readonly RequestDelegate _next;

        public TokenFromCookies(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, ITokenService tokenService)
        {
            if (context.Request.Path.Value!.Contains("/Auth"))
            {
                await _next(context);
                return;
            }


            if (context.Request.Cookies.ContainsKey("Access-Token"))
            {
                bool flag = false;
                var token = context.Request.Cookies["Access-Token"];
                if (!string.IsNullOrEmpty(token))
                {
                    if (tokenService!.IsTokenValid(token))
                    {
                        flag = true;
                        context.Request.Headers.Add("Authorization", $"Bearer {token}");
                    }
                }

                if (!flag)
                {
                    context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                    var serviceResult = new
                    {
                        ErrorCode = StatusCodes.Status401Unauthorized,
                        DevMessage = "Token has expired.",
                        Data = string.Empty
                    };
                    var res = JsonConvert.SerializeObject(serviceResult);
                    context.Response.ContentType = "application/json";
                    await context.Response.WriteAsync(res);
                    return;
                }
            }
            else
            {
                if (context.Request.Path.Value!.Contains("/User/GetUserByToken"))
                {
                    context.Response.StatusCode = StatusCodes.Status200OK;
                    var serviceResult = new
                    {
                        ErrorCode = StatusCodes.Status200OK,
                        DevMessage = "User not login or access token is expired",
                        Data = string.Empty
                    };
                    var res = JsonConvert.SerializeObject(serviceResult);
                    context.Response.ContentType = "application/json";
                    await context.Response.WriteAsync(res);
                    return;
                }
                else
                {
                    context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                    var serviceResult = new
                    {
                        ErrorCode = StatusCodes.Status401Unauthorized,
                        DevMessage = "User not login or access token is expired",
                        Data = string.Empty
                    };
                    var res = JsonConvert.SerializeObject(serviceResult);
                    context.Response.ContentType = "application/json";
                    await context.Response.WriteAsync(res);
                    return;
                }

            }
            await _next(context);
        }
    }
}
