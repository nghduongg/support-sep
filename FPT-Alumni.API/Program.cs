﻿using Controller.Middleware;
using Domain.Exceptions;
using FPT_Alumni.API.MiddleWare;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Core.Interfaces.Services;
using FPT_Alumni.Core.Services;
using FPT_Alumni.Core.Validations;
using FPT_Alumni.Infrastructure.Contexts.DataBaseContext;
using FPT_Alumni.Infrastructure.Interfaces;
using FPT_Alumni.Infrastructure.Repositories;
using FPT_Alumni.Infrastructure.UnitOfWork;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Text;

var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
var builder = WebApplication.CreateBuilder(args);
ConfigurationManager configuration = builder.Configuration;

//config add HttpContext to services
builder.Services.AddHttpContextAccessor();

// Naming policy
builder.Services.AddControllers().AddJsonOptions(o =>
{
    o.JsonSerializerOptions.PropertyNamingPolicy = null;
    o.JsonSerializerOptions.PropertyNamingPolicy = null;
});

builder.Services.AddControllers().AddNewtonsoftJson();

// Add services to the container.
builder.Services.AddControllers()
    .ConfigureApiBehaviorOptions(
    options =>
    {
        options.InvalidModelStateResponseFactory =
        context =>
        {
            var errros = context.ModelState.Values
            .SelectMany(x => x.Errors);
            return new BadRequestObjectResult(
                new BaseException()
                {
                    ErrorCode = 400,
                    UserMessage = "Read data from resource",
                    DevMessage = "Read data from resource",
                    TraceId = "",
                    MoreInfo = "",
                    Errors = errros
                }.ToString() ?? "");
        };
    }).AddJsonOptions(options =>
    {
        options.JsonSerializerOptions.PropertyNamingPolicy = null;
    });

// Adding Authentication
builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(x =>
{
    x.RequireHttpsMetadata = false;
    x.SaveToken = true;
    x.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"])),
        //ValidateIssuer = false,
        //ValidateAudience = false
        ValidateIssuer = true,
        ValidateAudience = true,
        ValidateLifetime = true,
        ValidIssuer = configuration["JWT:Issuer"],
        ValidAudience = configuration["JWT:Audience"],
    };
});

// add authorize
builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("AdminPolicy", policy => policy.RequireRole("Admin"));
    options.AddPolicy("StaffPolicy", policy => policy.RequireRole("Staff"));
    options.AddPolicy("AlumniPolicy", policy => policy.RequireRole("Alumni"));
});

// CORS
builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowSpecificOrigin",
        builder =>
        {
            builder
                .WithOrigins("http://localhost:4200")
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials();
        });
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "API", Version = "v1" });
    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        In = ParameterLocation.Header,
        Description = "Vui lòng nhập JWT với Bearer vào ô",
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey,
        BearerFormat = "JWT",
        Scheme = "Bearer"
    });
    c.AddSecurityRequirement(new OpenApiSecurityRequirement {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                    },
                    new string[] { }
                }
            });
});

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

// DI Config

builder.Services.AddScoped(typeof(IDBContext), typeof(SqlServerDBContext));
builder.Services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
builder.Services.AddScoped(typeof(IUserRepository), typeof(UserRepository));
builder.Services.AddScoped(typeof(IUserService), typeof(UserService));
builder.Services.AddScoped(typeof(ITokenService), typeof(TokenService));
builder.Services.AddScoped(typeof(ICommonService), typeof(CommonService));
builder.Services.AddScoped(typeof(IAuthService), typeof(AuthService));
builder.Services.AddScoped(typeof(IRoleRepository), typeof(RoleRepository));
builder.Services.AddScoped<IRoleService, RoleService>();
builder.Services.AddScoped(typeof(IAlumniService), typeof(AlumniService));
builder.Services.AddScoped(typeof(IAlumniRepository), typeof(AlumniRepository));
builder.Services.AddScoped(typeof(IUnitOfWork), typeof(UnitOfWork));
builder.Services.AddScoped(typeof(INewsService), typeof(NewsService));
builder.Services.AddScoped(typeof(INewsRepository), typeof(NewsRepository));
builder.Services.AddScoped(typeof(INewsCategoryService), typeof(NewsCategoryService));
builder.Services.AddScoped(typeof(INewsCategoryRepository), typeof(NewsCategoryRepository));
builder.Services.AddScoped(typeof(IFileValidation), typeof(FileValidation));
builder.Services.AddScoped(typeof(IAlumniService), typeof(AlumniService));
builder.Services.AddScoped(typeof(ICacheRepository), typeof(CacheRepository));
builder.Services.AddScoped(typeof(IFieldRepository), typeof(FieldRepository));
builder.Services.AddScoped(typeof(IFieldService), typeof(FieldService));
builder.Services.AddScoped(typeof(IAlumniFieldRepository), typeof(AlumniFieldRepository));
builder.Services.AddScoped(typeof(IPostService), typeof(PostService));
builder.Services.AddScoped(typeof(IPostRepository), typeof(PostRepository));
builder.Services.AddScoped(typeof(IMediaRepository), typeof(MediaRepository));
builder.Services.AddScoped(typeof(ITagRepository), typeof(TagRepository));
builder.Services.AddScoped(typeof(ITagService), typeof(TagService));
builder.Services.AddScoped(typeof(IPostFiledRepository), typeof(PostFiledRepository));
builder.Services.AddScoped(typeof(IPostTagRepository), typeof(PostTagRepository));
builder.Services.AddScoped(typeof(IPostUserRepository), typeof(PostUserRepository));
builder.Services.AddScoped(typeof(IStaffRepository), typeof(StaffRepository));
builder.Services.AddScoped(typeof(IStaffService), typeof(StaffService));
builder.Services.AddScoped(typeof(ICommentService), typeof(CommentService));
builder.Services.AddScoped(typeof(ICommentRepository), typeof(CommentRepository));
builder.Services.AddScoped(typeof(IMentoringRepository), typeof(MentoringRepository));
builder.Services.AddScoped(typeof(IMentoringService), typeof(MentoringService));
builder.Services.AddScoped(typeof(IPostCategoryRepository), typeof(PostCategoryRepository));
builder.Services.AddScoped(typeof(IPostCategoryService), typeof(PostCategoryService));
builder.Services.AddScoped(typeof(IJobService), typeof(JobService));
builder.Services.AddScoped(typeof(IJobRepository), typeof(JobRepository));
builder.Services.AddScoped(typeof(IApplicationService), typeof(ApplicationService));
builder.Services.AddScoped(typeof(IApplicationRepository), typeof(ApplicationRepository));

builder.Services.AddMemoryCache();
var app = builder.Build();



// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");
    }
        );
}


app.UseHttpsRedirection();
app.UseCors("AllowSpecificOrigin");
app.UseMiddleware<TokenFromCookies>(); // Custom middleware

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

//MiddleWare
app.UseMiddleware<ExceptionMiddeware>();
//app.UseRequestCulture();

app.Run();
