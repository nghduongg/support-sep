﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Interfaces.Services;
using FPT_Alumni.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPT_Alumni.API.Controller.Mentoring
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class MentoringController : ControllerBase
    {
        readonly IMentoringService _mentoringService;
        public MentoringController(IMentoringService mentoringService)
        {
            _mentoringService = mentoringService;
        }

        [HttpPost("BecomeAMentee")]
        public async Task<IActionResult> SearchMentor([FromBody] MentoringSearchDTO mentoringSave)
        {
            var result = await _mentoringService.CreateMentoringServiceAsync(mentoringSave);
            return StatusCode((int)result.StatusCode!, result);
        }

        [HttpPost("BecomeAMentor")]
        public async Task<IActionResult> SaveMentoring([FromBody] MentoringSaveDTO mentoringSave)
        {
            var result = await _mentoringService.CreateMentoringServiceAsync(mentoringSave);
            return StatusCode((int)result.StatusCode!, result);
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> GetMentoring(Guid Id)
        {
            var result = await _mentoringService.GetMentoringByIdServiceAsync(Id);
            return StatusCode((int)result.StatusCode!, result);
        }

        [HttpGet("GetMentoringForm")]
        public async Task<IActionResult> GetMentoringForm(string mentoringRole)
        {
            var result = await _mentoringService.GetMentoringFormServiceAsync( mentoringRole);
            return StatusCode((int)result.StatusCode!, result);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteMentoring(Guid id)
        {
            var result = await _mentoringService.DeleteMentoringServiceAsync(id);
            return StatusCode((int)result.StatusCode!, result);
        }

        [HttpGet("FilterMentors")]
        public async Task<IActionResult> FilterMentoringProfiles([FromQuery] MentoringFilterDTO filter)
        {
            var result = await _mentoringService.FilterMentorAsync(filter);
            return StatusCode((int)result.StatusCode!, result);
        }

        [HttpPost("Report")]
        public async Task<IActionResult> SaveMentoringReport([FromBody] MentoringReportSaveDTO filter)
        {
            var result = await _mentoringService.SaveMentoringReportServiceAsync(filter);
            return StatusCode((int)result.StatusCode!, result);
        }

        [HttpGet("Reports")]
        public async Task<IActionResult> GetMentoringReports()
        {
            var result = await _mentoringService.GetMentoringReportsServiceAsync();
            return StatusCode((int)result.StatusCode!, result);
        }

        [HttpGet("Reports/{Id}")]
        public async Task<IActionResult> GetMentoringReports(Guid Id)
        {
            var result = await _mentoringService.GetMentoringReportByIdServiceAsync(Id);
            return StatusCode((int)result.StatusCode!, result);
        }
    }
}
