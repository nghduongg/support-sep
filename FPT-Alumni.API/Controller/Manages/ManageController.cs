﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FPT_Alumni.API.Controller.Staffs
{
    [Authorize(Roles = "Admin,Staff")]
    [Route("api/[controller]")]
    [ApiController]
    public class ManageController : ControllerBase
    {
        IUserService _userService;
        IAlumniService _alumniService;
        IFieldService _fieldService;
        IStaffService _staffService;

        public ManageController(IUserService userService, IAlumniService alumniService,
            IFieldService fieldService, IStaffService staffService)
        {
            _userService = userService;
            _alumniService = alumniService;
            _fieldService = fieldService;
            _staffService = staffService;
        }

        [HttpGet("Alumni")]
        public async Task<IActionResult> GetAlumniList()
        {
            var res = await _alumniService.GetAlumniListAsync();
            return StatusCode((int)res.StatusCode!, res);
        }

        [HttpGet("GetAlumniesByStatus")]
        public async Task<IActionResult> GetAlumniesByStatus(string? status)
        {
            var res = await _alumniService.GetAlumniByStatusAsync(status);
            return StatusCode((int)res.StatusCode!, res);
        }

        [HttpPut("Alumni/Approval")]
        public async Task<IActionResult> ApproveRole([FromBody] UserStatusUpdateDTO userStatusUpdateDTO)
        {
            var res = await _userService.ApprovingRoleServiceAsync(userStatusUpdateDTO);
            return StatusCode((int)res.StatusCode!, res);
        }

        [AllowAnonymous]
        [HttpGet("Field")]
        public async Task<IActionResult> GetFieldList()
        {
            var res = await _fieldService.GetFieldListServiceAsync();
            return StatusCode((int)res.StatusCode!, res);
        }

        [HttpPost("Field")]
        public async Task<IActionResult> AddField([FromBody] FieldSaveDTO fieldSave)
        {
            var res = await _fieldService.AddFieldServiceAsync(fieldSave);
            return StatusCode((int)res.StatusCode!, res);
        }

        [HttpPut("Field")]
        public async Task<IActionResult> UpdateField([FromBody] FieldSaveDTO fieldSave)
        {
            var res = await _fieldService.UpdateFieldServiceAsync(fieldSave);
            return StatusCode((int)res.StatusCode!, res);
        }

        [HttpDelete("Field")]
        public async Task<IActionResult> DeleteField([FromBody] Guid fieldId)
        {
            var res = await _fieldService.DeleteServiceAsync(fieldId);
            return StatusCode((int)res.StatusCode!, res);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("Staff")]
        public async Task<IActionResult> GetStaffList()
        {
            var res = await _staffService.GetStaffListServiceAsync();
            return StatusCode((int)res.StatusCode!, res);
        }
        [Authorize(Roles = "Admin")]
        [HttpDelete("Staff")]
        public async Task<IActionResult> DeleteStaff([FromBody] Guid userId)
        {
            var res = await _staffService.DeleteStaffServiceAsync(userId);
            return StatusCode((int)res.StatusCode!, res);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("Staff")]
        public async Task<IActionResult> SaveStaff([FromBody] StaffWithUserModelView staffSaveDTO)
        {
            var res = await _staffService.SaveStaffServiceAsync(staffSaveDTO);
            return StatusCode((int)res.StatusCode!, res);
        }

        [HttpPost("Alumni")]
        public async Task<IActionResult> SaveAlumni([FromBody] UserModelSave alumniSaveDTO)
        {
            var res = await _alumniService.SaveAlumniAsync(alumniSaveDTO);
            return StatusCode((int)res.StatusCode!, res);
        }

        [HttpDelete("Alumni")]
        public async Task<IActionResult> DeleteAlumni([FromBody] Guid userId)
        {
            var res = await _alumniService.DeleteAlumniServiceAsync(userId);
            return StatusCode((int)res.StatusCode!, res);
        }


        [HttpGet("Alumni/{userId}")]
        public async Task<IActionResult> GetUserDetailAsync(Guid userId)
        {
            var res = await _alumniService.GetAlumniByUserIdServiceAsync(userId);
            return StatusCode((int)res.StatusCode!, res);
        }

        [HttpPut("Alumni/ApprovalMany")]
        public async Task<IActionResult> ApproveRoles([FromBody] List<UserStatusUpdateDTO> userStatusUpdateDTO)
        {
            var res = await _userService.ApprovingRolesServiceAsync(userStatusUpdateDTO);
            return StatusCode((int)res.StatusCode!, res);
        }
    }
}
