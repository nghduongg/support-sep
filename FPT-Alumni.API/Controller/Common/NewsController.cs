﻿using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Services;
using FPT_Alumni.Core.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPT_Alumni.API.Controller.Common
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewsController : ControllerBase
    {
        INewsService _newService;

        public NewsController(INewsService newService)
        {
            _newService = newService;
        }

        /// <summary>
        /// Find news by id 
        /// </summary>
        /// <param name="id">Id for news searching</param>
        /// <returns>news with specific id or null</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/22/2024
        [HttpGet]
        public async Task<IActionResult> FindById(Guid id)
        {
            var res = await _newService.FindByIdServiceAsync(id);
            return StatusCode((int)res.StatusCode, res);
        }

        /// <summary>
        /// Create new news and insert into Db
        /// </summary>
        /// <param name="news">News to insert</param>
        /// <returns>number of inserted record and status code</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/22/2024
        [HttpPost]
        public async Task<IActionResult> CreateNews(News news)
        {
            var res = await _newService.InsertServiceAsync(news);
            return StatusCode((int)res.StatusCode, res);
        }

        /// <summary>
        /// Create new News and insert into Db
        /// </summary>
        /// <param name="news">News to Update</param>
        /// <returns>number of updated record and status code</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/22/2024
        [HttpPut]
        public async Task<IActionResult> UpdateNews(News news)
        {
            var res = await _newService.UpdateServiceAsync(news);
            return StatusCode((int)res.StatusCode, res);
        }
    }
}
