﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace FPT_Alumni.API.Controller.Common
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authenticationService;
        readonly ITokenService _tokenService;
        public AuthController(IAuthService authenticationService, ITokenService tokenService)
        {
            _authenticationService = authenticationService;
            _tokenService = tokenService;
        }

        /// <summary>
        /// Signs in a user using a single credential (e.g., email, fullname).
        /// </summary>
        /// <param name="credential">The credential for authentication.</param>
        /// <returns>An IActionResult containing the authentication result.</returns>
        /// created by: Nguyễn Đình Trường
        ///  created at: 05/23/2024
        [HttpPost("SigninByCredential")]
        public async Task<IActionResult> SigninByCredential([FromBody] string credential)
        {
            if (string.IsNullOrEmpty(credential))
            {
                return BadRequest("Credential is required.");
            }
            var result = await _authenticationService.AuthenticateAsync(credential);
            return StatusCode((int)result.StatusCode!, result);
        }

        /// <summary>
        /// Signs in a user using a login model containing credentials.
        /// </summary>
        /// <param name="loginModel">The model containing the login credentials.</param>
        /// <returns>An IActionResult containing the authentication result.</returns>
        /// created by: Nguyễn Đình Trường
        ///  created at: 05/23/2024
        [HttpPost("SigninByLoginModel")]
        public async Task<IActionResult> SigninByLoginModel([FromBody] LoginModel loginModel)
        {
            var result = await _authenticationService.AuthenticateAsync(loginModel);
            return StatusCode((int)result.StatusCode!, result);
        }

        /// <summary>
        /// Signs up a new user with the provided unverified model.
        /// </summary>
        /// <param name="unauthenticatedModel">The model containing user information for registration.</param>
        /// <returns>An IActionResult containing the registration result.</returns>
        /// created by: Nguyễn Đình Trường
        ///  created at: 05/23/2024
        [HttpPost("Signup")]
        public async Task<IActionResult> Signup([FromBody] UnverifiedModel unauthenticatedModel)
        {
            var result = await _authenticationService.RegisterServiceAsync(unauthenticatedModel);
            return StatusCode((int)result.StatusCode!, result);
        }

        /// <summary>
        /// Refreshes the user's access token using the refresh token stored in cookies.
        /// </summary>
        /// <returns>An IActionResult containing the token refresh result.</returns>
        /// created by: Nguyễn Đình Trường
        ///  created at: 05/23/2024
        [HttpPost("RefreshToken")]
        public async Task<IActionResult> RefreshToken()
        {
            // Get the cookie header
            string cookieHeader = Request.Headers["Cookie"].ToString();

            // Parse the cookie header to extract individual cookies
            var cookies = cookieHeader.Split(',')
                                      .Select(cookie => cookie.Split('='))
                                      .ToDictionary(cookie => cookie[0].Trim(), cookie => cookie.Length > 1 ? cookie[1].Trim() : string.Empty);

            cookies.TryGetValue("Refresh-Token", out var refreshToken);

            var result = await _tokenService.RefreshServiceAsync(refreshToken);
            return StatusCode((int)result.StatusCode!, result);
        }

        /// <summary>
        /// Logs out the user by invalidating the refresh token stored in cookies.
        /// </summary>
        /// <returns>An IActionResult containing the logout result.</returns>
        /// created by: Nguyễn Đình Trường
        ///  created at: 05/23/2024
        [HttpPost("Logout")]
        public async Task<IActionResult> Logout()
        {
            // Get the cookie header
            string cookieHeader = Request.Headers["Cookie"].ToString();
            // Parse the cookie header to extract individual cookies
            var cookies = cookieHeader.Split(',')
                          .Select(cookie => cookie.Trim().Split('='))
                          .ToDictionary(cookie => cookie[0], cookie => Uri.UnescapeDataString(cookie[1]));

            cookies.TryGetValue("Refresh-Token", out var refreshToken);

            var result = await _tokenService.RefreshServiceAsync(refreshToken, "logout");
            return StatusCode((int)result.StatusCode!, result);
        }

        [HttpGet("GetTokenForgot")]
        public async Task<IActionResult> GetTokenForgot(Guid userId)
        {
            var token = _tokenService.GenerateChangePasswordToken(userId);

            return StatusCode((int)HttpStatusCode.OK, token);
        }

      

    }
}
