﻿using AutoMapper;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Services;
using FPT_Alumni.Core.JsonModels;
using FPT_Alumni.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml.FormulaParsing.LexicalAnalysis;
using System.Net;
using System.Security.Claims;

namespace FPT_Alumni.API.Controller.Common
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        IUserService _userService;
        readonly IMapper _mapper;
        readonly ITokenService _tokenService;

        public UserController(IUserService userService, IMapper mapper, ITokenService tokenService)
        {
            _userService = userService;
            _mapper = mapper;
            _tokenService = tokenService;
        }



        /// <summary>
        /// Get All information of alumni with email, role (from User table)
        /// </summary>
        /// <returns>A model view with all information of alumni</returns>
        ///  created by: Nguyễn Đình Trường
        ///  created at: 05/28/2024
        [HttpGet("Settings")]
        public async Task<IActionResult> Settings()
        {
            // Get the cookie header
            string cookieHeader = Request.Headers["Cookie"].ToString();

            // Parse the cookie header to extract individual cookies
            var cookies = cookieHeader.Split(',')
                                      .Select(cookie => cookie.Split('='))
                                      .ToDictionary(cookie => cookie[0].Trim(), cookie => cookie.Length > 1 ? cookie[1].Trim() : string.Empty);

            cookies.TryGetValue("Access-Token", out var accessToken);

            var result = await _userService.GetUserServiceAsync(accessToken);
            return StatusCode((int)result.StatusCode!, result);
        }

        /// <summary>
        /// Update information of alumni when submit a tab
        /// </summary>
        ///  <param name="alumniWithUserModel">alumniWithUserModel contains all field of alumni and user Update</param>
        /// <returns>status updated</returns>
        ///  created by: Nguyễn Đình Trường
        ///  created at: 05/28/2024
        [HttpPut("Settings")]
        public async Task<IActionResult> UpdateSettings([FromBody] UserModelSave alumniWithUserModel)
        {
            string cookieHeader = Request.Headers["Cookie"].ToString();
            // Parse the cookie header to extract individual cookies
            var cookies = cookieHeader.Split(',')
                                      .Select(cookie => cookie.Split('='))
                                      .ToDictionary(cookie => cookie[0].Trim(), cookie => cookie.Length > 1 ? cookie[1].Trim() : string.Empty);
            cookies.TryGetValue("Access-Token", out var accessToken);

            var result = await _userService.SettingUpdateServiceAsync(alumniWithUserModel, accessToken!);
            return StatusCode((int)result.StatusCode!, result);
        }

        /// <summary>
        /// Insert new work experience in alumni table to db by json string
        /// </summary>
        ///  <param name="user">workExperience contains userid and all field of work experience to insert</param>
        /// <returns>status inserted</returns>
        ///  created by: Nguyễn Đình Trường
        ///  created at: 05/28/2024
        [HttpPost("Settings/WorkExperience")]
        public async Task<IActionResult> AddNewWorkExperience([FromBody] WorkExperienceSaveDTO workExperience)
        {
            var result = await _userService.CreateNewWorkExperienceServiceAsync(workExperience);
            return StatusCode((int)result.StatusCode!, result);
        }

        /// <summary>
        /// Insert new education experience in alumni table to db by json string
        /// </summary>
        ///  <param name="educationExperienceSaveDTO">educationExperienceSaveDTO contains userid and all field of education experience to insert</param>
        /// <returns>status inserted</returns>
        ///  created by: Nguyễn Đình Trường
        ///  created at: 05/28/2024
        [HttpPost("Settings/EducationExperience")]
        public async Task<IActionResult> AddNewEducationExperience([FromBody] EducationExperienceSaveDTO educationExperienceSaveDTO)
        {
            var result = await _userService.CreateNewEducationExperienceServiceAsync(educationExperienceSaveDTO);
            return StatusCode((int)result.StatusCode!, result);
        }


        [AllowAnonymous]
        [HttpPost("ForgotPassword")]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordRequest request)
        {
            var result = await _userService.GetUserByEmailServiceAsync(request.Email);
            return StatusCode((int)result.StatusCode!, result);
        }

        /// <summary>
        /// Display change password interface
        /// </summary>
        /// <param name="token">token jwt contain userId and it's expires</param>
        /// <returns>display change password screeen or error</returns>
        ///  created by: Nguyễn Đình Trường
        ///  created at: 05/28/2024
        [AllowAnonymous]
        [HttpPost("CheckTokenForgotPassword")]
        public async Task<IActionResult> CheckTokenForgotPassword(string token)
        {
            var result = new ServiceResult();

            ClaimsPrincipal principal;
            try
            {
                principal = await _tokenService.GetPrincipalFromExpiredTokenAsync(token);
            }
            catch
            {
                result.StatusCode = HttpStatusCode.BadRequest;
                result.DevMsg = "Token has expired";
                return StatusCode((int)result.StatusCode, result);
            }

            var userIdClaim = principal.FindFirst("Id");
            if (userIdClaim == null)
            {
                result.StatusCode = HttpStatusCode.BadRequest;
                result.DevMsg = "Invalid token";
                return StatusCode((int)result.StatusCode, result);
            }

            var userId = Guid.Parse(userIdClaim.Value);
            result.StatusCode = HttpStatusCode.OK;
            result.Data = new ChangePasswordModel { Id = userId };
            result.Success = true;

            return StatusCode((int)result.StatusCode, result);
        }


        /// <summary>
        /// Change password
        /// </summary>
        /// <param name="changePasswordModel">change password of user by userid</param>
        /// <returns>change password status</returns>
        ///  created by: Nguyễn Đình Trường
        ///  created at: 05/28/2024
        [AllowAnonymous]
        [HttpPost("ChangePasswordByForgot")]
        public async Task<IActionResult> ChangePasswordByForgot([FromBody] ChangePasswordModel changePasswordModel)
        {
            var result = new ServiceResult();

            ClaimsPrincipal principal;
            try
            {
                principal = await _tokenService.GetPrincipalFromExpiredTokenAsync(changePasswordModel.token!);
            }
            catch
            {
                result.StatusCode = HttpStatusCode.Unauthorized;
                result.DevMsg = "Token has expired";
                result.Data = null;
                result.Success = false;
                return StatusCode((int)result.StatusCode, result);
            }

             result = await _userService.ChangePasswordServiceAsync(changePasswordModel);
            return StatusCode((int)result.StatusCode!, result);
        }

        /// <summary>
        /// Change password
        /// </summary>
        /// <param name="changePasswordModel">change password of user by userid</param>
        /// <returns>change password status</returns>
        ///  created by: Nguyễn Đình Trường
        ///  created at: 05/28/2024
        [AllowAnonymous]
        [HttpPost("ChangePasswordBySetting")]
        public async Task<IActionResult> ChangePasswordBySetting([FromBody] ChangePasswordModel changePasswordModel)
        {
            var result = await _userService.ChangePasswordServiceAsync(changePasswordModel);
            return StatusCode((int)result.StatusCode!, result);
        }

        /// <summary>
        /// Insert/update new privacy settings in alumni table to db by json string
        /// </summary>
        ///  <param name="privacySettingsSaveDTO">privacySettingsSaveDTO contains userid and all field of privacy Setting to insert</param>
        /// <returns>status inserted</returns>
        ///  created by: Nguyễn Đình Trường
        ///  created at: 05/28/2024
        [HttpPost("Settings/PrivacySettings")]
        public async Task<IActionResult> AddNewPrivacySettings([FromBody] PrivacySettingsSaveDTO privacySettingsSaveDTO)
        {
            var result = await _userService.CreateNewPrivacySettingsServiceAsync(privacySettingsSaveDTO);
            return StatusCode((int)result.StatusCode, result);
        }

        /// <summary>
        /// Update and delete experience in alumni table to db by json string
        /// </summary>
        ///  <param name="user">workExperience contains userid and all field of work experience to insert</param>
        /// <returns>status inserted</returns>
        ///  created by: Nguyễn Đình Trường
        ///  created at: 05/28/2024
        [HttpPut("Settings/WorkExperience")]
        public async Task<IActionResult> UpdateWorkExperience([FromBody]UpdateWorkExperienceDTO updateWorkExperienceDTO)
        {
            var result = await _userService.UpdateWorkExperienceServiceAsync(updateWorkExperienceDTO);
            return StatusCode((int)result.StatusCode!, result);
        }

        /// <summary>
        /// update/ delete education experience in alumni table to db by json string
        /// </summary>
        ///  <param name="educationExperienceSaveDTO">educationExperienceSaveDTO contains userid and all field of education experience to insert</param>
        /// <returns>status inserted</returns>
        ///  created by: Nguyễn Đình Trường
        ///  created at: 05/28/2024
        [HttpPut("Settings/EducationExperience")]
        public async Task<IActionResult> UpdateEducationExperience([FromBody] UpdateEducationExperienceDTO updateEducationExperienceDTO)
        {
            var result = await _userService.UpdateEducationExperienceServiceAsync(updateEducationExperienceDTO);
            return StatusCode((int)result.StatusCode!, result);
        }

        [HttpGet("GetUserByToken")]
        public async Task<IActionResult> GetUserByToken()
        {
            // Get the cookie header
            string cookieHeader = Request.Headers["Cookie"].ToString();
            // Parse the cookie header to extract individual cookies
            var cookies = cookieHeader.Split(',')
                          .Select(cookie => cookie.Trim().Split('='))
                          .ToDictionary(cookie => cookie[0], cookie => Uri.UnescapeDataString(cookie[1]));

            cookies.TryGetValue("Access-Token", out var accessToken);
            var result = await _userService.GetLoggedUser(accessToken);

            return StatusCode((int)result.StatusCode!, result);
        }
    }
}
