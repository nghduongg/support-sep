﻿using FPT_Alumni.Core.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPT_Alumni.API.Controller.Field
{
    [Route("api/[controller]")]
    [ApiController]
    public class FiledController : ControllerBase
    {
        IFieldService _fieldService;

        public FiledController(IFieldService fieldService)
        {
            _fieldService = fieldService;
        }

        /// <summary>
        /// Get all field
        /// </summary>
        /// <returns>All Field</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 06/23/2024
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var res = await _fieldService.GetAllServiceAsync();
            return StatusCode((int)res.StatusCode, res);
        }

        /// <summary>
        /// Find Field by key word form user
        /// </summary>
        /// <param name="key">Search key words</param>
        /// <returns>List of field match wih key words</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/27/2024
        [HttpGet("search")]
        public async Task<IActionResult> GetFieldByKeyWord(string key)
        {
            var res = await _fieldService.SearchServiceAsync(key);
            return StatusCode((int)res.StatusCode, res);
        }
    }
}
