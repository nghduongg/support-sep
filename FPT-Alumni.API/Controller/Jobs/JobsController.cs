﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Services;
using FPT_Alumni.Core.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPT_Alumni.API.Controller.Jobs
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobsController : ControllerBase
    {
        readonly IJobService _jobService;
        public JobsController(IJobService jobService)
        {
            _jobService = jobService;
        }

        [HttpPost]
        public async Task<IActionResult> AddNewJobAsync([FromBody] JobDTO job)
        {
            string cookieHeader = Request.Headers["Cookie"].ToString();
            // Parse the cookie header to extract individual cookies
            var cookies = cookieHeader.Split(',')
                                      .Select(cookie => cookie.Split('='))
                                      .ToDictionary(cookie => cookie[0].Trim(), cookie => cookie.Length > 1 ? cookie[1].Trim() : string.Empty);
            cookies.TryGetValue("Access-Token", out var accessToken);
            var result = await _jobService.AddNewJobAsync(job, accessToken);
            return StatusCode((int)result.StatusCode!, result);
        }

        [HttpGet("GetMyJob")]
        public async Task<IActionResult> GetMyJob(int currentPage, int pageSize)
        {
            string cookieHeader = Request.Headers["Cookie"].ToString();
            // Parse the cookie header to extract individual cookies
            var cookies = cookieHeader.Split(',')
                                      .Select(cookie => cookie.Split('='))
                                      .ToDictionary(cookie => cookie[0].Trim(), cookie => cookie.Length > 1 ? cookie[1].Trim() : string.Empty);
            cookies.TryGetValue("Access-Token", out var accessToken);
            var result = await _jobService.GetMyJob(accessToken, currentPage, pageSize);
            return StatusCode((int)result.StatusCode!, result);
        }

        [HttpGet("GetJobNeedApprove")]
        public async Task<IActionResult> GetJobNeedApprove(int currentPage, int pageSize)
        {
            string cookieHeader = Request.Headers["Cookie"].ToString();
            // Parse the cookie header to extract individual cookies
            var cookies = cookieHeader.Split(',')
                                      .Select(cookie => cookie.Split('='))
                                      .ToDictionary(cookie => cookie[0].Trim(), cookie => cookie.Length > 1 ? cookie[1].Trim() : string.Empty);
            cookies.TryGetValue("Access-Token", out var accessToken);
            var result = await _jobService.GetJobNeedApprove(accessToken, currentPage, pageSize);
            return StatusCode((int)result.StatusCode!, result);
        }

        [HttpGet("GetOtherJob")]
        public async Task<IActionResult> GetOtherJob(int currentPage, int pageSize)
        {
            string cookieHeader = Request.Headers["Cookie"].ToString();
            // Parse the cookie header to extract individual cookies
            var cookies = cookieHeader.Split(',')
                                      .Select(cookie => cookie.Split('='))
                                      .ToDictionary(cookie => cookie[0].Trim(), cookie => cookie.Length > 1 ? cookie[1].Trim() : string.Empty);
            cookies.TryGetValue("Access-Token", out var accessToken);
            var result = await _jobService.GetOtherJob(accessToken, currentPage, pageSize);
            return StatusCode((int)result.StatusCode!, result);
        }

        [HttpPut]
        public async Task<IActionResult> ApproveJobAsync(Guid postId)
        {
            var result = await _jobService.ApprovingJobServiceAsync(postId);
            return StatusCode((int)result.StatusCode!, result);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteJobAsync(Guid postId, Guid jobId)
        {
            var result = await _jobService.DeleteJobAsync(postId, jobId);
            return StatusCode((int)result.StatusCode!, result);
        }
    }
}
