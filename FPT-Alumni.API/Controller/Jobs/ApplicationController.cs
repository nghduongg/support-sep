﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Services;
using FPT_Alumni.Core.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPT_Alumni.API.Controller.Jobs
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicationController : ControllerBase
    {
        readonly IApplicationService _applicationService;
        public ApplicationController(IApplicationService applicationService)
        {
            _applicationService = applicationService;
        }

        [HttpPost]
        public async Task<IActionResult> AddNewApplicationAsync([FromBody] Application application)
        {
            string cookieHeader = Request.Headers["Cookie"].ToString();
            // Parse the cookie header to extract individual cookies
            var cookies = cookieHeader.Split(',')
                                      .Select(cookie => cookie.Split('='))
                                      .ToDictionary(cookie => cookie[0].Trim(), cookie => cookie.Length > 1 ? cookie[1].Trim() : string.Empty);
            cookies.TryGetValue("Access-Token", out var accessToken);
            var result = await _applicationService.AddNewApplicationAsync(application, accessToken);
            return StatusCode((int)result.StatusCode!, result);
        }

        [HttpGet("GetAllCandidateByJobId")]
        public async Task<IActionResult> GetMyJob(Guid jobId, int currentPage, int pageSize)
        {
            var result = await _applicationService.GetAllCandidateByJobId(jobId, currentPage, pageSize);
            return StatusCode((int)result.StatusCode!, result);
        }
    }
}
