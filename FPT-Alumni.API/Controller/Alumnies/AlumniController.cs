﻿using AutoMapper;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Services;
using FPT_Alumni.Core.Services;
using FPT_Alumni.Infrastructure.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.Numerics;

namespace FPT_Alumni.API.Controller.Alumnis
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlumniController : ControllerBase
    {
        IAlumniService _alumniService;

        public AlumniController(IAlumniService alumniService)
        {
            _alumniService = alumniService;
        }

        /// <summary>
        /// Create new alumni and insert into Db
        /// </summary>
        /// <param name="alumni">Alumni to insert</param>
        /// <returns>number of inserted record and status code</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/27/2024
        [HttpPost]
        public async Task<IActionResult> AddAsync(Alumni alumni)
        {
            var res = await _alumniService.InsertServiceAsync(alumni);
            return StatusCode((int)res.StatusCode!, res);
        }

        /// <summary>
        /// Receive a file -> validate file -> import success alumni
        /// </summary>
        /// <param name="key">key to get alumni list in cache</param>
        /// <returns>Number of created records</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/26/2024 
        ///  
        [HttpPost("ImportExcelFile")]
        public async Task<IActionResult> ImportFileBaseKey([FromBody] IFormFile file)
        {
            var res = await _alumniService.ImportFileServiceAsync(file);
            return StatusCode((int)res.StatusCode!, res);
        }

        /// <summary>
        /// Get key form user -> get invalid alumni list base key -> write to excel file
        /// </summary>
        /// <param name="key">key to get alumni list in cache</param>
        /// <returns>excel file that contain invalid alumni list</returns>
        ///  created by: Nguyễn Thiện Thắng 
        [HttpPost("ExportExcelFile")]
        public async Task<IActionResult> ExportFileBaseKey(string key)
        {
            var res = await _alumniService.ExportFileServiceAsync(key);
            var excelData = (Dictionary<string, object>)res.Data;
            var fileBytes = (byte[])excelData["FileBytes"];
            var fileName = (string)excelData["FileName"];
            return File(fileBytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName, true);
        }

        /// <summary>
        /// find alumni full name, email, student code by keyWords
        /// </summary>
        /// <param name="keyWord">key to find</param>
        /// <returns></returns>
        [HttpGet("FindALumniForTag")]
        public async Task<IActionResult> FindALumniByKeyWords(string keyWord)
        {
            var res = await _alumniService.FindAlumniByKeyWordsAsync(keyWord);
            return StatusCode((int)res.StatusCode!, res);
        }


    }
}
