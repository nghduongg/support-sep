﻿using AutoMapper;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Core.Interfaces.Services;
using FPT_Alumni.Core.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace FPT_Alumni.API.Controller.Posts
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        IPostService _postService;
        IPostRepository _postRepository;

        public PostController(IPostService postService, IPostRepository postRepository)
        {
            _postService = postService;
            _postRepository = postRepository;
        }

        /// <summary>
        /// Split all entity from post -> insert post and all relate entity
        /// </summary>
        /// <returns>Number of Post Insert success</returns>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 06/02/2024
        [HttpPost]
        public async Task<IActionResult> InsertAsync([FromBody] PostInsertDTO post)
        {
            string cookieHeader = Request.Headers["Cookie"].ToString();
            // Parse the cookie header to extract individual cookies
            var cookies = cookieHeader.Split(',')
                                      .Select(cookie => cookie.Split('='))
                                      .ToDictionary(cookie => cookie[0].Trim(), cookie => cookie.Length > 1 ? cookie[1].Trim() : string.Empty);
            cookies.TryGetValue("Access-Token", out var accessToken);
            var res = await _postService.InsertPostDTOServiceAsync(post, accessToken!);
            return StatusCode((int)res.StatusCode!, res);
        }

        /// <summary>
        /// Split all entity from post -> insert post and all relate entity
        /// </summary>
        /// <returns>Number of Post Insert success</returns>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 06/02/2024
        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] PostUpdateDTO post)
        {
            string cookieHeader = Request.Headers["Cookie"].ToString();
            // Parse the cookie header to extract individual cookies
            var cookies = cookieHeader.Split(',')
                                      .Select(cookie => cookie.Split('='))
                                      .ToDictionary(cookie => cookie[0].Trim(), cookie => cookie.Length > 1 ? cookie[1].Trim() : string.Empty);
            cookies.TryGetValue("Access-Token", out var accessToken);
            var res = await _postService.BeforeUpdateAsync(post, accessToken);
            return StatusCode((int)res.StatusCode, res);
        }

        [HttpGet]
        public async Task<IActionResult> Get(Guid id)
        {
            var res = await _postRepository.GetPostDTOByIdAsync(id, 2);
            return Ok(res);
        }

        /// <summary>
        /// change post status to public/ private
        /// </summary>
        /// <param name="status"> status to change </param>
        /// <returns>number of record updated</returns>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 08/06/2024
        [HttpPost("ChangePostStatus")]
        public async Task<IActionResult> ChangePostStatus(Guid postId, string status)
        {
            string cookieHeader = Request.Headers["Cookie"].ToString();
            // Parse the cookie header to extract individual cookies
            var cookies = cookieHeader.Split(',')
                                      .Select(cookie => cookie.Split('='))
                                      .ToDictionary(cookie => cookie[0].Trim(), cookie => cookie.Length > 1 ? cookie[1].Trim() : string.Empty);
            cookies.TryGetValue("Access-Token", out var accessToken);
            var res = await _postService.UpdatePostStatusServiceAsync(accessToken, postId, status);
            return StatusCode((int)res.StatusCode, res);
        }


        [HttpPost("ViewPostInUserProfile")]
        public async Task<IActionResult> ViewPostInUserProfile(
              [FromQuery] Guid ownId,
    [FromQuery] int currentPage,
    [FromQuery] int pageSize,
    [FromQuery] string? keySearch,
    [FromQuery] Guid[]? fieldIds,
    [FromQuery] DateTime? fromDate,
    [FromQuery] DateTime? toDate)
        {
            string cookieHeader = Request.Headers["Cookie"].ToString();
            // Parse the cookie header to extract individual cookies
            var cookies = cookieHeader.Split(',')
                                      .Select(cookie => cookie.Split('='))
                                      .ToDictionary(cookie => cookie[0].Trim(), cookie => cookie.Length > 1 ? cookie[1].Trim() : string.Empty);
            cookies.TryGetValue("Access-Token", out var accessToken);
            Guid[] fieldsIdTest = {
            new Guid("3A5D7EF9-1E5C-4DB7-9345-0B8F31C06E67"),
                new Guid("D320F2CF-D49C-48B7-9809-0F51EBF3D5B5"),
                new Guid("3FA85F64-5717-4562-B3FC-2C963F66AFA6"),
                  new Guid("2FB85F64-5717-4562-B3FC-2C963F66AFA6"),
                new Guid("8B8774AC-38A5-4203-99E4-57CB26245450"),
                new Guid("1BB27AD7-5D67-43C8-9BA5-F39463F15E64"),
                  new Guid("6F1CFE98-87B2-49B0-A8B1-6B0E0DB2D2B9"),
            };
            Guid[] CategoryIdsTest = {

                  new Guid("BA77FE63-8B6C-48A1-B5A7-0E167B9B5A4D"),
                new Guid("5C6655CF-C22F-4D8E-85B7-3FB1C9A7E704"),
                new Guid("74DA75C7-C122-4EC0-90AD-40C97A3D5288"),
                  new Guid("C5F1070C-5D91-427F-8F5C-ABF6381C9D0C"),
            };
            DateTime fromDateTest = new DateTime(2024, 6, 1);
            DateTime toDateTestt = new DateTime(2024, 5, 1);
            var res = await _postService.FilterPagingInUserProfile(currentPage, pageSize, accessToken, ownId, fieldsIdTest, keySearch, fromDateTest, toDateTestt);
            return StatusCode((int)res.StatusCode, res);
        }

        /// <summary>
        /// Filter paging all post in group
        /// </summary>
        /// <param name="groupId"> Group Id to get Post</param>
        /// <param name="currentPage">current page</param>
        /// <param name="pageSize">Number of post per page</param>
        /// <param name="keySearch">search key</param>
        /// <param name="fieldIds">Filed list</param>
        /// <param name="fromDate">created date of post to filter</param>
        /// <param name="toDate">created date of post to filter</param>
        /// <returns> post list in group</returns>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 30/06/2034
        [HttpPost("ViewPostInGroup")]
        public async Task<IActionResult> ViewPostInUserGroup(
             [FromQuery] Guid groupId,
   [FromQuery] int currentPage,
   [FromQuery] int pageSize,
   [FromQuery] string? keySearch,
   [FromQuery] Guid[]? fieldIds,
   [FromQuery] DateTime? fromDate,
   [FromQuery] DateTime? toDate)
        {
            string cookieHeader = Request.Headers["Cookie"].ToString();
            // Parse the cookie header to extract individual cookies
            var cookies = cookieHeader.Split(',')
                                      .Select(cookie => cookie.Split('='))
                                      .ToDictionary(cookie => cookie[0].Trim(), cookie => cookie.Length > 1 ? cookie[1].Trim() : string.Empty);
            cookies.TryGetValue("Access-Token", out var accessToken);
            Guid[] fieldsIdTest = { };
            Guid[] CategoryIdsTest = { new Guid("5C6655CF-C22F-4D8E-85B7-3FB1C9A7E704"), new Guid("74DA75C7-C122-4EC0-90AD-40C97A3D5288") };
            DateTime fromDateTest = new DateTime(2024, 6, 1);
            DateTime toDateTestt = new DateTime(2024, 5, 1);
            var res = await _postService.FilterPagingInGroup(currentPage, pageSize, accessToken, groupId, fieldsIdTest, keySearch, fromDateTest, toDateTestt);
            return StatusCode((int)res.StatusCode, res);
        }

    }
}
