﻿using FireSharp.Config;
using FireSharp.Interfaces;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Services;
using FPT_Alumni.Core.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPT_Alumni.API.Controller.Posts
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        ICommentService _commentService;

        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        /// <summary>
        /// Get page comments with postId 
        /// </summary>
        /// <param name="postId">Id of post for get comments</param>
        /// <returns>List of comment's information or null</returns>
        ///  created by: Vũ Xuân Trường
        ///  created at: 06/08/2024
        [HttpGet("GetPageOfCommentsByPostId/{postId}")]
        public async Task<IActionResult> GetPageOfCommentsByPostIdAsync(Guid postId, int pageNumber, int pageSize)
        {
            var res = await _commentService.GetPageOfCommentsByPostIdAsync(postId, pageNumber, pageSize);
            return StatusCode((int)res.StatusCode, res);
        }

        /// <summary>
        /// Get all comments with postId 
        /// </summary>
        /// <param name="postId">Id of post for get comments</param>
        /// <returns>List of comment's information or null</returns>
        ///  created by: Vũ Xuân Trường
        ///  created at: 06/08/2024
        [HttpGet("GetCommentsByPostId/{postId}")]
        public async Task<IActionResult> GetCommentsByPostIdAsync(Guid postId)
        {
            var res = await _commentService.GetCommentsByPostIdAsync(postId);
            return StatusCode((int)res.StatusCode, res);
        }

        /// <summary>
        /// Get number of comments with postId 
        /// </summary>
        /// <param name="postId">Id of post for get comments</param>
        /// <returns>Number of comment in post</returns>
        ///  created by: Vũ Xuân Trường
        ///  created at: 06/08/2024
        [HttpGet("GetNumberOfCommentsByPostIdAsync/{postId}")]
        public async Task<IActionResult> GetNumberOfCommentsByPostIdAsync(Guid postId)
        {
            var res = await _commentService.GetNumberOfCommentsByPostIdAsync(postId);
            return StatusCode((int)res.StatusCode, res);
        }

        /// <summary>
        /// Get all comments with parentCommentId 
        /// </summary>
        /// <param name="parentCommentId">Id of parent comment for get comments</param>
        /// <returns>List of comment's information or null</returns>
        ///  created by: Vũ Xuân Trường
        ///  created at: 06/08/2024
        [HttpGet("GetCommentsByParentCommentId/{parentCommentId}")]
        public async Task<IActionResult> GetCommentsByParentCommentIdAsync(string parentCommentId)
        {
            var res = await _commentService.GetCommentsByParentCommentIdAsync(parentCommentId);
            return StatusCode((int)res.StatusCode, res);
        }

        /// <summary>
        /// Create new comment and insert into firebase
        /// </summary>
        /// <param name="comment">Comment to insert</param>
        /// <returns>number of inserted record and status code</returns>
        ///  created by: Vũ Xuân Trường
        ///  created at: 06/08/2024
        [HttpPost]
        public async Task<IActionResult> AddAsync(Comment comment)
        {
            string cookieHeader = Request.Headers["Cookie"].ToString();
            // Parse the cookie header to extract individual cookies
            var cookies = cookieHeader.Split(',')
                                      .Select(cookie => cookie.Split('='))
                                      .ToDictionary(cookie => cookie[0].Trim(), cookie => cookie.Length > 1 ? cookie[1].Trim() : string.Empty);
            cookies.TryGetValue("Access-Token", out var accessToken);
            var res = await _commentService.InsertServiceAsync(comment, accessToken);
            return StatusCode((int)res.StatusCode, res);
        }

        /// <summary>
        /// Update comment's information and change into firebase
        /// </summary>
        /// <param name="comment">Comment to Update</param>
        /// <returns>number of updated record and status code</returns>
        ///  created by: Vũ Xuân Trường
        ///  created at: 06/08/2024
        [HttpPut]
        public async Task<IActionResult> UpdateAsync(Comment comment)
        {
            var res = await _commentService.UpdateServiceAsync(comment);
            return StatusCode((int)res.StatusCode, res);
        }

        /// <summary>
        /// Delete comment and change into firebase
        /// </summary>
        /// <param name="id">Id for comment to delete</param>
        /// <returns>number of deleted record and status code</returns>
        ///  created by: Vũ Xuân Trường
        ///  created at: 06/08/2024
        [HttpDelete]
        public async Task<IActionResult> DeleteAsync(string id)
        {
            var res = await _commentService.DeleteServiceAsync(id);
            return StatusCode((int)res.StatusCode, res);
        }
    }
}
