﻿using FPT_Alumni.Core.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPT_Alumni.API.Controller.Posts
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostCategoryController : ControllerBase
    {
        IPostCategoryService _postCategoryService;

        public PostCategoryController(IPostCategoryService postCategoryService)
        {
            _postCategoryService = postCategoryService;
        }
        /// <summary>
        /// Get all post category
        /// </summary>
        /// <returns>All post category</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 06/23/2024
        [HttpGet]
        public async Task<IActionResult> GetBaseRole()
        {
            string cookieHeader = Request.Headers["Cookie"].ToString();
            // Parse the cookie header to extract individual cookies
            var cookies = cookieHeader.Split(',')
                                      .Select(cookie => cookie.Split('='))
                                      .ToDictionary(cookie => cookie[0].Trim(), cookie => cookie.Length > 1 ? cookie[1].Trim() : string.Empty);
            cookies.TryGetValue("Access-Token", out var accessToken);
            var res = await _postCategoryService.GetAllBaseRoleServiceAsync(accessToken);
            return StatusCode((int)res.StatusCode, res);
        }
    }
}
