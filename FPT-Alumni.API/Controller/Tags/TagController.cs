﻿using FPT_Alumni.Core.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPT_Alumni.API.Controller.Tags
{
    [Route("api/[controller]")]
    [ApiController]
    public class TagController : ControllerBase
    {
        ITagService _tagService;

        public TagController(ITagService tagService)
        {
            _tagService = tagService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var res = await _tagService.GetAllServiceAsync();
            return StatusCode((int)res.StatusCode, res);
        }

        [HttpGet("FindByKeyWords")]
        public async Task<IActionResult> FindTagByTagNameForTagging(string key)
        {
            var res = await _tagService.FindTagByKeyWordForTagingServiceAsync(key);
            return StatusCode((int)res.StatusCode, res);
        }
    }
}
