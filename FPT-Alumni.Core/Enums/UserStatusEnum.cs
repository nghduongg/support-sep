﻿

namespace FPT_Alumni.Core.Enums
{
    public enum UserStatusEnum
    {
        /// <summary>
        /// unauthorize accout
        /// </summary>
        UNAUTHORIZE,
        /// <summary>
        /// activated accout
        /// </summary>
        ACTIVATED,
        /// <summary>
        /// Inactivated account
        /// </summary>
        INACTIVATED,
        /// <summary>
        /// Deleted account
        /// </summary>
        DELETED
    }
}
