﻿

namespace FPT_Alumni.Core.Enums
{
    public enum ParticipantRoleEnum
    {
        PARTICIPANT,
        ORGANIZER,
        VOLUNTEER,
    }
}
