﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Enums
{
    public enum ViewModeEnum
    {
        GUEST_VIEW, 
        OWN_VIEW,
        ALUMNI_VIEW,
        STAFF_VIEW
    }
}
