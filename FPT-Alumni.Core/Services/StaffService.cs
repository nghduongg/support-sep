﻿using AutoMapper;
using FPT_Alumni.Core.Constants;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Enums;
using FPT_Alumni.Core.Exceptions;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Core.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Services
{
    public class StaffService : BaseService<Staff>, IStaffService
    {
        readonly IUnitOfWork _unitOfWork;
        readonly IMapper _mapper;
        readonly ICommonService _commonService;
        public StaffService(IBaseRepository<Staff> baseRepository, IUnitOfWork unitOfWork,
            IMapper mapper, ICommonService commonService) : base(baseRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _commonService = commonService;
        }

        public async Task<ServiceResult> GetStaffListServiceAsync()
        {
            var staffList = await _unitOfWork.Staff.GetStaffListAsync();
            if (staffList == null || staffList.Count == 0)
            {
                return new ServiceResult
                {
                    StatusCode = System.Net.HttpStatusCode.NotFound,
                    Success = false,
                    DevMsg = "List of staff is empty"
                };
            }
            var statusList = HelperConstants.UserStatusList();
            return new ServiceResult
            {
                StatusCode = System.Net.HttpStatusCode.OK,
                Success = true,
                Data = new { staffList, statusList }
            };
        }

        public async Task<ServiceResult> GetStaffByStatusServiceAsync(string? status)
        {
            var staffList = await _unitOfWork.Staff.GetStaffsByStatusAsync(status);
            if (staffList == null || staffList.Count == 0)
            {
                return new ServiceResult
                {
                    StatusCode = System.Net.HttpStatusCode.NotFound,
                    Success = false,
                    DevMsg = "List of staff is empty"
                };
            }
            var statusList = HelperConstants.UserStatusList();
            return new ServiceResult
            {
                StatusCode = System.Net.HttpStatusCode.OK,
                Success = true,
                Data = new { staffList, statusList }
            };
        }

        public async Task<ServiceResult> DeleteStaffServiceAsync(Guid userId)
        {
            var updateUserResult = await _unitOfWork.Staff.DeleteStaffByUserIdAsync(userId);
            if (updateUserResult > 0 || updateUserResult == -1)
            {
                return new ServiceResult()
                {
                    Data = updateUserResult,
                    Success = true,
                    StatusCode = System.Net.HttpStatusCode.OK,
                };
            }
            throw new InternalServerException(500);
        }

        public async Task<ServiceResult> SaveStaffServiceAsync(StaffWithUserModelView staffSaveDTO)
        {
            var result = 0;
            if (staffSaveDTO.UserId == null || staffSaveDTO.UserId == Guid.Empty)
            {
                var newUser = _mapper.Map<User>(staffSaveDTO);
                var staff = _mapper.Map<Staff>(staffSaveDTO);

                bool isEmailTakenByAnotherUser = false;
                if (!string.IsNullOrEmpty(staffSaveDTO.MainEmail))
                {
                    isEmailTakenByAnotherUser = await _unitOfWork.User.IsEmailTakenByAnotherUserAsync(staffSaveDTO.MainEmail, Guid.Empty);
                }

                if (isEmailTakenByAnotherUser)
                {
                    return new ServiceResult()
                    {
                        Success = false,
                        StatusCode = System.Net.HttpStatusCode.Conflict,
                        DevMsg = "Your email entered is taken by another user"
                    };
                }

                newUser.Id = Guid.NewGuid();
                newUser.Status = Enums.UserStatusEnum.ACTIVATED.ToString();
                newUser.RoleId = RoleConstants.STAFF;
                newUser.SubEmail = staffSaveDTO.MainEmail;
                string password = _commonService.GenerateRandomPassword(10);
                newUser.Password = BCrypt.Net.BCrypt.HashPassword(password); //  password hashing
                var insertUserResult = await _unitOfWork.User.InsertAsync(newUser);
                if (insertUserResult > 0 || insertUserResult == -1) result++;

                staff.UserId = newUser.Id;
                staff.Id = Guid.NewGuid();

                var updateResult = await _unitOfWork.Staff.InsertAsync(staff);
                if (updateResult > 0 || updateResult == -1) result++;


                if (result > 0)
                {
                    BodyMail.Title = "Your staff account: \n";
                    BodyMail.Content = $@"  <p>Email: {newUser.MainEmail}</p>
                                        <p>Password:{password}</p>";

                    BodyMail.LinkRedirect = "https://www.facebook.com/ALUMNI.FPTU";
                    MailDTO mailDTO = new MailDTO()
                    {
                        To = newUser.MainEmail,
                        Subject = "Welcome to FAL-FPT Alumni Portal",
                        Body = BodyMail.GetBody()
                    };
                    var checkSend = await _commonService.SendMailAsync(mailDTO);
                    if (!checkSend)
                    {
                        return new ServiceResult()
                        {
                            Data = result,
                            Success = true,
                            StatusCode = System.Net.HttpStatusCode.Created,
                            UserMsg = "Send Email Fail"
                        };
                    }
                }
            }
            else
            {
                bool isEmailTakenByAnotherUser = false;
                if (!string.IsNullOrEmpty(staffSaveDTO.MainEmail))
                {
                    isEmailTakenByAnotherUser = await _unitOfWork.User.IsEmailTakenByAnotherUserAsync(staffSaveDTO.MainEmail, staffSaveDTO.UserId);
                }

                if (isEmailTakenByAnotherUser)
                {
                    return new ServiceResult()
                    {
                        Success = false,
                        StatusCode = System.Net.HttpStatusCode.Conflict,
                        DevMsg = "Your email entered is taken by another user"
                    };
                }

               
                var updateUser = _mapper.Map<User>(staffSaveDTO);
                if (!string.IsNullOrEmpty(staffSaveDTO.Password))
                {
                    updateUser.Password = BCrypt.Net.BCrypt.HashPassword(staffSaveDTO.Password);
                }
                updateUser.Id = (Guid)staffSaveDTO.UserId;
                var updateUserResult = await _unitOfWork.User.UpdateByUserIdAsync(updateUser);

                if (updateUserResult > 0 || updateUserResult == -1) result++;

                var staff = _mapper.Map<Staff>(staffSaveDTO);
               
                var updateResult = await _unitOfWork.Staff.UpdateStaffByUserIdAsync(staff);
                if (updateResult > 0 || updateResult == -1) result++;
            }

            if (result > 0)
            {
                return new ServiceResult()
                {
                    Data = result,
                    Success = true,
                    StatusCode = System.Net.HttpStatusCode.OK,
                };
            }
            throw new InternalServerException("Save staff failed. Internal error");
        }
    }
}
