﻿using AutoMapper;
using FPT_Alumni.Core.Constants;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Enums;
using FPT_Alumni.Core.Exceptions;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Core.Interfaces.Services;
using FPT_Alumni.Core.JsonModels;
using FPT_Alumni.Infrastructure.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Globalization;
using System.Net;
using System.Reflection;
using System.Resources;


namespace FPT_Alumni.Core.Services
{
    public class AlumniService : BaseService<Alumni>, IAlumniService
    {
        readonly IAlumniRepository _alumniRepository;
        readonly IUnitOfWork _unitOfWork;
        readonly IFileValidation _fileValidation;
        ICacheRepository _cacheRepository;
        ICommonService _commonService;
        IMapper _mapper;

        public AlumniService(IAlumniRepository baseRepository, IUnitOfWork unitOfWork, IFileValidation fileValidation, ICacheRepository cacheRepository, ICommonService commonService, IMapper mapper) : base(baseRepository)
        {
            _alumniRepository = baseRepository;
            _unitOfWork = unitOfWork;
            _fileValidation = fileValidation;
            _cacheRepository = cacheRepository;
            _commonService = commonService;
            _mapper = mapper;
        }

        public async Task<ServiceResult> ExportFileServiceAsync(string key)
        {
            if (!_cacheRepository.IsCacheExist(key))
            {
                throw new BadRequestException("Dữ liệu không tồn tại hoặc quá hạn, vui lòng thử lại");
            }
            List<AlumniImportDTO> invalidALumni = _cacheRepository.GetCache<List<AlumniImportDTO>>(key);
            if (invalidALumni.Count > 0)
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                // create an excel file
                using (ExcelPackage package = new ExcelPackage())
                {
                    // create a work sheet
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Invalid ALumni");
                    int row = 2;
                    int index = 1;

                    // Header title
                    string[] headerTitles = { "STT", "Student Code", "Full name", "Email", "Phone Number", "Date of birth", "Major", "Class", "Graduate", "Graduation Year", "Current Country", "Current City", "Job", "Company", "Error" };
                    for (int col = 1; col <= headerTitles.Length; col++)
                    {
                        worksheet.Cells[1, col].Value = headerTitles[col - 1];
                        worksheet.Cells[1, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[1, col].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        worksheet.Cells[1, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Row(1).Height = 30;
                    }
                    worksheet.View.FreezePanes(2, 1);
                    // Write data
                    foreach (var alumni in invalidALumni)
                    {
                        // Ghi dữ liệu vào từng ô tương ứng
                        worksheet.Cells[row, 1].Value = index;
                        worksheet.Cells[row, 2].Value = alumni.StudentCode;
                        worksheet.Cells[row, 3].Value = alumni.FullName;
                        worksheet.Cells[row, 4].Value = alumni.Email;
                        worksheet.Cells[row, 5].Value = alumni.PhoneNumber.ToString();
                        worksheet.Cells[row, 6].Style.Numberformat.Format = "dd/MM/yyyy";
                        worksheet.Cells[row, 6].Value = alumni.DateOfBirth;
                        worksheet.Cells[row, 7].Value = alumni.Major;
                        worksheet.Cells[row, 8].Value = alumni.Class;
                        worksheet.Cells[row, 9].Value = alumni.IsGraduated;
                        worksheet.Cells[row, 10].Value = alumni.GraduationYear;
                        worksheet.Cells[row, 11].Value = alumni.Country;
                        worksheet.Cells[row, 12].Value = alumni.City;
                        worksheet.Cells[row, 13].Value = alumni.Job;
                        worksheet.Cells[row, 14].Value = alumni.Company;
                        // auto fit collumn
                        worksheet.Cells.AutoFitColumns();
                        worksheet.Column(15).Width = 50;
                        var indentedErrors = alumni.ErrorList.Select(error => "\t" + " - " + error);
                        worksheet.Cells[row, 15].Style.WrapText = true;
                        worksheet.Cells[row, 15].Value = string.Join(Environment.NewLine, indentedErrors);
                        worksheet.Cells[row, 15].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                        row++;
                        index++;
                    }

                    worksheet.Cells[1, 1, row - 1, headerTitles.Length + 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    MemoryStream stream = new MemoryStream(package.GetAsByteArray());
                    byte[] fileBytes = stream.ToArray();
                    string fileName = "Error ALumni";
                    var excelData = new Dictionary<string, object>
            {
                { "FileBytes", fileBytes },
                { "FileName", fileName }
                    };
                    //return excel 
                    return await Task.FromResult(new ServiceResult
                    {
                        Success = true,
                        Data = excelData,
                        StatusCode = HttpStatusCode.OK,
                        UserMsg = "Xuất khẩu thành công "
                    });
                }
            }
            throw new BadRequestException("Dữ liệu không tồn tại hoặc quá hạn, vui lòng thử lại");
        }

        public async Task<ServiceResult> ImportFileServiceAsync(IFormFile file)
        {
            // cache do not exist
            AlumniAndKeyDTO alumniAndKeyDTO = await PreviewFileServiceAsync(file);
            List<AlumniImportDTO> alumniImportDTOs = alumniAndKeyDTO.AlumniImportDTOs;
            int count = 0;
            if (alumniImportDTOs.Count > 0)
            {
                _unitOfWork.BeginTransaction();
                foreach (var a in alumniImportDTOs)
                {
                    User user = new()
                    {
                        Id = Guid.NewGuid(),
                        MainEmail = a.Email,
                        RoleId = RoleConstants.ALUMNI,
                        EmailLogged = a.Email,
                        Status = Enums.UserStatusEnum.ACTIVATED.ToString()
                    };
                    Alumni alumni = new()
                    {
                        Id = Guid.NewGuid(),
                        StudentCode = a.StudentCode,
                        PhoneNumber = a.PhoneNumber,
                        Class = a.Class,
                        Major = a.Major,
                        IsGraduated = a.IsGraduated,
                        Job = a.Job,
                        Company = a.Company,
                        LinkedUrl = "string",
                        WorkExperience = "string",
                        EducationExperience = "string",
                        PrivacySettings = "string",
                        FullName = a.FullName,
                        DateOfBirth = a.DateOfBirth,
                        GraduationYear = a.GraduationYear,
                        Gender = Enums.GenderEnum.Nam,
                        Country = a.Country,
                        City = a.City,
                        UserId = user.Id,
                        CreatedBy = user.Id,
                        ModifiedBy = user.Id,
                    };
                    var res1 = await _unitOfWork.User.InsertAsync(user);
                    var res2 = await _unitOfWork.Alumni.InsertAsync(alumni);
                    if (res1 < 1 || res2 < 1)
                    {
                        _unitOfWork.RollBack();
                        throw new Exception("Có lỗi xảy ra, vui Lòng thử lại");
                    }
                    count++;
                };
                _unitOfWork.Commit();
            }
            return new ServiceResult()
            {
                StatusCode = count > 0 ? System.Net.HttpStatusCode.Created : HttpStatusCode.OK,
                Success = true,
                Data = new
                {
                    SuccessAlumniImported = count,
                    InvalidKey = alumniAndKeyDTO.InvalidKey,
                    FailedNumber = alumniAndKeyDTO.FailedNumber
                },
                UserMsg = count > 0 ? $"Nhập khẩu thành công {count} Alumni" : "Không có Alumni hợp lệ để nhập khẩu"
            };
        }

        public async Task<AlumniAndKeyDTO> PreviewFileServiceAsync(IFormFile file)
        {
            string[] expectedHeader = { "STT", "Student Code", "Full name", "Email", "Phone Number", "Date of birth", "Major", "Class", "Graduate", "Graduation Year", "Current Country", "Current City", "Job", "Company" };
            await _fileValidation.ValidateFileAsync(file, ".xlsx", expectedHeader);
            List<AlumniImportDTO> validAlumni = new();
            List<AlumniImportDTO> invalidAlumni = new();
            List<AlumniImportDTO> allAlumni = new();
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                using (var package = new ExcelPackage(stream))
                {
                    var worksheet = package.Workbook.Worksheets[0];
                    var rowCount = worksheet.Dimension.Rows;
                    var phoneNumberDict = new Dictionary<string, List<AlumniImportDTO>>();
                    var emailDict = new Dictionary<string, List<AlumniImportDTO>>();
                    var studentCodeDict = new Dictionary<string, List<AlumniImportDTO>>();
                    var tasks = new List<Task>();

                    for (int row = 2; row <= rowCount; row++)
                    {
                        var alumni = new AlumniImportDTO
                        {
                            StudentCode = worksheet.Cells[row, 2].Value?.ToString(),
                            FullName = worksheet.Cells[row, 3].Value?.ToString(),
                            Email = worksheet.Cells[row, 4].Value?.ToString(),
                            PhoneNumber = worksheet.Cells[row, 5].Value?.ToString(),
                            Major = worksheet.Cells[row, 7].Value?.ToString(),
                            Class = worksheet.Cells[row, 8].Value?.ToString(),
                            GraduationYear = worksheet.Cells[row, 10].Value?.ToString(),
                            Country = worksheet.Cells[row, 11].Value?.ToString(),
                            City = worksheet.Cells[row, 12].Value?.ToString(),
                            Job = worksheet.Cells[row, 13].Value?.ToString(),
                            Company = worksheet.Cells[row, 14].Value?.ToString()
                        };
                        CheckEmptyAndAddErrorMsg(alumni, alumni.FullName, "Họ và tên");
                        CheckEmptyAndAddErrorMsg(alumni, alumni.Major, "Chuyên ngành");
                        CheckEmptyAndAddErrorMsg(alumni, alumni.Class, "Khóa");
                        // check null + duplicate student code
                        if (string.IsNullOrEmpty(alumni.StudentCode))
                        {
                            alumni.ErrorList.Add("Mã số sinh viên không được phép để trống");
                        }
                        else
                        {
                            await CheckDuplicateAttributesInSheet("Mã sinh viên", "StudentCode", alumni.StudentCode, alumni, studentCodeDict);
                        }

                        // check duplicate Phonumber
                        if (!string.IsNullOrEmpty(alumni.PhoneNumber))
                        {
                            await CheckDuplicateAttributesInSheet("Số điện thoại", "PhoneNumber", alumni.PhoneNumber, alumni, phoneNumberDict);
                        }

                        // check duplicate Email
                        if (!string.IsNullOrEmpty(alumni.Email))
                        {
                            await CheckDuplicateAttributesInSheet("Email", "MainEmail", alumni.Email, alumni, emailDict);
                        }
                        // DateOfBirh
                        var dobString = worksheet.Cells[row, 6].Value?.ToString()?.Trim().Replace("SA", "AM").Replace("CH", "PM");
                        CheckDateOfBirthFormat(alumni, dobString);
                        // IsGraduated
                        var isGraduatedString = worksheet.Cells[row, 9].Value?.ToString()?.Trim().ToLower();
                        CheckIsGraduatedFormat(alumni, isGraduatedString);
                        // GraduatedYear
                        var graduatedYearString = worksheet.Cells[row, 10].Value?.ToString();
                        CheckGraduatedYearFormat(alumni, graduatedYearString);
                        lock (allAlumni)
                        {
                            allAlumni.Add(alumni);
                        }
                    }
                    validAlumni = allAlumni.Where(a => a.ErrorList.Count == 0).ToList();
                    invalidAlumni = allAlumni.Where(a => a.ErrorList.Count > 0).ToList();
                }
            }

            TimeSpan timeSpan = TimeSpan.FromMinutes(30);
            var validListKey = Guid.NewGuid().ToString();
            string inValidListKey = null;
            if (invalidAlumni.Count > 0)
            {
                inValidListKey = Guid.NewGuid().ToString();
                _cacheRepository.SetCache(inValidListKey, invalidAlumni, timeSpan);
            }
            return new AlumniAndKeyDTO
            {
                AlumniImportDTOs = validAlumni,
                InvalidKey = inValidListKey,
                FailedNumber = invalidAlumni.Count
            };
        }

        /// <summary>
        /// Check duplicate in a collumn of worksheet, if duplicate data -> add Error msg into alumni
        /// </summary>
        /// <param name="attName"> Name to add to msg</param>
        /// <param name="attValue"> value to check and add to msg</param>
        /// <param name="alumni"> alumni contain ErrorMsgList</param>
        /// <param name="attDict">The dictionary contains a key that is an attribute that cannot be duplicated, 
        /// the value will be a list of objects, this value will only appear once in the list of objects. 
        /// </param>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/28/2024 
        private async Task CheckDuplicateAttributesInSheet(string attName, string collumName, string attValue, AlumniImportDTO alumni, Dictionary<string, List<AlumniImportDTO>> attDict)
        {
            if (collumName != "MainEmail")
            {
                Alumni a = new Alumni();
                if (await _unitOfWork.Alumni.IsParamValueExistAsync<string>(a, collumName, attValue))
                {
                    alumni.ErrorList.Add($"{attName} {attValue} đã tồn tại trong hệ thống");
                }
            }
            else
            {
                User u = new();
                if (await _unitOfWork.User.IsParamValueExistAsync<string>(u, collumName, attValue))
                {
                    alumni.ErrorList.Add($"{attName} {attValue} đã tồn tại trong hệ thống");
                }
            }

            if (attDict.ContainsKey(attValue))
            {
                alumni.ErrorList.Add($"{attName} {attValue} đã tồn tại trong danh sách của bạn");
                foreach (var existingAlumni in attDict[attValue])
                {
                    if (!existingAlumni.ErrorList.Contains($"{attName} {attValue} đã tồn tại trong danh sách của bạn"))
                        existingAlumni.ErrorList.Add($"{attName} {attValue} đã tồn tại trong danh sách của bạn");
                }
            }
            else
            {
                attDict[attValue] = new List<AlumniImportDTO> { alumni };
            }
        }

        /// <summary>
        /// If alumni's attribute is null or empty -> add error msg to alumni's ErrorList
        /// </summary>
        /// <param name="alumni">Almni contain ErrorMsgList</param>
        /// <param name="attValue"> value to check and add to msg</param>
        /// <param name="attName">To add to error msg</param>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/28/2024 
        private void CheckEmptyAndAddErrorMsg(AlumniImportDTO alumni, string attValue, string attName)
        {
            if (string.IsNullOrEmpty(attValue))
                alumni.ErrorList.Add($"{attName} của sinh viên không được phép để trống");
        }

        /// <summary>
        /// Check format date must be dd/MM/yyyy, if not match -> add error msg to alumni's ErrorList
        /// </summary>
        /// <param name="alumni">Contain ErrorList</param>
        /// <param name="dobString">To check Format</param>
        private void CheckDateOfBirthFormat(AlumniImportDTO alumni, string? dobString)
        {
            if (!string.IsNullOrWhiteSpace(dobString))
            {
                // Chuẩn hóa chuỗi nhập vào: loại bỏ phần thời gian nếu có
                string[] dateTimeParts = dobString.Split(' ');
                string datePart = dateTimeParts[0];

                // Tách các thành phần ngày, tháng, năm từ chuỗi nhập vào
                string[] dateParts = datePart.Split('/');
                if (dateParts.Length == 3 && int.TryParse(dateParts[0], out int day) && int.TryParse(dateParts[1], out int month) && int.TryParse(dateParts[2], out int year))
                {
                    // Tạo chuỗi ngày mới với định dạng dd/MM/yyyy
                    string formattedDate = $"{day:D2}/{month:D2}/{year:D4}";

                    // Kiểm tra định dạng của chuỗi mới
                    if (DateTime.TryParseExact(formattedDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dateTime))
                    {
                        alumni.DateOfBirth = dateTime;
                    }
                    else
                    {
                        alumni.ErrorList.Add("Ngày sinh phải theo định dạng dd/MM/yyyy");
                    }
                }
                else
                {
                    alumni.ErrorList.Add("Ngày sinh phải theo định dạng dd/MM/yyyy");
                }
            }
            else
            {
                alumni.ErrorList.Add("Ngày sinh không được phép để trống");
            }

        }

        /// <summary>
        /// Chekc dob format must be yyyy and can not be null or empty, if not valid -> add errMsg to alumni's ErrorList
        /// </summary>
        /// <param name="alumni">Contain ErrorList</param>
        /// <param name="graduatedYearString">graduatedYear to check</param>
        private void CheckGraduatedYearFormat(AlumniImportDTO alumni, string? graduatedYearString)
        {
            if (!string.IsNullOrEmpty(graduatedYearString))
            {
                if (DateTime.TryParseExact(graduatedYearString, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out _))
                    alumni.GraduationYear = graduatedYearString;
                else
                    alumni.ErrorList.Add("Năm tốt nghiệp phải theo định dạng yyyy");
            }
            else
            {
                alumni.ErrorList.Add("Năm tốt nghiệp không được phép để trống");
            }
        }

        private void CheckIsGraduatedFormat(AlumniImportDTO alumni, string? isGraduatedString)
        {
            if (isGraduatedString == "yes")
            {
                alumni.IsGraduated = true;
            }
            else if (isGraduatedString == "no")
            {
                alumni.IsGraduated = false;
            }
            else
            {
                alumni.ErrorList.Add("Giá trị Graduate phải là Yes hoặc No ");
            }
        }

        public async Task<ServiceResult> GetAlumniListAsync()
        {
            var alumniList = await _unitOfWork.Alumni.GetAlumniListAsync();
            if (alumniList == null || alumniList.Count == 0)
            {
                return new ServiceResult
                {
                    StatusCode = System.Net.HttpStatusCode.NotFound,
                    Success = false,
                    DevMsg = "List of alumni is empty"
                };
            }
            var statusList = HelperConstants.UserStatusList();
            return new ServiceResult
            {
                StatusCode = System.Net.HttpStatusCode.OK,
                Success = true,
                Data = new { alumniList, statusList }
            };
        }

        public async Task<ServiceResult> GetAlumniByStatusAsync(string? status)
        {
            var alumniList = await _unitOfWork.Alumni.GetAlumniesByStatusAsync(status);
            if (alumniList == null || alumniList.Count == 0)
            {
                return new ServiceResult
                {
                    StatusCode = System.Net.HttpStatusCode.NotFound,
                    Success = false,
                    DevMsg = "List of alumni is empty"
                };
            }
            var statusList = HelperConstants.UserStatusList();
            return new ServiceResult
            {
                StatusCode = System.Net.HttpStatusCode.OK,
                Success = true,
                Data = new { alumniList, statusList }
            };
        }

        public async Task<ServiceResult> SaveAlumniAsync(UserModelSave alumniSaveDTO)
        {
            var result = 0;
            if (alumniSaveDTO.UserId == Guid.Empty)
            {
                var newUser = _mapper.Map<User>(alumniSaveDTO);
                var alumni = _mapper.Map<Alumni>(alumniSaveDTO);

                bool isEmailTakenByAnotherUser = false;
                if (!string.IsNullOrEmpty(alumniSaveDTO.MainEmail))
                {
                    isEmailTakenByAnotherUser = await _unitOfWork.User.IsEmailTakenByAnotherUserAsync(alumniSaveDTO.MainEmail, Guid.Empty);
                }

                if (isEmailTakenByAnotherUser)
                {
                    return new ServiceResult()
                    {
                        Success = false,
                        StatusCode = System.Net.HttpStatusCode.Conflict,
                        DevMsg = "Your email entered is taken by another user"
                    };
                }

                newUser.Id = Guid.NewGuid();
                newUser.Status = Enums.UserStatusEnum.UNAUTHORIZE.ToString();
                newUser.RoleId = RoleConstants.ALUMNI;
                string password = _commonService.GenerateRandomPassword(10);
                newUser.Password = BCrypt.Net.BCrypt.HashPassword(password); //  password hashing
                var insertUserResult = await _unitOfWork.User.InsertAsync(newUser);
                if (insertUserResult > 0 || insertUserResult == -1) result++;

                alumni.UserId = newUser.Id;
                alumni.Id = Guid.NewGuid();
                alumni.ModifiedBy = alumni.UserId;
                alumni.CreatedDate = DateTime.UtcNow;
                alumni.ModifiedDate = DateTime.UtcNow;
                alumni.CreatedBy = alumni.UserId;
                var updateResult = await _unitOfWork.Alumni.InsertAsync(alumni);
                if (updateResult > 0 || updateResult == -1) result++;


                if (result > 0)
                {
                    BodyMail.Title = "Your alumni account: \n";
                    BodyMail.Content = $@"  <p>Email: {newUser.MainEmail}</p>
                                        <p>Password:{password}</p>";

                    BodyMail.LinkRedirect = "https://www.facebook.com/ALUMNI.FPTU";
                    MailDTO mailDTO = new MailDTO()
                    {
                        To = newUser.MainEmail,
                        Subject = "Welcome to FAL-FPT Alumni Portal",
                        Body = BodyMail.GetBody()
                    };
                    var checkSend = await _commonService.SendMailAsync(mailDTO);
                    if (!checkSend)
                    {
                        return new ServiceResult()
                        {
                            Data = result,
                            Success = true,
                            StatusCode = System.Net.HttpStatusCode.Created,
                            UserMsg = "Send Email Fail"
                        };
                    }
                }
            }
            else
            {
                bool isEmailTakenByAnotherUser = false;
                if (!string.IsNullOrEmpty(alumniSaveDTO.MainEmail))
                {
                    isEmailTakenByAnotherUser = await _unitOfWork.User.IsEmailTakenByAnotherUserAsync(alumniSaveDTO.MainEmail, alumniSaveDTO.UserId);
                }

                if (isEmailTakenByAnotherUser)
                {
                    return new ServiceResult()
                    {
                        Success = false,
                        StatusCode = System.Net.HttpStatusCode.Conflict,
                        DevMsg = "Your email entered is taken by another user"
                    };
                }

                UserModelView existedAlumni = await _unitOfWork.Alumni.GetAlumniByUserIdAsync(alumniSaveDTO.UserId);
                if (existedAlumni != null)
                {
                    if (alumniSaveDTO.IsGraduated == null)
                    {
                        alumniSaveDTO.IsGraduated = existedAlumni.IsGraduated;
                    }
                    if (alumniSaveDTO.Gender == null)
                    {
                        alumniSaveDTO.Gender = existedAlumni.Gender;
                    }
                }
                var updateUser = _mapper.Map<User>(alumniSaveDTO);
                if (!string.IsNullOrEmpty(alumniSaveDTO.Password))
                {
                    updateUser.Password = BCrypt.Net.BCrypt.HashPassword(alumniSaveDTO.Password);
                }
                updateUser.Id = (Guid)alumniSaveDTO.UserId;
                var updateUserResult = await _unitOfWork.User.UpdateByUserIdAsync(updateUser);

                if (updateUserResult > 0 || updateUserResult == -1) result++;

                var alumni = _mapper.Map<Alumni>(alumniSaveDTO);
                alumni.ModifiedBy = alumni.UserId;
                alumni.ModifiedDate = DateTime.UtcNow;
                var updateResult = await _unitOfWork.Alumni.UpdateAlumniByUserIdAsync(alumni);
                if (updateResult > 0 || updateResult == -1) result++;
            }

            if (result > 0)
            {
                return new ServiceResult()
                {
                    Data = result,
                    Success = true,
                    StatusCode = System.Net.HttpStatusCode.OK,
                };
            }
            throw new InternalServerException(500);
        }

        public async Task<ServiceResult> DeleteAlumniServiceAsync(Guid userId)
        {
            User updateUser = new User();
            updateUser.Id = userId;
            updateUser.Status = UserStatusEnum.DELETED.ToString();
            var updateUserResult = await _unitOfWork.User.UpdateByUserIdAsync(updateUser);
            if (updateUserResult > 0 || updateUserResult == -1)
            {
                return new ServiceResult()
                {
                    Data = updateUserResult,
                    Success = true,
                    StatusCode = System.Net.HttpStatusCode.OK,
                };
            }
            throw new InternalServerException(500);
        }
        public async Task<ServiceResult> FindAlumniByKeyWordsAsync(string key)
        {
            List<AlumniSearchToTagDTO> alumniDTO = new();
            if (!String.IsNullOrEmpty(key))
            {
                List<AlumniSearchToTagDTO> alumni = await _unitOfWork.Alumni.FindAlumniByKeyWordsAsync(key);
                alumniDTO = _mapper.Map<List<AlumniSearchToTagDTO>>(alumni);
            }
            return new ServiceResult()
            {
                Success = true,
                StatusCode = HttpStatusCode.OK,
                Data = alumniDTO,
                UserMsg = "Tìm thành công"
            };
        }

        public async Task<ServiceResult> GetAlumniByUserIdServiceAsync(Guid userId)
        {
            var alumni = await _unitOfWork.Alumni.GetAlumniByUserIdAsync(userId);
            if (alumni == null)
            {
                return new ServiceResult
                {
                    StatusCode = System.Net.HttpStatusCode.NotFound,
                    Success = false,
                    UserMsg = "Alumni not existed"
                };
            }
            var statusList = HelperConstants.UserStatusList();
            return new ServiceResult
            {
                StatusCode = System.Net.HttpStatusCode.OK,
                Success = true,
                Data = new { alumni, statusList }
            };
        }

        public Task<AlumniProfileDTO> GetAlumniProfileByUserIdAsync(Guid userId)
        {
            throw new NotImplementedException();
        }

        public AlumniProfileDTO HiddenDataBaseSettings(AlumniProfileDTO alumniProfileDTO)
        {
            PrivacySettings settings = JsonConvert.DeserializeObject<PrivacySettings>(alumniProfileDTO.PrivacySettings);
            if (!settings.IsShowStudentCode) alumniProfileDTO.StudentCode = null;
            if (!settings.IsShowMajor) alumniProfileDTO.Major = null;
            if (!settings.IsJob) alumniProfileDTO.Job = null;
            if (!settings.IsShowGraduationYear) alumniProfileDTO.GraduationYear = null;
            if (!settings.IsShowCity) alumniProfileDTO.City = null;
            if (!settings.IsShowCountry) alumniProfileDTO.Country = null;
            if (!settings.IsShowLinkedUrl) alumniProfileDTO.LinkedUrl = null;
            if (!settings.IsShowClass) alumniProfileDTO.Class = null;
            if (!settings.IsShowCompany) alumniProfileDTO.Company = null;
            if (!settings.IsShowEducationExperiece) alumniProfileDTO.EducationExperience = null;
            if (!settings.IsShowDateOfBirth) alumniProfileDTO.DateOfBirth = null;
            if (!settings.IsGraduated) alumniProfileDTO.IsGraduated = false;
            if (!settings.EmailPublic) alumniProfileDTO.Email = null;
            if (!settings.PhonePublic) alumniProfileDTO.PhoneNumber = null;
            if (!settings.IsShowWorkExperiece) alumniProfileDTO.WorkExperience = null;
            alumniProfileDTO.PrivacySettings = null;
            return alumniProfileDTO;
        }
    }
}
