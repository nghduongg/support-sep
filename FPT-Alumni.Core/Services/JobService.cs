﻿using AutoMapper;
using FPT_Alumni.Core.Constants;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Exceptions;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Core.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Services
{
    public class JobService : BaseService<Job>,IJobService
    {
        ICommonService _commonService;
        IPostService _postService;
        IUnitOfWork _unitOfWork;
        readonly IMapper _mapper;
        public JobService(IBaseRepository<Job> baseRepository, IMapper mapper, ICommonService commonService, IPostService postService, IUnitOfWork unitOfWork) : base(baseRepository)
        {
            _mapper = mapper;
            _commonService = commonService;
            _postService = postService;
            _unitOfWork = unitOfWork;

        }

        public async Task<ServiceResult> AddNewJobAsync(JobDTO model, string accessToken)
        {
            Guid userId = await _commonService.GetUserIdByAccesTokenAsync(accessToken);


            Post post = _mapper.Map<Post>(model);
            post.Id = Guid.NewGuid();
            post.CreatedBy = userId;
            post.ModifiedBy = userId;
            post.CategoryId = Guid.Parse("3eebb25a-a866-463e-8bdd-c6293a8025b6");
            await _postService.InsertServiceAsync(post);

            Job job = _mapper.Map<Job>(model);
            job.Id = Guid.NewGuid();
            job.PostId = post.Id;
            await InsertServiceAsync(job);

            model.Id = job.Id;
            model.PostId = post.Id;

            return new ServiceResult
            {
                Success = true,
                Data = model,
                StatusCode = System.Net.HttpStatusCode.Created,
                UserMsg = "Create job successfully"
            };
        }

        public async Task<ServiceResult> GetMyJob(string accessToken, int currentPage, int pageSize)
        {
            Guid userId = await _commonService.GetUserIdByAccesTokenAsync(accessToken);

            List<JobDTO> jobDTOs = await _unitOfWork.Job.GetMyJob(userId, currentPage, pageSize);
            return new ServiceResult()
            {
                Success = true,
                StatusCode = System.Net.HttpStatusCode.OK,
                Data = jobDTOs,
                UserMsg = "Lấy thành công"
            };
        }

        public async Task<ServiceResult> GetOtherJob(string accessToken, int currentPage, int pageSize)
        {
            Guid userId = await _commonService.GetUserIdByAccesTokenAsync(accessToken);

            List<JobDTO> jobDTOs = await _unitOfWork.Job.GetOtherJob(userId, currentPage, pageSize);
            return new ServiceResult()
            {
                Success = true,
                StatusCode = System.Net.HttpStatusCode.OK,
                Data = jobDTOs,
                UserMsg = "Lấy thành công"
            };
        }

        public async Task<ServiceResult> ApprovingJobServiceAsync(Guid postId)
        {
            var result = await _unitOfWork.Job.ApproveJobAsync(postId);
            return new ServiceResult
            {
                StatusCode = result ? System.Net.HttpStatusCode.OK : System.Net.HttpStatusCode.BadRequest,
                Success = result
            };
        }

        public async Task<ServiceResult> DeleteJobAsync(Guid postId, Guid jobId)
        {
            if(postId == Guid.Empty || jobId == Guid.Empty)
            {
                return new ServiceResult
                {
                    Success = false,
                    StatusCode = System.Net.HttpStatusCode.NotFound,
                    UserMsg = "postId and jobId must not be empty."
                };
            }
            await _unitOfWork.Application.DeleteApplicationByJobIdAsync(jobId);
            await DeleteServiceAsync(jobId);
            await _postService.DeleteServiceAsync(postId);
            return new ServiceResult
            {
                Data = new {postId, jobId},
                Success = true,
                StatusCode = System.Net.HttpStatusCode.OK,
                UserMsg = "Delete job successfully"
            };
        }

        public async Task<ServiceResult> GetJobNeedApprove(string accessToken, int currentPage, int pageSize)
        {
            UserRoleDTO userRole = await _commonService.GetUserIdAndRoleByAccesTokenAsync(accessToken);
            if (userRole.Role == "Alumni")
            {
                throw new BadRequestException("Bạn không có quyền thực hiện hành động này");
            }

            List<JobDTO> jobDTOs = await _unitOfWork.Job.GetJobNeedApprove(currentPage, pageSize);
            return new ServiceResult()
            {
                Success = true,
                StatusCode = System.Net.HttpStatusCode.OK,
                Data = jobDTOs,
                UserMsg = "Lấy thành công"
            };
        }
    }
}
