﻿using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Services;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using FPT_Alumni.Core.Constants;

namespace FPT_Alumni.Core.Services
{
    public class CommentService : ICommentService
    {
        ICommonService _commonService;
        readonly IUnitOfWork _unitOfWork;

        public CommentService(ICommonService commonService, IUnitOfWork unitOfWork)
        {
            _commonService = commonService;
            _unitOfWork = unitOfWork;
        }
        public async Task<ServiceResult> DeleteServiceAsync(string id)
        {
            FirebaseResponse response = await FirebaseConstants.client.DeleteAsync("Comments/" + id);
            return new ServiceResult()
            {
                Success = true,
                StatusCode = System.Net.HttpStatusCode.Created,
            };
        }

        public async Task<ServiceResult> GetPageOfCommentsByPostIdAsync(Guid postId, int pageNumber, int pageSize)
        {
            FirebaseResponse response = await FirebaseConstants.client.GetAsync("Comments");
            dynamic data = JsonConvert.DeserializeObject<dynamic>(response.Body);
            var comments = new List<Comment>();
            var parentComments = new Dictionary<string, Comment>();

            if (data != null)
            {
                foreach (var item in data)
                {
                    Comment cmt = JsonConvert.DeserializeObject<Comment>(((JProperty)item).Value.ToString());
                    if (cmt.PostId.Equals(postId))
                    {
                        if (string.IsNullOrEmpty(cmt.ParentCommentId))
                        {
                            parentComments[cmt.Id] = cmt;
                        }
                        else
                        {
                            comments.Add(cmt);
                        }
                    }
                }

                foreach (var cmt in comments)
                {
                    if (parentComments.TryGetValue(cmt.ParentCommentId, out Comment parent))
                    {
                        if (parent.ChildComments == null)
                        {
                            parent.ChildComments = new List<Comment>();
                        }
                        parent.ChildComments.Add(cmt);
                    }
                }
            }
            var result = parentComments.Values.Reverse().Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            var res = new ServiceResult()
            {
                StatusCode = System.Net.HttpStatusCode.OK,
                DevMsg = result.Count > 0 ? "Data fetched successfully" : "No data found",
                UserMsg = result.Count > 0 ? "Data fetched successfully" : "No data found",
                Data = result,
                Success = result.Count > 0
            };

            return res;
        }

        public async Task<ServiceResult> GetCommentsByPostIdAsync(Guid postId)
        {
            FirebaseResponse response = await FirebaseConstants.client.GetAsync("Comments");
            dynamic data = JsonConvert.DeserializeObject<dynamic>(response.Body);
            var comments = new List<Comment>();
            var parentComments = new Dictionary<string, Comment>();

            if (data != null)
            {
                foreach (var item in data)
                {
                    Comment cmt = JsonConvert.DeserializeObject<Comment>(((JProperty)item).Value.ToString());
                    if (cmt.PostId.Equals(postId))
                    {
                        if (string.IsNullOrEmpty(cmt.ParentCommentId))
                        {
                            parentComments[cmt.Id] = cmt;
                        }
                        else
                        {
                            comments.Add(cmt);
                        }
                    }
                }

                foreach (var cmt in comments)
                {
                    if (parentComments.TryGetValue(cmt.ParentCommentId, out Comment parent))
                    {
                        if (parent.ChildComments == null)
                        {
                            parent.ChildComments = new List<Comment>();
                        }
                        parent.ChildComments.Add(cmt);
                    }
                }
            }
            var result = parentComments.Values.Reverse().ToList();
            var res = new ServiceResult()
            {
                StatusCode = System.Net.HttpStatusCode.OK,
                DevMsg = result != null && result.Count > 0 ? "Data fetched successfully" : "No data found",
                UserMsg = result != null && result.Count > 0 ? "Data fetched successfully" : "No data found",
                Data = result,
                Success = result != null && result.Count > 0
            };

            return res;
        }

        public async Task<ServiceResult> GetNumberOfCommentsByPostIdAsync(Guid postId)
        {
            var result = _unitOfWork.Comment.GetNumberOfCommentsByPostIdAsync(postId);
            var res = new ServiceResult()
            {
                StatusCode = System.Net.HttpStatusCode.OK,
                Data = result,
                Success = true
            };

            return res;
        }

        public async Task<ServiceResult> GetCommentsByParentCommentIdAsync(string parentCommentId)
        {
            FirebaseResponse response = await FirebaseConstants.client.GetAsync("Comments");
            dynamic data = JsonConvert.DeserializeObject<dynamic>(response.Body);
            var list = new List<Comment>();
            if (data != null)
            {
                foreach (var item in data)
                {
                    Comment cmt = JsonConvert.DeserializeObject<Comment>(((JProperty)item).Value.ToString());
                    if (cmt.ParentCommentId?.Equals(parentCommentId) == true)
                    {
                        list.Add(cmt);
                    }
                }
            }
            list.Reverse();
            var res = new ServiceResult()
            {
                StatusCode = System.Net.HttpStatusCode.OK,
                DevMsg = list != null && list.Count > 0 ? "Data fetched successfully" : "No data found",
                UserMsg = list != null && list.Count > 0 ? "Data fetched successfully" : "No data found",
                Data = list,
                Success = list != null && list.Count > 0
            };

            return res;
        }

        public async Task<ServiceResult> InsertServiceAsync(Comment entity, string accessToken)
        {
            UserRoleDTO user = await _commonService.GetUserIdAndRoleByAccesTokenAsync(accessToken);
            if (user.Role == "Alumni")
            {
                var alumni = await _unitOfWork.Alumni.GetAlumniByUserIdAsync(user.Id);
                entity.Fullname = alumni.FullName;
                entity.Avatar = alumni.Avartar;
            }
            else if (user.Role == "Staff")
            {
                var staff = await _unitOfWork.Staff.GetStaffByUserIdAsync(user.Id);
                entity.Fullname = staff.FullName;
                entity.Avatar = staff.Avartar;
            }
            entity.CreatedBy = user.Id;
            PushResponse response = await FirebaseConstants.client.PushAsync("Comments/", entity);
            entity.Id = response.Result.name;
            SetResponse setResponse = await FirebaseConstants.client.SetAsync("Comments/" + entity.Id, entity);
            return new ServiceResult()
            {
                Data = entity,
                Success = true,
                StatusCode = System.Net.HttpStatusCode.Created,
            };
        }

        public async Task<ServiceResult> UpdateServiceAsync(Comment entity)
        {
            SetResponse response = await FirebaseConstants.client.SetAsync("Comments/" + entity.Id, entity);

            return new ServiceResult()
            {
                Data = entity,
                Success = true,
                StatusCode = System.Net.HttpStatusCode.Created,
            };
        }
    }
}
