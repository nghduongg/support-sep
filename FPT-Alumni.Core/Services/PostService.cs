﻿using AutoMapper;
using Domain.Exceptions;
using FPT_Alumni.Core.Constants;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Enums;
using FPT_Alumni.Core.Exceptions;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Core.Interfaces.Services;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Services
{
    public class PostService : BaseService<Post>, IPostService
    {
        IMapper _mapper;
        ICommonService _commonService;
        IUnitOfWork _unitOfWork;
        ITokenService _tokenService;
        IAlumniService _alumniService;

        public PostService(IBaseRepository<Post> baseRepository, IMapper mapper, ICommonService commonService, IUnitOfWork unitOfWork, ITokenService tokenService, IAlumniService alumniService) : base(baseRepository)
        {
            _mapper = mapper;
            _commonService = commonService;
            _unitOfWork = unitOfWork;
            _tokenService = tokenService;
            _alumniService = alumniService;
        }

        public Task<List<PostDTO>> AutoPagingPostDTO(int page, int pagesize)
        {
            throw new NotImplementedException();
        }

        public async Task<ServiceResult> InsertPostDTOServiceAsync(PostInsertDTO postInsertDTO, string accessToken)
        {
            // email list to send after tag success
            List<string> emails = new();
            Post post = _mapper.Map<Post>(postInsertDTO);
            UserRoleDTO userRole = await _commonService.GetUserIdAndRoleByAccesTokenAsync(accessToken);
            if (userRole.Role == "Alumni" && postInsertDTO.CategoryId == PostCategoryConstant.STATUS)
            {
                throw new BadRequestException("Bạn không có quyền thực hiện hành động này");
            }
            if (userRole.Role == "Alumni" && post.GroupId != null)
            {
                throw new NotFoundException("Bạn không có quyền thực hiện hành động này");
            }
            post.CreatedBy = userRole.Id;
            post.ModifiedBy = userRole.Id;
            post.Id = Guid.NewGuid();

            _unitOfWork.BeginTransaction();
            await _unitOfWork.Post.InsertAsync(post);

            // Insert media
            if (postInsertDTO.MediaInsertDTOs.Count > 0)
            {
                var mediaInsertTasks = postInsertDTO.MediaInsertDTOs
               .Select(async mediaDTO =>
               {
                   var media = _mapper.Map<Media>(mediaDTO);
                   media.Id = Guid.NewGuid(); // Set the Id for Media
                   media.PostId = post.Id;
                   await _unitOfWork.Media.InsertAsync(media);
               }).ToList();
                await Task.WhenAll(mediaInsertTasks);
            }

            // Insert user tags
            if (postInsertDTO.UserTagedIds.Count > 0)
            {
                var postUserInsertTasks = postInsertDTO.UserTagedIds
                .Select(async userId =>
                    {
                        var email = await _unitOfWork.User.GetEmailByIdAsync(userId);
                        emails.Add(email);
                        var postUser = new PostUser { PostId = post.Id, UserId = userId };
                        await _unitOfWork.PostUser.InsertAsync(postUser);
                    }).ToList();
                await Task.WhenAll(postUserInsertTasks);
            }
            else
            {
                var postUser = new PostUser { PostId = post.Id, UserId = null };
                await _unitOfWork.PostUser.InsertAsync(postUser);
            }

            if (postInsertDTO.MajorTagIds.Count > 0)
            {
                // Insert tags
                var postTagInsertTasks = postInsertDTO.MajorTagIds
                    .Select(async tagId =>
                    {
                        if (await _unitOfWork.Tag.IsParamValueExistAsync<Guid>(new Tag(), "Id", tagId))
                        {
                            var postTag = new PostTag { PostId = post.Id, TagId = tagId };
                            await _unitOfWork.PostTag.InsertAsync(postTag);
                        }
                    }).ToList();
                await Task.WhenAll(postTagInsertTasks);
            }

            // Insert fields
            if (postInsertDTO.FiledIds.Count > 0)
            {
                var postFieldInsertTasks = postInsertDTO.FiledIds
                    .Select(async filedId =>
                    {
                        if (await _unitOfWork.Field.IsParamValueExistAsync<Guid>(new Field(), "Id", filedId))
                        {
                            var postField = new PostField { PostId = post.Id, FieldId = filedId };
                            await _unitOfWork.PostFiled.InsertAsync(postField);
                        }
                        else
                        {
                            throw new BadRequestException("Trường lĩnh vực này không tồn tại");
                        }
                    }).ToList();
                await Task.WhenAll(postFieldInsertTasks);
            }
            else
            {
                throw new BadRequestException("Trường lĩnh vực không được phép để trống");
            }

            _unitOfWork.Commit();

            if (emails.Count > 0)
            {
                _ = Task.Run(async () =>
                {
                    foreach (var email in emails)
                    {
                        BodyMail.Title = "<p>Bạn đã được nhắc đến trong 1 bài viết tại FPT Alumni</p>";
                        BodyMail.Content = "<p>Bạn đã được nhắc đến trong 1 bài viết tại FPT Alumni</p>";
                        BodyMail.LinkRedirect = "https://www.facebook.com/ALUMNI.FPTU";
                        var mailDTO = new MailDTO
                        {
                            To = email,
                            Subject = "Bạn đã được nhắc đến trong 1 bài viết tại FPT Alumni",
                            Body = BodyMail.GetBody()
                        };
                        await _commonService.SendMailAsync(mailDTO);
                    }
                });
            }

            return new ServiceResult
            {
                Success = true,
                Data = 1,
                StatusCode = System.Net.HttpStatusCode.Created,
                UserMsg = "Create post successfully"
            };
        }

        public async Task<ServiceResult> UpdatePostStatusServiceAsync(string accessToken, Guid postId, string status)
        {
            Guid ownId = await _commonService.GetUserIdByAccesTokenAsync(accessToken);
            // check is post exist
            Post post = await _unitOfWork.Post.FindByIdAsync(postId);
            if (post == null)
            {
                throw new BadRequestException("Bài viết không hợp lệ để cập nhật");
            }
            // user is not own's post
            if (post.CreatedBy != ownId)
            {
                throw new BadRequestException("Bạn không có quyền thực hiện hành động này");
            }
            // invalid status
            if (!int.TryParse(status, out int validStatus) || (validStatus != 1 && validStatus != 0))
            {
                throw new BadRequestException("Trạng thái không hợp lệ để thay đổi");
            }

            int res = await _unitOfWork.Post.UpdatePostStatusAsync(postId, validStatus);
            if (res > 0)
            {
                return new ServiceResult()
                {
                    Success = true,
                    StatusCode = System.Net.HttpStatusCode.OK,
                    Data = res,
                    UserMsg = "Cập nhật trạng thái thành công"
                };
            }
            throw new Exception("Lỗi hệ thống, không thể cập nhật trạng thái, vui lòng thử lại sau");
        }

        public async Task<ServiceResult> BeforeUpdateAsync(PostUpdateDTO postUpdateDTO, string accessToken)
        {
            // email list to send after tag success
            List<string> emails = new();
            Post postToUpdate = _mapper.Map<Post>(postUpdateDTO);
            PostDTO postDTOInDb = await _unitOfWork.Post.GetPostDTOByIdAsync(postToUpdate.Id, 1);
            // post do not exsit
            if (postDTOInDb == null)
            {
                throw new NotFoundException("Bài viết này không tồn tại");
            }
            //user is not own of post
            Guid guestId = await _commonService.GetUserIdByAccesTokenAsync(accessToken);
            if (guestId != postDTOInDb.CreatedId)
            {
                throw new BadRequestException("Bạn không có quyền thực hiện hành động này");
            }
            postToUpdate.CreatedBy = postDTOInDb.CreatedId;
            postToUpdate.ModifiedBy = postDTOInDb.CreatedId;
            postToUpdate.ModifiedDate = DateTime.Now;
            _unitOfWork.BeginTransaction();
            await _unitOfWork.Post.UpdateAsync(postToUpdate);


            //check media of old post > 0 -> delete all
            if (postDTOInDb.Medias.Count > 0 || (postUpdateDTO.MediaInsertDTOs?.Count > 0))
            {
                //delete all media by post id
                await _unitOfWork.Media.DeleteByPostIdAsync(postToUpdate.Id);
                //update all new media
                if (postUpdateDTO.MediaInsertDTOs?.Count > 0)
                {
                    var mediaInsertTasks = postUpdateDTO.MediaInsertDTOs.Select(mediaDTO =>
                    {
                        var media = _mapper.Map<Media>(mediaDTO);
                        media.Id = Guid.NewGuid();
                        media.PostId = postToUpdate.Id;
                        return _unitOfWork.Media.InsertAsync(media);
                    }).ToList();
                    await Task.WhenAll(mediaInsertTasks);
                }
            }

            //check Fields of old post > 0 ->delete all
            if (postDTOInDb.Fields.Count > 0 || (postUpdateDTO.FiledIds?.Count > 0))
            {
                //delete all field by post id
                await _unitOfWork.PostFiled.DeleteByPostIdAsync(postUpdateDTO.Id);
                //update all new field
                if (postUpdateDTO.FiledIds?.Count > 0)
                {
                    var postFieldInsertTasks = postUpdateDTO.FiledIds.Select(async filedId =>
                      {
                          if (await _unitOfWork.Field.IsParamValueExistAsync<Guid>(new Field(), "Id", filedId))
                          {
                              var postField = new PostField { PostId = postToUpdate.Id, FieldId = filedId };
                              await _unitOfWork.PostFiled.InsertAsync(postField);
                          }
                          else
                          {
                              throw new BadRequestException("Trường lĩnh vực này không tồn tại");
                          }
                      }).ToList();
                    await Task.WhenAll(postFieldInsertTasks);
                }

            }

            // major tag
            if (postDTOInDb.Tags.Count > 0)
            {
                var existingTag = postDTOInDb.Tags.Select(t => t.Id).ToList();
                var tagIdsToDelete = existingTag.Except(postUpdateDTO.MajorTagIds).ToList();
                // delete all tags not matching with new tags
                await _unitOfWork.PostTag.MultipleDeleteByPostIdAndTagId(postToUpdate.Id, tagIdsToDelete);
                var postTagInsertTasks = postUpdateDTO.MajorTagIds
                .Select(async tagId =>
                {
                    if (!existingTag.Contains(tagId))
                    {
                        if (await _unitOfWork.Tag.IsParamValueExistAsync<Guid>(new Tag(), "Id", tagId))
                        {
                            var postTag = new PostTag { PostId = postToUpdate.Id, TagId = tagId };
                            await _unitOfWork.PostTag.InsertAsync(postTag);
                        }
                    }
                }).ToList();
                await Task.WhenAll(postTagInsertTasks);
            }

            // no one is tagged in new post
            if (postUpdateDTO.UserTagedIds.Count == 0 && postDTOInDb.UserTaggedInPostDTOs.Count > 0)
            {
                await _unitOfWork.PostUser.DeleteByPostIdAsync(postToUpdate.Id);
                // create fake userId
                PostUser postUser = new() { PostId = postToUpdate.Id, UserId = null };
                await _unitOfWork.PostUser.InsertAsync(postUser);
            }
            else if (postDTOInDb.UserTaggedInPostDTOs.Count > 0 || (postUpdateDTO.UserTagedIds?.Count > 0))
            {
                var existingUser = postDTOInDb.UserTaggedInPostDTOs.Select(t => t.UserId);
                var userIdsToDelete = existingUser.Except(postUpdateDTO.UserTagedIds).ToList();
                // delete all tag not math with new tag
                if (userIdsToDelete.Count > 0)
                {
                    await _unitOfWork.PostUser.MultipleDeleteByPostIdAndTagId(postToUpdate.Id, userIdsToDelete);
                }
                var postUserInsertTasks = postUpdateDTO.UserTagedIds
                .Select(async userId =>
                {
                    if (!existingUser.Contains(userId))
                    {
                        if (await _unitOfWork.User.IsParamValueExistAsync<Guid>(new User(), "Id", userId))
                        {
                            PostUser postUser = new()
                            {
                                PostId = postToUpdate.Id,
                                UserId = userId,
                            };
                            await _unitOfWork.PostUser.InsertAsync(postUser);
                            var email = await _unitOfWork.User.GetEmailByIdAsync(userId);
                            emails.Add(email);
                        }
                    }
                }).ToList();
                await Task.WhenAll(postUserInsertTasks);
            }

            _unitOfWork.Commit();
            if (emails.Count > 0)
            {
                _ = Task.Run(async () =>
                {
                    foreach (var email in emails)
                    {
                        BodyMail.Title = "<p>Bạn đã được nhắc đến trong 1 bài viết tại FPT Alumni</p>";
                        BodyMail.Content = "<p>Bạn đã được nhắc đến trong 1 bài viết tại FPT Alumni</p>";
                        BodyMail.LinkRedirect = "https://www.facebook.com/ALUMNI.FPTU";
                        var mailDTO = new MailDTO
                        {
                            To = email,
                            Subject = "Bạn đã được nhắc đến trong 1 bài viết tại FPT Alumni",
                            Body = BodyMail.GetBody()
                        };
                        await _commonService.SendMailAsync(mailDTO);
                    }
                });
            }
            return new ServiceResult()
            {
                Success = true,
                Data = 1,
                StatusCode = System.Net.HttpStatusCode.OK,
                UserMsg = "Create post succcessfully"
            };
        }


        public async Task<ServiceResult> FilterPagingInUserProfile(int currentPage, int pageSize, string accessToken,
     Guid ownId, Guid[]? filedIds,
     string key, DateTime? fromDate, DateTime? toDate)
        {
            UserRoleDTO userRole = await _commonService.GetUserIdAndRoleByAccesTokenAsync(accessToken);
            string validKey = _commonService.RemoveTextSpace(key);
            validKey = _commonService.RemoveUnicode(validKey);
            ServiceResult result = new ServiceResult()
            {
                Success = true,
                StatusCode = System.Net.HttpStatusCode.OK,
                UserMsg = "Lấy thành công"
            };

            List<PostDTO> postDTOs = new();
            AlumniProfileDTO profileDTO = new();
            // own view -> filter paging related post
            if (ownId == userRole.Id)
            {
                postDTOs = await _unitOfWork.Post.FilterPagingInUserPorfile(currentPage, pageSize, ownId, filedIds, validKey, fromDate, toDate);
                if (postDTOs?.Count > 0)
                {
                    foreach (var postDTO in postDTOs)
                    {
                        postDTO.NumberOfComment = _unitOfWork.Comment.GetNumberOfCommentsByPostIdAsync(postDTO.Id);
                    }
                }
                result.Data = postDTOs;
            }
            // guest view -> get info only
            else
            {
                profileDTO = await _unitOfWork.Alumni.GetAlumniProfileByUserIdAsync(ownId);
                profileDTO = _alumniService.HiddenDataBaseSettings(profileDTO);
                result.Data = profileDTO;
            }
            return result;
        }

        public async Task<ServiceResult> FilterPagingInGroup(int currentPage, int pageSize, string accessToken, Guid groupId, Guid[]? filedIds, string key, DateTime? startDate, DateTime? endDate)
        {
            UserRoleDTO userRole = await _commonService.GetUserIdAndRoleByAccesTokenAsync(accessToken);
            string validKey = _commonService.RemoveTextSpace(key);
            validKey = _commonService.RemoveUnicode(validKey);
            List<PostDTO> postDTOs = await _unitOfWork.Post.FilterPagingInGroup(currentPage, pageSize, groupId, filedIds, validKey, startDate, endDate);

            if (postDTOs?.Count > 0)
            {
                foreach (var postDTO in postDTOs)
                {
                    postDTO.NumberOfComment = _unitOfWork.Comment.GetNumberOfCommentsByPostIdAsync(postDTO.Id);
                }
            }
            return new ServiceResult()
            {
                Success = true,
                StatusCode = System.Net.HttpStatusCode.OK,
                Data = postDTOs,
                UserMsg = "Lấy thành công"
            };
        }
    }
}
