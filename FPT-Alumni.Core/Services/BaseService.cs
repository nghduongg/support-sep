﻿using Domain.Exceptions;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Core.Interfaces.Services;


namespace FPT_Alumni.Core.Services
{
    public class BaseService<T> : IBaseService<T> where T : class
    {
        protected IBaseRepository<T> _baseRepository;
        public BaseService(IBaseRepository<T> baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public virtual async Task<ServiceResult> FindByIdServiceAsync(Guid id)
        {
            var entity = await _baseRepository.FindByIdAsync(id);
            if (entity != null)
            {
                return new ServiceResult()
                {
                    Data = entity,
                    StatusCode = System.Net.HttpStatusCode.OK,
                    Success = true,
                };
            }
            throw new NotFoundException("Data Not Found");
        }

        public virtual async Task<ServiceResult> DeleteServiceAsync(Guid id)
        {
            int res = await _baseRepository.DeleteAsync(id);
            if (res > 0)
            {
                return new ServiceResult()
                {
                    Data = res,
                    Success = true,
                    StatusCode = System.Net.HttpStatusCode.OK,

                };
            }
            throw new NotFoundException("Data Not Found");
        }

        public virtual async Task<ServiceResult> InsertServiceAsync(T entity)
        {
            int res = await _baseRepository.InsertAsync(entity);
            if (res > 0)
            {
                return new ServiceResult()
                {
                    Data = res,
                    Success = true,
                    StatusCode = System.Net.HttpStatusCode.Created,
                };
            }
            return new ServiceResult()
            {
                Data = res,
                Success = false,
                StatusCode = System.Net.HttpStatusCode.BadRequest,
            };
        }

        public virtual async Task<ServiceResult> UpdateServiceAsync(T entity)
        {
            int res = await _baseRepository.UpdateAsync(entity);
            if (res > 0)
            {
                return new ServiceResult()
                {
                    Data = res,
                    Success = true,
                    StatusCode = System.Net.HttpStatusCode.OK,

                };
            }
            return new ServiceResult()
            {
                Data = res,
                Success = false,
                StatusCode = System.Net.HttpStatusCode.BadRequest,

            };
        }
    }
}
