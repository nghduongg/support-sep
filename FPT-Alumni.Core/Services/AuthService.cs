﻿using AutoMapper;
using FPT_Alumni.Core.Constants;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Services;
using FPT_Alumni.Core.JsonModels;
using Google.Apis.Auth;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Net;


namespace FPT_Alumni.Core.Services
{
    public class AuthService : IAuthService
    {
        readonly ITokenService _tokenService;
        readonly  IMapper _mapper;
        readonly IUnitOfWork _unitOfWork;
        readonly IConfiguration _configuration;
        readonly ICommonService _commonService;


        public AuthService(ITokenService tokenService,
            IMapper mapper, IUnitOfWork unitOfWork, IConfiguration configuration, ICommonService commonService)
        {
            _configuration = configuration;
            _tokenService = tokenService;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _commonService = commonService;
        }

        public async Task<ServiceResult> AuthenticateAsync(string credential)
        {
            var settings = new GoogleJsonWebSignature.ValidationSettings()
            {
                Audience = new List<string> { _configuration["SSO:ClientId"].ToString() }
            };
            GoogleJsonWebSignature.Payload payload;
            payload = await GoogleJsonWebSignature.ValidateAsync(credential, settings);
            var res = await _unitOfWork.User.GetByEmailAsync(payload.Email);
            if (res == null)
            {
                return new ServiceResult
                {
                    StatusCode = HttpStatusCode.OK,
                    Success = true,
                    Data = new UnverifiedModel
                    {
                        EmailLogged = payload.Email,
                        FullName = payload.Name,
                        IsExisted = false
                    },
                };
            }
           
            var u = _mapper.Map<User>(res);
            u.EmailLogged = payload.Email;


            TokenResultDTO tokenModel = await _tokenService.JWTGeneratorAsync(u);
            return new ServiceResult
            {
                StatusCode = HttpStatusCode.OK,
                Data = new
                {
                    Id = res.Id,
                    MainEmail = res.MainEmail,
                    RoleName = res.RoleName,
                    FullName = res.FullName
                },
                Success = true
            };
        }

        public async Task<ServiceResult> AuthenticateAsync(LoginModel loginModel)
        {
            var res = await _unitOfWork.User.GetByEmailAsync(loginModel.EmailLogged!);
            if (res == null)
            {
                return new ServiceResult
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Success = false,
                    DevMsg = "Login Failed. Invalid Email or Password!"
                };
            }

            // So sánh mật khẩu đã nhập với mật khẩu đã hash trong cơ sở dữ liệu
            if (!BCrypt.Net.BCrypt.Verify(loginModel.Password, res.Password))
            {
                return new ServiceResult
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Success = false,
                    DevMsg = "Login Failed. Invalid Email or Password!"
                };
            }

            var user = _mapper.Map<User>(res);
            user.EmailLogged = loginModel.EmailLogged;
           
            //await _unitOfWork.User.UpdateAsync(res);

            TokenResultDTO tokenModel = await _tokenService.JWTGeneratorAsync(user);
            return new ServiceResult
            {
                StatusCode = HttpStatusCode.OK,
                Data = new
                {
                    Id = res.Id,
                    MainEmail = res.MainEmail,
                    RoleName = res.RoleName,
                    FullName = res.FullName
                },
                Success = true
            };
        }


        public async Task<ServiceResult> RegisterServiceAsync(UnverifiedModel unauthenticatedModel)
        {
            UserReturnDTO user = await _unitOfWork.User.GetByEmailAsync(unauthenticatedModel.EmailLogged);
            if (user != null)
            {
                return new ServiceResult
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    DevMsg = "Email existed in db",
                    UserMsg = "Email existed in system",
                    Success = false
                };
            }

            var userSave = _mapper.Map<User>(unauthenticatedModel);
            var userId = Guid.NewGuid();
            userSave.Id = userId;
            userSave.MainEmail = unauthenticatedModel.EmailLogged;
            userSave.SubEmail = unauthenticatedModel.EmailLogged;
            userSave.Status = Enums.UserStatusEnum.UNAUTHORIZE.ToString();
            userSave.RoleId = RoleConstants.ALUMNI;

            string password = _commonService.GenerateRandomPassword(10);
            userSave.Password = BCrypt.Net.BCrypt.HashPassword(password); //  password hashing
            var insertUserResult = await _unitOfWork.User.InsertAsync(userSave);

            var alumniSave = _mapper.Map<Alumni>(unauthenticatedModel);
            var alumniId = Guid.NewGuid();
            alumniSave.Id = alumniId;
            alumniSave.UserId = userId;
            alumniSave.CreatedBy = userId;
            alumniSave.ModifiedBy = userId;
            PrivacySettings p = new PrivacySettings();
            alumniSave.PrivacySettings = JsonConvert.SerializeObject(p);
            await _unitOfWork.Alumni.InsertAsync(alumniSave);

            // Lưu thông tin các field của user
            foreach (var fieldId in unauthenticatedModel.FieldIds)
            {
                var userField = new AlumniField
                {
                    UserId = userId,
                    FieldId = fieldId
                };
                await _unitOfWork.AlumniField.InsertAsync(userField);
            }


            if (insertUserResult > 0)
            {
                BodyMail.Title = "Your alumni account: \n";
                BodyMail.Content = $@"  <p>Email: {unauthenticatedModel.EmailLogged}</p>
                                        <p>Password:{password}</p>";

                BodyMail.LinkRedirect = "https://www.facebook.com/ALUMNI.FPTU";
                MailDTO mailDTO = new MailDTO()
                {
                    To = userSave.MainEmail,
                    Subject = "Welcome to FAL-FPT Alumni Portal",
                    Body = BodyMail.GetBody()
                };
                var checkSend = await _commonService.SendMailAsync(mailDTO);
                if (!checkSend)
                {
                    return new ServiceResult()
                    {
                        Data = insertUserResult,
                        Success = true,
                        StatusCode = System.Net.HttpStatusCode.Created,
                        UserMsg = "Send Email Fail"
                    };
                }
            }

            //if registered by form --> generate password,  send noti and password to email registered
            return new ServiceResult
            {
                StatusCode = HttpStatusCode.OK,
                Success = true,
                DevMsg = "Signup by form success. Password auto generate and send to email",
                UserMsg = "Signup success, Please check your email to get your password"
            };
        }
    }
}
