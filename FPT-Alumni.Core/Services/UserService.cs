﻿using AutoMapper;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using System.Net;
using FPT_Alumni.Core.Constants;
using FPT_Alumni.Core.Exceptions;
using FPT_Alumni.Core.JsonModels;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Core.Interfaces.Services;
using System.Security.Claims;

namespace FPT_Alumni.Core.Services
{
    public class UserService : BaseService<User>, IUserService
    {
        readonly IUserRepository _userRepository;
        readonly IUnitOfWork _unitOfWork;
        readonly ITokenService _tokenService;
        readonly IMapper _mapper;
        readonly ICommonService _commonService;
        readonly IConfiguration _configuration;
        public UserService(IUserRepository userRepository, IUnitOfWork unitOfWork,
           ITokenService tokenService, IMapper mapper,
           ICommonService commonService, IConfiguration configuration) : base(userRepository)
        {
            _unitOfWork = unitOfWork;
            _userRepository = userRepository;
            _tokenService = tokenService;
            _mapper = mapper;
            _commonService = commonService;
            _configuration = configuration;
        }

        public async Task<ServiceResult> GetAllAsync()
        {
            var res = await _unitOfWork.User.GetAllAsync();
            var response = new ServiceResult()
            {
                StatusCode = res != null && res.Count > 0 ? System.Net.HttpStatusCode.OK : System.Net.HttpStatusCode.NotFound,
                DevMsg = res != null && res.Count > 0 ? "Data fetched successfully" : "No data found",
                UserMsg = res != null && res.Count > 0 ? "Data fetched successfully" : "No data found",
                Data = res,
                Success = res != null && res.Count > 0
            };

            return response;
        }

        public async Task<ServiceResult> GetUserServiceAsync(string? accessToken)
        {
            var result = new ServiceResult()
            {
                StatusCode = HttpStatusCode.NoContent,
                Success = false
            };

            if (string.IsNullOrEmpty(accessToken))
            {
                result.StatusCode = HttpStatusCode.BadRequest;
                result.Success = false;
                result.DevMsg = "Invalid access token";
            }

            var principal = await _tokenService.GetPrincipalFromExpiredTokenAsync(accessToken!);
            if (principal == null)
            {
                result.StatusCode = HttpStatusCode.Unauthorized;
                result.Success = false;
                result.DevMsg = "Invalid or expired access token";
            }

            Guid userId = new Guid(principal!.FindFirst("Id")!.Value);
            string status = principal.FindFirst("Status")!.Value;
            string role = principal.FindFirst(ClaimTypes.Role)!.Value;
            string email = principal.FindFirst(ClaimTypes.Email)!.Value;
            var tabs = HelperConstants.Tabs;
            switch (role)
            {
                case "Alumni":
                    var alumni = await _unitOfWork.Alumni.GetAlumniByUserIdAsync(userId);
                    if (alumni == null)
                    {
                        result.StatusCode = HttpStatusCode.NotFound;
                        result.Success = false;
                        result.DevMsg = "Alumni not found";
                    }
                    result.StatusCode = HttpStatusCode.OK;
                    result.Success = true;
                    result.Data = new { alumni, tabs };
                    break;
                case "Staff":
                    var staff = await _unitOfWork.Staff.GetStaffByUserIdAsync(userId);
                    if (staff == null)
                    {
                        result.StatusCode = HttpStatusCode.NotFound;
                        result.Success = false;
                        result.DevMsg = "Staff not found";
                    }
                    result.StatusCode = HttpStatusCode.OK;
                    result.Success = true;
                    result.Data = new { staff, tabs };
                    break;
                case "Admin":
                    var admin = new UserModelView();
                    admin.UserId = userId;
                    admin.Status = status;
                    admin.RoleName = role;
                    admin.MainEmail = email;
                    result.StatusCode = HttpStatusCode.OK;
                    result.Success = true;
                    result.Data = new { admin, tabs };
                    break;
                default:
                    result.StatusCode = HttpStatusCode.Forbidden;
                    result.Success = false;
                    result.DevMsg = "Unauthorized role";
                    break;
            }

            return result;
        }

        public override async Task<ServiceResult> InsertServiceAsync(User user)
        {
            user.Id = Guid.NewGuid();
            user.Password = _commonService.GenerateRandomPassword(10);
            int res = await _baseRepository.InsertAsync(user);


            BodyMail.Title = "";
            BodyMail.Content = "";
            BodyMail.LinkRedirect = "https://www.facebook.com/ALUMNI.FPTU";
            if (res > 0)
            {
                MailDTO mailDTO = new MailDTO()
                {
                    To = user.MainEmail,
                    Subject = "Welcome to FAL-FPT Alumni Portal",
                    Body = BodyMail.GetBody()
                };
                var checkSend = await _commonService.SendMailAsync(mailDTO);
                if (checkSend)
                {
                    return new ServiceResult()
                    {
                        Data = res,
                        Success = true,
                        StatusCode = System.Net.HttpStatusCode.Created,
                    };
                }
                return new ServiceResult()
                {
                    Data = res,
                    Success = true,
                    StatusCode = System.Net.HttpStatusCode.Created,
                    UserMsg = "Send Email Fail"
                };
            }
            return new ServiceResult()
            {
                Data = res,
                Success = false,
                StatusCode = System.Net.HttpStatusCode.BadRequest,
            };
        }

        public async Task<ServiceResult> SettingUpdateServiceAsync(UserModelSave alumniWithUserModel, string? accesstoken)
        {
            var userId = await _commonService.GetUserIdByAccesTokenAsync(accesstoken);
            var result = 0;
            var resultSaveField = 0;
            var u = await _unitOfWork.User.GetUserByIdAsync(userId);

            if (u.RoleId.Equals(RoleConstants.ALUMNI))
            {
                UserModelView existedAlumni = await _unitOfWork.Alumni.GetAlumniByUserIdAsync(userId);
                if (existedAlumni != null)
                {
                    if (alumniWithUserModel.IsGraduated == null)
                    {
                        alumniWithUserModel.IsGraduated = existedAlumni.IsGraduated;
                    }
                    if (alumniWithUserModel.Gender == null)
                    {
                        alumniWithUserModel.Gender = existedAlumni.Gender;
                    }
                }


                if (!string.IsNullOrEmpty(alumniWithUserModel.Tab) && alumniWithUserModel.Tab.Equals("Contact details"))
                {
                    bool isEmailTakenByAnotherUser = false;
                    if (!string.IsNullOrEmpty(alumniWithUserModel.MainEmail))
                    {
                        isEmailTakenByAnotherUser = await _unitOfWork.User.IsEmailTakenByAnotherUserAsync(alumniWithUserModel.MainEmail, userId);
                    }

                    if (isEmailTakenByAnotherUser)
                    {
                        return new ServiceResult()
                        {
                            Success = false,
                            StatusCode = System.Net.HttpStatusCode.Conflict,
                            DevMsg = "Your email entered is taken by another user"
                        };
                    }

                    var updateUser = _mapper.Map<User>(alumniWithUserModel);
                    updateUser.Password = alumniWithUserModel.Password;
                    updateUser.Id = userId;
                    var updateUserResult = await _unitOfWork.User.UpdateByUserIdAsync(updateUser);

                    if (updateUserResult > 0 || updateUserResult == -1) result++;
                }

                var alumni = _mapper.Map<Alumni>(alumniWithUserModel);
                alumni.ModifiedBy = userId;
                alumni.ModifiedDate = DateTime.UtcNow;
                alumni.UserId = userId;
                var updateResult = await _unitOfWork.Alumni.UpdateAlumniByUserIdAsync(alumni);
                if (updateResult > 0 || updateResult == -1) result++;

                if (alumniWithUserModel.FieldIds != null)
                {
                    resultSaveField = await _unitOfWork.Alumni.UpdateAlumniFieldsAsync(userId, alumniWithUserModel.FieldIds);
                }
            }
            else
            {
                var staff = _mapper.Map<Staff>(alumniWithUserModel);
                staff.UserId = userId;
                var updateResult = await _unitOfWork.Staff.UpdateStaffByUserIdAsync(staff);
                if (updateResult > 0 || updateResult == -1) result++;
            }

            if (result <= 0)
            {
                return new ServiceResult()
                {
                    Data = result,
                    Success = false,
                    DevMsg = "Invalid information ",
                    UserMsg = "Invalid information ",
                    StatusCode = System.Net.HttpStatusCode.BadRequest,
                };
            }

            if (result > 0 && resultSaveField > 0)
            {
                return new ServiceResult()
                {
                    Data = result + resultSaveField,
                    Success = true,
                    DevMsg = "profile Updated successfully ",
                    UserMsg = "profile Updated successfully ",
                    StatusCode = System.Net.HttpStatusCode.OK,
                };
            }

            if (resultSaveField <= 0 && result > 0)
            {
                return new ServiceResult()
                {
                    Data = result,
                    Success = true,
                    DevMsg = "profile Updated successfully.  Can't update fields",
                    UserMsg = "profile Updated successfully. Can't update fields",
                    StatusCode = System.Net.HttpStatusCode.OK,
                };
            }

            return new ServiceResult()
            {
                Data = result,
                Success = true,
                DevMsg = "Updated fields successfully",
                UserMsg = "Updated fields successfully",
                StatusCode = System.Net.HttpStatusCode.OK,
            };
        }

        public async Task<ServiceResult> CreateNewWorkExperienceServiceAsync(WorkExperienceSaveDTO workExperienceDTO)
        {
            UserModelView existedAlumni = await _unitOfWork.Alumni.GetAlumniByUserIdAsync(workExperienceDTO.UserId);
            if (existedAlumni == null)
            {
                return new ServiceResult()
                {
                    Data = existedAlumni,
                    Success = false,
                    StatusCode = System.Net.HttpStatusCode.BadRequest,
                    DevMsg = "Not found alumni for create new work experience"
                };
            }

            WorkExperience newWorkExperience = _mapper.Map<WorkExperience>(workExperienceDTO);
            List<WorkExperience> WorkExperience = existedAlumni.WorkExperiences ?? new List<WorkExperience>();
            WorkExperience.Add(newWorkExperience);
            string updatedWorkExperienceJson = JsonConvert.SerializeObject(WorkExperience);

            var insertResult = await _unitOfWork.Alumni.UpdateWorkExperienceByUserIdAsync(updatedWorkExperienceJson, existedAlumni.UserId); ;

            if (insertResult > 0)
            {
                return new ServiceResult()
                {
                    Data = insertResult,
                    Success = true,
                    StatusCode = System.Net.HttpStatusCode.OK,
                    DevMsg = "Inserted new Work experience"
                };
            }

            throw new InternalServerException("Internal error. Can't insert new work experience");
        }

        public async Task<ServiceResult> CreateNewEducationExperienceServiceAsync(EducationExperienceSaveDTO educationExperienceSaveDTO)
        {
            UserModelView existedAlumni = await _unitOfWork.Alumni.GetAlumniByUserIdAsync(educationExperienceSaveDTO.UserId);
            if (existedAlumni == null)
            {
                return new ServiceResult()
                {
                    Data = existedAlumni,
                    Success = false,
                    StatusCode = System.Net.HttpStatusCode.BadRequest,
                    DevMsg = "Not found alumni for create new education experience"
                };
            }

            EducationExperience newEducationExperience = _mapper.Map<EducationExperience>(educationExperienceSaveDTO);
            List<EducationExperience> educationExperiences = existedAlumni.EducationExperiences ?? new List<EducationExperience>();
            educationExperiences.Add(newEducationExperience);
            string updatedEducationExperienceJson = JsonConvert.SerializeObject(educationExperiences);


            var insertResult = await _unitOfWork.Alumni.UpdateEducationExperienceByUserIdAsync(updatedEducationExperienceJson, existedAlumni.UserId);
            if (insertResult > 0)
            {
                return new ServiceResult()
                {
                    Data = insertResult,
                    Success = true,
                    StatusCode = System.Net.HttpStatusCode.OK,
                    DevMsg = "Inserted new Education experience"
                };
            }

            throw new InternalServerException("Internal error. Can't insert new Education experience");
        }

        public async Task<ServiceResult> GetUserByEmailServiceAsync(string? email)
        {
            var user = await _unitOfWork.User.GetByEmailAsync(email!);
            if (user == null)
            {
                return new ServiceResult()
                {
                    Data = user,
                    Success = true,
                    StatusCode = System.Net.HttpStatusCode.NotFound,
                    DevMsg = "Your email you entered does not existed in system"
                };
            }

            string token = _tokenService.GenerateChangePasswordToken(user.Id);
            BodyMail.Title = "Change password";
            BodyMail.Content = "Click to change password " + $"<a href=\"{_configuration["ChangePasswordUrl"]}?token={token}\">Change Password</a>";
            BodyMail.LinkRedirect = "https://www.facebook.com/ALUMNI.FPTU";

            MailDTO mailDTO = new MailDTO()
            {
                To = email,
                Subject = "FAL-FPT Alumni Portal - Forgot Password",
                Body = BodyMail.GetBody()
            };
            var checkSend = await _commonService.SendMailAsync(mailDTO);
            if (checkSend)
            {
                return new ServiceResult()
                {
                    DevMsg = "Send email to change password successfully",
                    Success = true,
                    StatusCode = System.Net.HttpStatusCode.OK,
                };
            }
            return new ServiceResult()
            {
                Success = false,
                StatusCode = System.Net.HttpStatusCode.Created,
                UserMsg = "Send Email Fail"
            };
        }

        public async Task<ServiceResult> ChangePasswordServiceAsync(ChangePasswordModel changePasswordModel)
        {
            var isMatched = true;
            if (!string.IsNullOrEmpty(changePasswordModel.CurrentPassword))
            {
                isMatched = await _unitOfWork.User.CheckPasswordAsync(changePasswordModel.CurrentPassword, changePasswordModel.Id);
            }

            if (!isMatched)
            {
                return new ServiceResult
                {
                    Success = false,
                    StatusCode = System.Net.HttpStatusCode.Unauthorized,
                    DevMsg = "Current password entered is incorrect"
                };
            }

            User user = _mapper.Map<User>(changePasswordModel);
            user.Password = BCrypt.Net.BCrypt.HashPassword(user.Password);
            var result = await _unitOfWork.User.UpdateByUserIdAsync(user);

            if (result > 0)
            {
                return new ServiceResult()
                {
                    Data = result,
                    Success = true,
                    StatusCode = System.Net.HttpStatusCode.OK,
                    DevMsg = "Change password success"
                };
            }
            throw new InternalServerException("Internal error. Can't change password");
        }

        public async Task<ServiceResult> ApprovingRoleServiceAsync(UserStatusUpdateDTO userStatusUpdateDTO)
        {
            var result = await _unitOfWork.User.ApproveRole(userStatusUpdateDTO);
            return new ServiceResult
            {
                StatusCode = result ? System.Net.HttpStatusCode.OK : System.Net.HttpStatusCode.BadRequest,
                Success = result
            };
        }

        public async Task<ServiceResult> ApprovingRolesServiceAsync(List<UserStatusUpdateDTO> userStatusUpdateDTOs)
        {
            var result = await _unitOfWork.User.ApproveRoles(userStatusUpdateDTOs);
            return new ServiceResult
            {
                StatusCode = result ? System.Net.HttpStatusCode.OK : System.Net.HttpStatusCode.BadRequest,
                Success = result,
                Data = userStatusUpdateDTOs
            };
        }

        public async Task<ServiceResult> CreateNewPrivacySettingsServiceAsync(PrivacySettingsSaveDTO privacySettingsSaveDTO)
        {
            UserModelView existedAlumni = await _unitOfWork.Alumni.GetAlumniByUserIdAsync(privacySettingsSaveDTO.UserId);
            if (existedAlumni == null)
            {
                return new ServiceResult()
                {
                    Data = existedAlumni,
                    Success = false,
                    StatusCode = System.Net.HttpStatusCode.BadRequest,
                    DevMsg = "Not found alumni for create new privacry setting experience"
                };
            }

            PrivacySettings newPrivacySettings = _mapper.Map<PrivacySettings>(privacySettingsSaveDTO);
            var existedPrivacySetting = existedAlumni.PrivacySetting ?? new PrivacySettings();

            existedPrivacySetting = newPrivacySettings;
            string updatedPrivacySettingsJson = JsonConvert.SerializeObject(existedPrivacySetting);

            var insertResult = await _unitOfWork.Alumni.UpdatePrivacySettingsByUserIdAsync(updatedPrivacySettingsJson, existedAlumni.UserId);
            if (insertResult > 0)
            {
                return new ServiceResult()
                {
                    Data = insertResult,
                    Success = true,
                    StatusCode = System.Net.HttpStatusCode.OK,
                    DevMsg = "Inserted new Education experience"
                };
            }

            throw new InternalServerException("Internal error. Can't insert new Education experience");
        }

        public async Task<ServiceResult> UpdateWorkExperienceServiceAsync(UpdateWorkExperienceDTO updateWorkExperienceDTO)
        {
            // Sử dụng AutoMapper để chuyển đổi danh sách WorkExperienceSaveDTO thành danh sách WorkExperience
            List<WorkExperience> workExperiences = updateWorkExperienceDTO.WorkExperiences ?? new List<WorkExperience>();

            // Chuyển đổi danh sách WorkExperience thành chuỗi JSON
            string updatedWorkExperienceJson = JsonConvert.SerializeObject(workExperiences);

            // Cập nhật chuỗi JSON trong cơ sở dữ liệu
            var updateResult = await _unitOfWork.Alumni.UpdateWorkExperienceByUserIdAsync(updatedWorkExperienceJson, updateWorkExperienceDTO.UserId);

            // Trả về kết quả dịch vụ
            if (updateResult > 0)
            {
                return new ServiceResult()
                {
                    Data = updateResult,
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    DevMsg = "Updated Work Experience successfully"
                };
            }

            throw new InternalServerException("Internal error. Can't update work experience");
        }

        public async Task<ServiceResult> UpdateEducationExperienceServiceAsync(UpdateEducationExperienceDTO updateEducationExperienceDTO)
        {

            List<EducationExperience> educationExperiences = updateEducationExperienceDTO.EducationExperiences ?? new List<EducationExperience>();

            // Chuyển đổi danh sách WorkExperience thành chuỗi JSON
            string updatedWorkExperienceJson = JsonConvert.SerializeObject(educationExperiences);

            // Cập nhật chuỗi JSON trong cơ sở dữ liệu
            var updateResult = await _unitOfWork.Alumni.UpdateEducationExperienceByUserIdAsync(updatedWorkExperienceJson, updateEducationExperienceDTO.UserId);

            // Trả về kết quả dịch vụ
            if (updateResult > 0)
            {
                return new ServiceResult()
                {
                    Data = updateResult,
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    DevMsg = "Updated Education successfully"
                };
            }

            throw new InternalServerException("Internal error. Can't update Education experience");
        }

        public async Task<ServiceResult> DeleteUserServiceAsync(Guid userId)
        {
            var res = await _unitOfWork.User.DeleteUserAsync(userId);
            if (res)
            {
                return new ServiceResult()
                {
                    Data = res,
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    UserMsg = "Deleted user"
                };
            }

            return new ServiceResult()
            {
                Data = res,
                Success = false,
                StatusCode = HttpStatusCode.BadRequest,
                UserMsg = "Can't delete user."
            };
        }

        public async Task<ServiceResult> GetLoggedUser(string? accessToken)
        {

            var result = new ServiceResult()
            {
                StatusCode = HttpStatusCode.NoContent,
                Success = false
            };

            if (string.IsNullOrEmpty(accessToken))
            {
                result.StatusCode = HttpStatusCode.BadRequest;
                result.Success = false;
                result.DevMsg = "Invalid access token";
            }

            if (!_tokenService.IsTokenValid(accessToken!))
            {
                return new ServiceResult()
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Success = false,
                    Data = string.Empty,
                    UserMsg = "Access Token is expires"
                };
            }

            var principal = await _tokenService.GetPrincipalFromExpiredTokenAsync(accessToken!);
            if (principal == null)
            {
                result.StatusCode = HttpStatusCode.Unauthorized;
                result.Success = false;
                result.DevMsg = "Invalid or expired access token";
            }

            Guid userId = new Guid(principal!.FindFirst("Id")!.Value);
            string status = principal.FindFirst("Status")!.Value;
            string role = principal.FindFirst(ClaimTypes.Role)!.Value;
            string email = principal.FindFirst(ClaimTypes.Email)!.Value;
            switch (role)
            {
                case "Alumni":
                    var alumni = await _unitOfWork.Alumni.GetAlumniByUserIdAsync(userId);
                    result.StatusCode = HttpStatusCode.OK;
                    result.Success = true;
                    if (alumni == null)
                    {
                        result.Data = string.Empty;
                    }
                    else
                    {
                        result.Data = new
                        {
                            UserId = userId,
                            Status = status,
                            FullName = alumni!.FullName ,
                            Avatar = alumni.Avartar 
                        };
                    }

                    break;
                case "Staff":
                    var staff = await _unitOfWork.Staff.GetStaffByUserIdAsync(userId);
                    result.StatusCode = HttpStatusCode.OK;
                    result.Success = true;
                    if (staff == null)
                    {
                        result.Data = string.Empty;
                    }
                    else
                    {
                        result.Data = new
                        {
                            UserId = userId,
                            Status = status,
                            FullName = staff.FullName,
                            Avatar = staff.Avartar
                        };
                    }
                    break;
                case "Admin":
                    var admin = new UserModelView();
                    admin.UserId = userId;
                    admin.Status = status;
                    admin.RoleName = role;
                    admin.MainEmail = email;
                    admin.Avartar = "admin.png";
                    admin.FullName = "Admin";
                    result.StatusCode = HttpStatusCode.OK;
                    result.Success = true;
                    result.Data = admin;
                    break;
                default:
                    result.StatusCode = HttpStatusCode.Forbidden;
                    result.Success = false;
                    result.DevMsg = "Unauthorized role";
                    break;
            }

            return result;
        }
    }
}
