﻿using AutoMapper;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Exceptions;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Core.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace FPT_Alumni.Core.Services
{
    public class FieldService : BaseService<Field>, IFieldService
    {
        readonly IUnitOfWork _unitOfWork;
        readonly IFieldRepository _fieldRepository;
        readonly IMapper _mapper;
        readonly ICommonService _commonService;
        public FieldService(IUnitOfWork unitOfWork, IFieldRepository fieldRepository,
            IMapper mapper, ICommonService commonService) : base(fieldRepository)
        {
            _unitOfWork = unitOfWork;
            _fieldRepository = fieldRepository;
            _mapper = mapper;
            _commonService = commonService;
        }

        public async Task<ServiceResult> GetFieldListServiceAsync()
        {
            var res = await _unitOfWork.Field.GetAllFieldAsync();
            var response = new ServiceResult()
            {
                StatusCode = res != null && res.Count > 0 ? System.Net.HttpStatusCode.OK : System.Net.HttpStatusCode.NotFound,
                DevMsg = res != null && res.Count > 0 ? "Data fetched successfully" : "No data found",
                UserMsg = res != null && res.Count > 0 ? "Data fetched successfully" : "No data found",
                Data = res,
                Success = res != null && res.Count > 0
            };

            return response;
        }

        public async Task<ServiceResult> AddFieldServiceAsync(FieldSaveDTO fieldSave)
        {
            fieldSave.Id = Guid.NewGuid();
            var existedField = await _unitOfWork.Field.SearchFieldByNameAsync(fieldSave.FieldName);
            if (existedField != null)
            {
                return new ServiceResult
                {
                    StatusCode = System.Net.HttpStatusCode.BadRequest,
                    DevMsg = "Field with name " + fieldSave.FieldName + " is existed",
                    Success = false,
                    Data = existedField,

                };
            }
            Field field = _mapper.Map<Field>(fieldSave);
            var res = await _unitOfWork.Field.InsertAsync(field);
            return new ServiceResult
            {
                StatusCode = System.Net.HttpStatusCode.OK,
                DevMsg = "Insert new field success",
                Success = true,
                Data = res
            };
        }

        public async Task<ServiceResult> UpdateFieldServiceAsync(FieldSaveDTO fieldSave)
        {
            var existedField = await _unitOfWork.Field.SearchFieldByNameAsync(fieldSave.FieldName);
            if (existedField != null)
            {
                return new ServiceResult
                {
                    StatusCode = System.Net.HttpStatusCode.BadRequest,
                    DevMsg = "Field with name " + fieldSave.FieldName + " is existed",
                    Success = false,
                    Data = existedField,

                };
            }
            Field field = _mapper.Map<Field>(fieldSave);
            var res = await _unitOfWork.Field.UpdateAsync(field);
            return new ServiceResult
            {
                StatusCode = System.Net.HttpStatusCode.OK,
                DevMsg = "Update field success",
                Success = true,
                Data = res
            };
        }

        public async Task<ServiceResult> SearchServiceAsync(string key)
        {
            if (string.IsNullOrEmpty(key.Trim()))
            {
                throw new BadRequestException("Từ khóa tìm kiếm không được để trống");
            }
            string validKey = _commonService.RemoveTextSpace(key);
            //string[] validKeyArray = validKey.Split(new char[] { ' ' });
            return new ServiceResult()
            {
                Success = true,
                Data = await _unitOfWork.Field.SearchFieldByNameAsync(validKey),
                StatusCode = System.Net.HttpStatusCode.OK,
                UserMsg = "Lấy thành công"
            };
        }

        public async Task<ServiceResult> GetAllServiceAsync()
        {
            return new ServiceResult()
            {
                Success = true,
                Data = await _unitOfWork.Field.GetAllFieldAsync(),
                StatusCode = System.Net.HttpStatusCode.OK,
                UserMsg = "Lấy thành công"
            };
        }
    }
}
