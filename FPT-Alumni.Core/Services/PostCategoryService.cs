﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Enums;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Core.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Services
{
    public class PostCategoryService : BaseService<PostCategory>, IPostCategoryService
    {
        IUnitOfWork _unitOfWork;
        ICommonService _commonService;
        public PostCategoryService(IBaseRepository<PostCategory> baseRepository, IUnitOfWork unitOfWork, ICommonService commService) : base(baseRepository)
        {
            _unitOfWork = unitOfWork;
            _commonService = commService;
        }

        public async Task<ServiceResult> GetAllBaseRoleServiceAsync(string accessToken)
        {
            UserRoleDTO userRole = await _commonService.GetUserIdAndRoleByAccesTokenAsync(accessToken);
            var result = new object();
            switch (userRole.Role)
            {
                case "Alumni":
                    result = await _unitOfWork.PostCategory.GetAllBaseViewModeAsync(ViewModeEnum.ALUMNI_VIEW);
                    break;
                case "Staff":
                    result = await _unitOfWork.PostCategory.GetAllBaseViewModeAsync(ViewModeEnum.STAFF_VIEW);
                    break;
            }
            return new ServiceResult()
            {
                Success = true,
                Data = result,
                StatusCode = System.Net.HttpStatusCode.OK,
                UserMsg = "Lấy thành công"
            };
        }
    }
}
