﻿using AutoMapper;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Core.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Services
{
    public class ApplicationService : BaseService<Application>, IApplicationService
    {
        ICommonService _commonService;
        IUnitOfWork _unitOfWork;
        readonly IMapper _mapper;
        public ApplicationService(IBaseRepository<Application> baseRepository, IMapper mapper, ICommonService commonService, IUnitOfWork unitOfWork) : base(baseRepository)
        {
            _mapper = mapper;
            _commonService = commonService;
            _unitOfWork = unitOfWork;
        }

        public async Task<ServiceResult> AddNewApplicationAsync(Application model, string accessToken)
        {
            Guid userId = await _commonService.GetUserIdByAccesTokenAsync(accessToken);

            model.CandidateId = userId;
            await InsertServiceAsync(model);

            return new ServiceResult
            {
                Success = true,
                Data = model,
                StatusCode = System.Net.HttpStatusCode.Created,
                UserMsg = "Create job successfully"
            };
        }

        public async Task<ServiceResult> GetAllCandidateByJobId(Guid jobId, int currentPage, int pageSize)
        {
            List<Application> applications = await _unitOfWork.Application.GetAllCandidateByJobId(jobId, currentPage, pageSize);
            return new ServiceResult()
            {
                Success = true,
                StatusCode = System.Net.HttpStatusCode.OK,
                Data = applications,
                UserMsg = "Lấy thành công"
            };
        }
    }
}
