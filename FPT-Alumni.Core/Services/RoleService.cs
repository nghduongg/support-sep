﻿using FPT_Alumni.Core.Constants;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Core.Interfaces.Services;


namespace FPT_Alumni.Core.Services
{
    public class RoleService : BaseService<Role>, IRoleService
    {
        readonly IRoleRepository _roleRepository;
        readonly IUnitOfWork _unitOfWork;

        public RoleService(IRoleRepository repository, IUnitOfWork unitOfWork) : base(repository)
        {
            _roleRepository = repository;
            _unitOfWork = unitOfWork;
        }

        public async Task<ServiceResult> GetAllAsync()
        {
            var res = await _unitOfWork.Role.GetAllAsync();
            var response = new ServiceResult()
            {
                StatusCode = res != null && res.Count > 0 ? System.Net.HttpStatusCode.OK : System.Net.HttpStatusCode.NotFound,
                DevMsg = res != null && res.Count > 0 ? "Data fetched successfully" : "No data found",
                UserMsg = res != null && res.Count > 0 ? "Data fetched successfully" : "No data found",
                Data = res,
                Success = res != null && res.Count > 0
            };

            return response;
        }

        public string GetRoleNameById(Guid roleId)
        {
            if (roleId == RoleConstants.ADMIN)
                return "Admin";
            if (roleId == RoleConstants.STAFF)
                return "Staff";
            if (roleId == RoleConstants.ALUMNI)
                return "Alumni";

            throw new ArgumentException("Invalid role ID");
        }
    }
}
