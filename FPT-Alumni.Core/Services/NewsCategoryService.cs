﻿using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Core.Interfaces.Services;


namespace FPT_Alumni.Core.Services
{
    public class NewsCategoryService : BaseService<NewsCategory>, INewsCategoryService
    {

        ITagRepository _tagRepository;
        IUnitOfWork _unitOfWork;
        public NewsCategoryService(IBaseRepository<NewsCategory> baseRepository, IUnitOfWork unitOfWork, ITagRepository tagRepository) : base(baseRepository)
        {
            _unitOfWork = unitOfWork;
            _tagRepository = tagRepository;
        }
    }
}
