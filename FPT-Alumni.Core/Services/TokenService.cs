﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;


namespace FPT_Alumni.Core.Services
{
    public class TokenService : ITokenService
    {
        readonly IConfiguration _configuration;
        readonly IUnitOfWork _unitOfWork;
        readonly IHttpContextAccessor _httpContextAccessor;
        readonly IRoleService _roleService;


        public TokenService(IConfiguration configuration, IUnitOfWork unitOfWork,
            IHttpContextAccessor httpContextAccessor, IRoleService roleService)
        {
            _configuration = configuration;
            _unitOfWork = unitOfWork;
            _httpContextAccessor = httpContextAccessor;
            _roleService = roleService;
        }
        HttpContext HttpContext => _httpContextAccessor.HttpContext!;

        public string GenerateAccessToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var roleName = _roleService.GetRoleNameById((Guid)user.RoleId!);

            var authClaims = new List<Claim>
            {
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(ClaimTypes.Role, roleName),
                     new Claim(ClaimTypes.Email, user.EmailLogged!),
                     new Claim("Id", user.Id.ToString()),
                      new Claim("Status", user.Status !.ToString())
            };


            var authSigningKey = Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]);

            _ = int.TryParse(_configuration["JWT:TokenValidityInMinutes"], out int tokenValidityInMinutes);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = _configuration["JWT:Issuer"],
                Audience = _configuration["JWT:Audience"],
                Subject = new ClaimsIdentity(authClaims),
                Expires = DateTime.UtcNow.AddMinutes(tokenValidityInMinutes),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(authSigningKey), SecurityAlgorithms.HmacSha256Signature)
            };


            var token = tokenHandler.CreateToken(tokenDescriptor);
            var encrypterToken = tokenHandler.WriteToken(token);

            SetCookies("Access-Token", encrypterToken, DateTime.UtcNow.AddMinutes(10));

            return encrypterToken;
        }

        public async Task<TokenResultDTO> JWTGeneratorAsync(User user)
        {

            var encrypterToken = GenerateAccessToken(user);

            var newRefreshToken = await UpdateUserRefreshTokenAsync(user);

            return new TokenResultDTO
            {
                AccessToken = encrypterToken,
                RefreshToken = new RefreshTokenModel
                {
                    Token = newRefreshToken.RefreshToken!,
                    Expires = newRefreshToken.ExperiedDateToken
                }
            };
        }

        public async Task<User> UpdateUserRefreshTokenAsync(User user)
        {
            var newRefreshToken = GenerateRefreshToken();
            user.RefreshToken = newRefreshToken.Token.ToString();
            user.ExperiedDateToken = newRefreshToken.Expires;
            await _unitOfWork.User.UpdateByUserIdAsync(user);
            SetCookies("Refresh-Token", newRefreshToken.Token, (DateTime)newRefreshToken.Expires!);

            return user;
        }

        public RefreshTokenModel GenerateRefreshToken()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();
            var token = new string(Enumerable.Repeat(chars, 64)
                .Select(s => s[random.Next(s.Length)]).ToArray());

            var rt = new RefreshTokenModel
            {
                Token = token,
                Expires = DateTime.UtcNow.AddMinutes(int.Parse(_configuration["JWT:RefreshTokenValidityInMinutes"].ToString())),
            };
            return rt;
        }

        public void SetCookies(string? variable, string? value, DateTime expires)
        {
            HttpContext.Response.Cookies.Append(variable!, value!,
                new CookieOptions
                {
                    Expires = expires,
                    HttpOnly = true,
                    Secure = true,
                    IsEssential = true,
                    SameSite = SameSiteMode.None
                });
        }

        public async Task<ServiceResult> RefreshServiceAsync(string? refreshToken, string? action)
        {
            refreshToken = WebUtility.UrlEncode(refreshToken);
            var user = await _unitOfWork.User.GetUserByTokenAsync(refreshToken);
            if (user != null)
            {
                if (action!.Equals("logout", StringComparison.Ordinal))
                {
                    user.RefreshToken = "";
                    user.ExperiedDateToken = DateTime.UtcNow.AddDays(-10);

                    // Đặt lại IsLogged của tất cả các email của người dùng thành false
                    user.EmailLogged = string.Empty;
                    await _unitOfWork.User.UpdateAsync(user);

                    SetCookies("Access-Token", "", DateTime.UtcNow.AddDays(-10));
                    SetCookies("Refresh-Token", "", DateTime.UtcNow.AddDays(-10));

                    return new ServiceResult
                    {
                        StatusCode = System.Net.HttpStatusCode.OK,
                        DevMsg = "Logout successfully",
                        UserMsg = "Logout successfully"
                    };
                }

                var newAccessToken = GenerateAccessToken(user);

                var newRefreshToken = await UpdateUserRefreshTokenAsync(user);

                var data = new TokenResultDTO
                {
                    AccessToken = newAccessToken,
                    RefreshToken = new RefreshTokenModel
                    {
                        Token = newRefreshToken.RefreshToken!,
                        Expires = newRefreshToken.ExperiedDateToken
                    }
                };

                return new ServiceResult
                {
                    StatusCode = System.Net.HttpStatusCode.OK,
                    Data = data
                };
            }
            return new ServiceResult
            {
                StatusCode = System.Net.HttpStatusCode.NotFound,
                DevMsg = "There is no user with this token"
            };
        }

        public Task<ClaimsPrincipal> GetPrincipalFromExpiredTokenAsync(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = _configuration["JWT:Issuer"],
                ValidateAudience = true,
                ValidAudience = _configuration["JWT:Audience"],
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]))
            };

            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters,
                out SecurityToken securityToken);

            if (securityToken is not JwtSecurityToken jwtSecurityToken || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");
            return Task.FromResult(principal);
        }

        public string GenerateChangePasswordToken(Guid userId)
        {
            var authClaims = new List<Claim>
            {
                     new Claim("Id", userId.ToString()),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            var authSigningKey = Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]);

            var tokenHandler = new JwtSecurityTokenHandler();

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = _configuration["JWT:Issuer"],
                Audience = _configuration["JWT:Audience"],
                Subject = new ClaimsIdentity(authClaims),
                Expires = DateTime.UtcNow.AddMinutes(10),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(authSigningKey), SecurityAlgorithms.HmacSha256Signature)
            };


            var token = tokenHandler.CreateToken(tokenDescriptor);

            var encrypterToken = tokenHandler.WriteToken(token);

            return encrypterToken;
        }

        public bool IsTokenValid(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = _configuration["JWT:Issuer"],
                ValidateAudience = true,
                ValidAudience = _configuration["JWT:Audience"],
                ValidateLifetime = false, // Đặt ValidateLifetime thành false để kiểm tra thủ công
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]))
            };

            try
            {
                var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken securityToken);

                // Kiểm tra xem token có phải là JWT token hay không và thuật toán ký có chính xác không
                if (securityToken is not JwtSecurityToken jwtSecurityToken ||
                    !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                    throw new SecurityTokenExpiredException();

                // Kiểm tra thời gian hết hạn của token
                var expClaim = jwtSecurityToken.Claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Exp);
                if (expClaim != null)
                {
                    var expTimestamp = long.Parse(expClaim.Value);
                    var expDateTime = DateTimeOffset.FromUnixTimeSeconds(expTimestamp).UtcDateTime;
                    if (expDateTime < DateTime.UtcNow)
                        throw new SecurityTokenExpiredException();
                }

                return true;
            }
            catch (SecurityTokenExpiredException)
            {
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
