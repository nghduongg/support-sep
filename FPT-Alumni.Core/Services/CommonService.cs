﻿using FPT_Alumni.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using FPT_Alumni.Core.Interfaces.Services;
using FPT_Alumni.Core.Exceptions;
using System.Security.Claims;
using System.Text.RegularExpressions;
using FPT_Alumni.Core.Entities;
using Microsoft.AspNetCore.Http;

namespace FPT_Alumni.Core.Services
{
    public class CommonService : ICommonService
    {
        private const string _chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+";
        ITokenService _tokenService;
        private readonly IHttpContextAccessor _httpContextAccessor;


        public CommonService(ITokenService tokenService, IHttpContextAccessor httpContextAccessor)
        {
            _tokenService = tokenService;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<bool> SendMailAsync(MailDTO mail)
        {
            MailMessage message = new MailMessage(
                from: Credential.Email,
                to: mail.To,
                subject: mail.Subject,
                body: mail.Body
            );
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.SubjectEncoding = System.Text.Encoding.UTF8;
            message.IsBodyHtml = true;
            message.ReplyToList.Add(new MailAddress(Credential.Email));
            message.Sender = new MailAddress(Credential.Email);

            // Tạo SmtpClient kết nối đến smtp.gmail.com
            using (SmtpClient client = new SmtpClient("smtp.gmail.com"))
            {
                client.Port = 587;
                client.Credentials = new NetworkCredential(Credential.Email, Credential.Password);
                client.EnableSsl = true;
                try
                {
                    await client.SendMailAsync(message);
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return false;
                }
            }
        }

        public string GenerateRandomPassword(int length)
        {
            var random = new Random();
            return new string(Enumerable.Repeat(_chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public async Task<string> GetAccessTokenAsync()
        {
            var request = _httpContextAccessor.HttpContext?.Request;
            if (request == null)
            {
                throw new Exception("Request is null");
            }

            // Get the cookie header
            string cookieHeader = request.Headers["Cookie"].ToString();

            // Parse the cookie header to extract individual cookies
            var cookies = cookieHeader.Split(',')
                                      .Select(cookie => cookie.Split('='))
                                      .ToDictionary(cookie => cookie[0].Trim(), cookie => cookie.Length > 1 ? cookie[1].Trim() : string.Empty);

            cookies.TryGetValue("Access-Token", out var accessToken);

            return accessToken!;
        }

        public async Task<Guid> GetUserIdByAccesTokenAsync(string? accessToken)
        {
            if (accessToken == null)
            {
                throw new BadRequestException("Bạn không có quyền truy cập dung này");
            }
            ClaimsPrincipal claimsPrincipal = await _tokenService.GetPrincipalFromExpiredTokenAsync(accessToken);
            var idClaim = claimsPrincipal.FindFirst("Id");
            // user without account was viewed
            if (idClaim == null)
            {
                throw new BadRequestException("Bạn không có quyền truy cập nội dung này");
            }
            return Guid.Parse(idClaim.Value);
        }

        public string RemoveTextSpace(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                text = text.Trim();
                text = Regex.Replace(text, @"\s+", " ");
                return text;
            }
            return "";
        }

        public async Task<UserRoleDTO> GetUserIdAndRoleByAccesTokenAsync(string? accessToken)
        {
            if (accessToken == null)
            {
                throw new BadRequestException("Bạn không có quyền truy cập dung này");
            }
            ClaimsPrincipal claimsPrincipal = await _tokenService.GetPrincipalFromExpiredTokenAsync(accessToken);
            var idClaim = claimsPrincipal.FindFirst("Id");
            var roles = claimsPrincipal.FindAll("http://schemas.microsoft.com/ws/2008/06/identity/claims/role").ToList();
            var status = claimsPrincipal.FindFirst("Status")!.Value;
            // user without account was viewed
            if (idClaim == null || roles[0].Value == null || status != "ACTIVATED")
            {
                throw new BadRequestException("Bạn không có quyền truy cập nội dung này");
            }

            return new UserRoleDTO()
            {
                Id = Guid.Parse(idClaim.Value),
                Role = roles[0].Value,
            };
        }

        public string RemoveUnicode(string text)
        {
            string[] arr1 = new string[] { "á", "à", "ả", "ã", "ạ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ",
    "đ",
    "é","è","ẻ","ẽ","ẹ","ê","ế","ề","ể","ễ","ệ",
    "í","ì","ỉ","ĩ","ị",
    "ó","ò","ỏ","õ","ọ","ô","ố","ồ","ổ","ỗ","ộ","ơ","ớ","ờ","ở","ỡ","ợ",
    "ú","ù","ủ","ũ","ụ","ư","ứ","ừ","ử","ữ","ự",
    "ý","ỳ","ỷ","ỹ","ỵ",};
            string[] arr2 = new string[] { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
    "d",
    "e","e","e","e","e","e","e","e","e","e","e",
    "i","i","i","i","i",
    "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
    "u","u","u","u","u","u","u","u","u","u","u",
    "y","y","y","y","y",};
            for (int i = 0; i < arr1.Length; i++)
            {
                text = text.Replace(arr1[i], arr2[i]);
                text = text.Replace(arr1[i].ToUpper(), arr2[i].ToUpper());
            }
            return text;
        }
    }
}
