﻿using AutoMapper;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Core.Interfaces.Services;
using FPT_Alumni.Core.JsonModels;
using Newtonsoft.Json;


namespace FPT_Alumni.Core.Services
{
    public class MentoringService : BaseService<Mentoring>, IMentoringService
    {
        readonly IMentoringRepository _mentoringRepository;
        readonly IUnitOfWork _unitOfWork;
        readonly IMapper _mapper;
        readonly ICommonService _commonService;
        public MentoringService(IMentoringRepository mentoringRepository, IMapper mapper,
            IUnitOfWork unitOfWork, ICommonService commonService)
            : base(mentoringRepository)
        {
            _mentoringRepository = mentoringRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _commonService = commonService;
        }

        public async Task<ServiceResult> CreateMentoringServiceAsync(MentoringSaveDTO mentoringSave)
        {
            var mentoring = new Mentoring();
            int result = 0;
            string action = "insert";

            if (mentoringSave?.Id != null && mentoringSave.Id != Guid.Empty) // update
            {
                action = "update";
                mentoring.Id = mentoringSave.Id;
            }
            else
            {
                mentoring.Id = Guid.NewGuid();
            }

            string accessToken = await _commonService.GetAccessTokenAsync();
            if (accessToken == null || string.IsNullOrEmpty(accessToken))
            {
                return new ServiceResult
                {
                    Success = false,
                    StatusCode = System.Net.HttpStatusCode.BadRequest,
                    Data = string.Empty,
                    UserMsg = "Can't save mentoring. Can't access Token is empty"
                };
            }

            Guid userid = await _commonService.GetUserIdByAccesTokenAsync(accessToken);
            mentoring.UserId = userid;
            mentoring.CurrentCity = mentoringSave!.CurrentCity;
            mentoring.CurrentCountry = mentoringSave!.CurrentCountry;
            mentoring.BasicInformation = JsonConvert.SerializeObject(mentoringSave.BasicInformation);
            mentoring.WorkAndEducation = JsonConvert.SerializeObject(mentoringSave.WorkAndEducation);
            mentoring.OtherPreferences = mentoringSave.OtherPreferences;
            mentoring.MentoringRole = "mentor";

            result = await _unitOfWork.Mentoring.SaveMentoringAsync(mentoring, action);

            List<Guid> mentoringFields = mentoringSave.MentoringFields!;
            if (mentoringFields != null && mentoringFields.Count > 0)
            {
                result += await _unitOfWork.Mentoring.SaveMentoringFieldsAsync(mentoring.Id, mentoringFields);
            }

            List<Guid> mentoringTypes = mentoringSave.ConnectionAndWorkings!;
            if (mentoringTypes != null && mentoringTypes.Count > 0)
            {
                result += await _unitOfWork.Mentoring.SaveMentoringTypesAsync(mentoring.Id, mentoringTypes);
            }

            List<Guid> mentoringLanguages = mentoringSave.Languages!;
            if (mentoringLanguages != null && mentoringLanguages.Count > 0)
            {
                result += await _unitOfWork.Mentoring.SaveMentoringLanguagesAsync(mentoring.Id, mentoringLanguages);
            }

            if (result >= 1)
            {
                return new ServiceResult
                {
                    Success = true,
                    StatusCode = System.Net.HttpStatusCode.Created,
                    Data = mentoringSave,
                    UserMsg = "Saved mentoring"
                };
            }

            return new ServiceResult
            {
                Success = false,
                StatusCode = System.Net.HttpStatusCode.InternalServerError,
                Data = mentoringSave,
                UserMsg = "Can't save mentoring"
            };
        }

        public async Task<ServiceResult> GetMentoringByIdServiceAsync(Guid id)
        {
            var mentoring = await _unitOfWork.Mentoring.GetMentoringByIdAsync(id);
            if (mentoring == null)
            {
                return new ServiceResult
                {
                    Success = true,
                    StatusCode = System.Net.HttpStatusCode.OK,
                    UserMsg = "Not found mentor"
                };
            }
            var mentoringDto = MapToMentoringModelView(mentoring);
            var fieldsOfMentoring = await _unitOfWork.Field.GetFieldsByMentoringId(mentoringDto.Id);
            var types = await _unitOfWork.Mentoring.GetTypesByMentoringId(mentoringDto.Id);
            var languages = await _unitOfWork.Mentoring.GetLanguagesByMentoringId(mentoringDto.Id);

            mentoringDto.MentoringFields = fieldsOfMentoring;
            mentoringDto.ConnectionAndWorkings = types;
            mentoringDto.LanguageSkills = languages;

            return new ServiceResult
            {
                Success = true,
                StatusCode = System.Net.HttpStatusCode.OK,
                Data = mentoringDto
            };
        }

        private MentoringModelView MapToMentoringModelView(Mentoring mentoring)
        {
            return new MentoringModelView
            {
                Id = mentoring.Id,
                UserId = mentoring.UserId,
                CurrentCountry = mentoring.CurrentCountry,
                CurrentCity = mentoring.CurrentCity,
                BasicInformation = !string.IsNullOrEmpty(mentoring.BasicInformation) ? JsonConvert.DeserializeObject<BasicInformation>(mentoring.BasicInformation) : null,
                WorkAndEducation = !string.IsNullOrEmpty(mentoring.WorkAndEducation) ? JsonConvert.DeserializeObject<WorkAndEducation>(mentoring.WorkAndEducation) : null,
                OtherPreferences = mentoring.OtherPreferences,
                MentoringRole = mentoring.MentoringRole
            };
        }

        public async Task<ServiceResult> DeleteMentoringServiceAsync(Guid id)
        {
            var result = await _unitOfWork.Mentoring.DeleteMentoringFieldsByMentoringId(id);
            result += await _unitOfWork.Mentoring.DeleteMentoringLanguagesByMentoringId(id);
            result += await _unitOfWork.Mentoring.DeleteMentoringConnectionAndWorkingByMentoringId(id);
            result += await _unitOfWork.Mentoring.DeleteMentoringByMentoringId(id);
            if (result >= 1)
            {
                return new ServiceResult
                {
                    Success = true,
                    StatusCode = System.Net.HttpStatusCode.OK,
                    Data = id,
                    UserMsg = "Deleted Mentoring"
                };
            }
            return new ServiceResult
            {
                Success = false,
                StatusCode = System.Net.HttpStatusCode.InternalServerError,
                Data = id,
                UserMsg = "Delete Mentoring Failed"
            };
        }

        public async Task<ServiceResult> FilterMentorAsync(MentoringFilterDTO mentoringFilterDTO)
        {
            var mentors = await _unitOfWork.Mentoring.FilterMentoringsAsync(mentoringFilterDTO, "mentor");
            List<MentoringModelView> mentorings = new List<MentoringModelView>();
            foreach (var mentor in mentors)
            {
                var mentoringProfile = MapToMentoringModelView(mentor);
                mentoringProfile.LanguageSkills = await _unitOfWork.Mentoring.GetLanguagesByMentoringId(mentor.Id);
                mentoringProfile.MentoringFields = await _unitOfWork.Field.GetFieldsByMentoringId(mentor.Id);
                mentoringProfile.ConnectionAndWorkings = await _unitOfWork.Mentoring.GetTypesByMentoringId(mentor.Id);
                mentorings.Add(mentoringProfile);
            }
            List<Language> languages = await _unitOfWork.Mentoring.GetLanguageListAsync();
            List<ConnectionAndWorking> connectionAndWorkings = await _unitOfWork.Mentoring.GetConnectionAndWorkingsAsync();
            return new ServiceResult
            {
                Success = true,
                StatusCode = System.Net.HttpStatusCode.OK,
                Data =new {
                    Fields = mentoringFilterDTO.FieldIds,
                    Mentorings = mentorings,
                    Languages = languages,
                    ConnectionAndWorkings = connectionAndWorkings
                }
            };
        }

        public async Task<ServiceResult> CreateMentoringServiceAsync(MentoringSearchDTO mentoringSave)
        {
            var mentoring = new Mentoring();
            int result = 0;
            string action = "insert";

            if (mentoringSave?.Id != null && mentoringSave.Id != Guid.Empty) // update
            {
                action = "update";
                mentoring.Id = mentoringSave.Id;
            }
            else
            {
                mentoring.Id = Guid.NewGuid();
            }

            string accessToken = await _commonService.GetAccessTokenAsync();
            if (accessToken == null || string.IsNullOrEmpty(accessToken))
            {
                return new ServiceResult
                {
                    Success = false,
                    StatusCode = System.Net.HttpStatusCode.BadRequest,
                    Data = string.Empty,
                    UserMsg = "Can't save mentoring. Can't access Token is empty"
                };
            }
            Guid userId = await _commonService.GetUserIdByAccesTokenAsync(accessToken);
            mentoring.UserId = userId;
            mentoring.BasicInformation = JsonConvert.SerializeObject(mentoringSave!.BasicInformation);
            mentoring.WorkAndEducation = JsonConvert.SerializeObject(mentoringSave.WorkAndEducation);
            mentoring.OtherPreferences = mentoringSave.OtherPreferences;
            mentoring.MentoringRole = "mentee";

            result = await _unitOfWork.Mentoring.SaveMentoringAsync(mentoring, action);

            List<Guid> mentoringFields = mentoringSave.MentoringFields!;
            if (mentoringFields != null && mentoringFields.Count > 0)
            {
                result += await _unitOfWork.Mentoring.SaveMentoringFieldsAsync(mentoring.Id, mentoringFields);
            }

            List<Guid> mentoringTypes = mentoringSave.ConnectionAndWorkings!;
            if (mentoringTypes != null && mentoringTypes.Count > 0)
            {
                result += await _unitOfWork.Mentoring.SaveMentoringTypesAsync(mentoring.Id, mentoringTypes);
            }


            if (result >= 1)
            {
                return new ServiceResult
                {
                    Success = true,
                    StatusCode = System.Net.HttpStatusCode.Created,
                    Data = mentoringSave,
                    UserMsg = "Saved mentor searching"
                };
            }

            return new ServiceResult
            {
                Success = false,
                StatusCode = System.Net.HttpStatusCode.InternalServerError,
                Data = result,
                UserMsg = "Can't save mentor searching"
            };
        }

        public async Task<ServiceResult> GetMentoringFormServiceAsync(string mentoringRole)
        {
            string accessToken = await _commonService.GetAccessTokenAsync();
            if (accessToken == null || string.IsNullOrEmpty(accessToken))
            {
                return new ServiceResult
                {
                    Success = false,
                    StatusCode = System.Net.HttpStatusCode.BadRequest,
                    Data = string.Empty,
                    UserMsg = "Can't open form.Token is expired"
                };
            }

            Guid userid = await _commonService.GetUserIdByAccesTokenAsync(accessToken);

            var mentoring = await _unitOfWork.Mentoring.GetMentoringByUserIdAndRoleAsync(userid, mentoringRole);
            var fields = await _unitOfWork.Field.GetAllFieldAsync();
            var connectionAndWorings = await _unitOfWork.Mentoring.GetConnectionAndWorkingsAsync();
            var languages = await _unitOfWork.Mentoring.GetLanguageListAsync();
            if (mentoring != null)
            {
                var mentoringProfile = MapToMentoringModelView(mentoring);
                mentoringProfile.LanguageSkills = await _unitOfWork.Mentoring.GetLanguagesByMentoringId(mentoring.Id);
                mentoringProfile.MentoringFields = await _unitOfWork.Field.GetFieldsByMentoringId(mentoring.Id);
                mentoringProfile.ConnectionAndWorkings = await _unitOfWork.Mentoring.GetTypesByMentoringId(mentoring.Id);
                return new ServiceResult
                {
                    Success = true,
                    StatusCode = System.Net.HttpStatusCode.OK,
                    Data = new
                    {
                        Action = "update",
                        ForRole = mentoringRole,
                        Mentoring = mentoringProfile,
                        FieldList = fields,
                        ConnectionAndWoringList = connectionAndWorings,
                        LanguageList = languages,
                    },
                };
            }
            return new ServiceResult
            {
                Success = true,
                StatusCode = System.Net.HttpStatusCode.OK,
                Data = new
                {
                    Action = "insert",
                    ForRole = mentoringRole,
                    FieldList = fields,
                    ConnectionAndWoringList = connectionAndWorings,
                    LanguageList = languages
                },
            };
        }

        public async Task<ServiceResult> SaveMentoringReportServiceAsync(MentoringReportSaveDTO filter)
        {
            var report = _mapper.Map<MentoringReport>(filter);
            report.Id = Guid.NewGuid();
            report.CreatedAt = DateTime.UtcNow;

            var result = await _unitOfWork.Mentoring.SaveMentoringReportAsync(report);
            if(result > 0)
            {
                return new ServiceResult
                {
                    Success = true,
                    StatusCode = System.Net.HttpStatusCode.OK,
                    Data = report,
                    UserMsg = "Report Mentoring Success"
                };
            }
            return new ServiceResult
            {
                Success = false,
                StatusCode = System.Net.HttpStatusCode.Conflict,
                Data = report,
                UserMsg = "Report mentoring failed. Internal error"
            };
        }

        public async Task<ServiceResult> GetMentoringReportsServiceAsync()
        {
            var result = await _unitOfWork.Mentoring.GetReportListAsync();
            return new ServiceResult
            {
                Success = true,
                StatusCode = System.Net.HttpStatusCode.OK,
                Data = result,
            };
        }

        public async Task<ServiceResult> GetMentoringReportByIdServiceAsync(Guid id)
        {
            var report = await _unitOfWork.Mentoring.GetReportDetailsAsync(id);
            var mentoringReportView = _mapper.Map<MentoringReportDetailsModelView>(report);
            if (!string.IsNullOrEmpty(report.ReporterBasicInformation))
            {
                mentoringReportView.ReporterBasicInformationView = JsonConvert.DeserializeObject<BasicInformation>(report.ReporterBasicInformation);
            }
            if(!string.IsNullOrEmpty(report.VictimBasicInformation))
            {
                mentoringReportView.VictimBasicInformationView = JsonConvert.DeserializeObject<BasicInformation>(report.VictimBasicInformation);
            }
            return new ServiceResult
            {
                Success = true,
                StatusCode = System.Net.HttpStatusCode.OK,
                Data = mentoringReportView,
            };
        }
    }
}
