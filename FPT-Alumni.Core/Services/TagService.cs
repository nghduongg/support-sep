﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Exceptions;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Core.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Services
{
    public class TagService : BaseService<Tag>, ITagService
    {
        IUnitOfWork _unitOfWork;
        ITagRepository _tagRepository;
        ICommonService _commonService;
        public TagService(IBaseRepository<Tag> baseRepository, IUnitOfWork unitOfWork, ITagRepository tagRepository, ICommonService commonService) : base(baseRepository)
        {
            _unitOfWork = unitOfWork;
            _tagRepository = tagRepository;
            _commonService = commonService;
        }

        public async Task<ServiceResult> FindTagByKeyWordForTagingServiceAsync(string key)
        {
            List<Tag> tags = new();
            if (!String.IsNullOrEmpty(key.Trim()))
            {
                string validKey = _commonService.RemoveTextSpace(key);
                tags = await _unitOfWork.Tag.FindByTagNameForTagingAsync(validKey);
            }
            return new ServiceResult()
            {
                Success = true,
                StatusCode = System.Net.HttpStatusCode.OK,
                Data = tags,
                UserMsg = "Lấy thành công"
            };
        }

        public async Task<ServiceResult> GetAllServiceAsync()
        {
            return new ServiceResult()
            {
                Success = true,
                StatusCode = System.Net.HttpStatusCode.OK,
                Data = await _unitOfWork.Tag.GetAllAsync(),
                UserMsg = "Lấy thành công"
            };
        }
    }
}
