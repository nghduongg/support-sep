﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Core.Interfaces.Services;


namespace FPT_Alumni.Core.Services
{
    public class NewsService : BaseService<News>, INewsService
    {
        IUnitOfWork _unitOfWork;
        public NewsService(IBaseRepository<News> baseRepository, IUnitOfWork unitOfWork) : base(baseRepository)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<ServiceResult> FilterPagingServiceAsync(string key, int page, int size)
        {
            throw new NotImplementedException();
        }
    }
}
