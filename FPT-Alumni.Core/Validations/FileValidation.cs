﻿using FPT_Alumni.Core.Exceptions;
using FPT_Alumni.Core.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;

namespace FPT_Alumni.Core.Validations
{
    public class FileValidation : IFileValidation
    {

        public async Task ValidateFileAsync(IFormFile file, string expectedExtention, string[] expectedHeader)
        {
            if (file == null)
            {
                throw new BadRequestException("File can not be empty");
            }
            // Check file extension
            string fileName = file.FileName;
            string fileNameExtention = Path.GetExtension(fileName);
            if (fileNameExtention != expectedExtention)
            {
                throw new BadRequestException($"File name extension must be {expectedExtention}");
            }
            var filePath = Path.Combine("UploadedFiles", file.FileName);
            // File Exist -> delete file and save new file
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            // save file into folder
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
            if (!File.Exists(filePath))
            {
                throw new Exception($"Upload failed, please try again");
            }

            // excel file -> check file structure by expectedHeader
            if (fileNameExtention == ".xlsx")
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

                using (var package = new ExcelPackage(new FileInfo(filePath)))
                {
                    var worksheet = package.Workbook.Worksheets[0];
                    // check headers title are match with Demo file or not
                    for (int i = 1; i <= expectedHeader.Length; i++)
                    {
                        // not match -> throw exption
                        if (worksheet.Cells[1, i].Value?.ToString()?.Trim().ToLower() != expectedHeader[i - 1].Trim().ToLower())
                        {
                            throw new BadRequestException("Structure not match");
                        }
                    }
                }
            }
        }
    }
}
