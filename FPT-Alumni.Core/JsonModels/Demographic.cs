﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.JsonModels
{
  
    public class Experiences
    {
        public string? Organisation { get; set; }
        public string? Position { get; set; }
        public string? StartDate { get; set; }
        public string? EndDate { get; set; }
        public string? PositionSummary { get; set; }
    }
    public class WorkAndEducation
    {
        public List<Experiences?>? Experiences { get; set; }
        public string? LinkedInUrl { get; set; }
        public string? CVUrl { get; set; }
        public int? SupervisoryExperience { get; set; }
    }

    public class BasicInformation
    {
        public string? Avatar {  get; set; }
        public string? FullName { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }
        public string? Gender { get; set; }
        public string? StudentCode {  get; set; }
        public string? Class { get; set; }
        public string? GeneralIntroduction {  get; set; }

    }
}
