﻿using FPT_Alumni.Core.DTOs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.JsonModels
{
    public class WorkExperience
    {
        public string? Company { get; set; }
        public string? Position { get; set; }
        public DateTime? StartDate { get; set; } = DateTime.MinValue;
        public DateTime? EndDate { get; set; } = DateTime.UtcNow;
        public string? Description { get; set; }
    }

    public class EducationExperience
    {
        public string? Institution { get; set; }
        public string? Major { get; set; }
        public DateTime StartDate { get; set; } = DateTime.MinValue;
        public DateTime EndDate { get; set; } = DateTime.UtcNow;
        public string? Description { get; set; }
    }

    public class PrivacySettings
    {
        public bool EmailPublic { get; set; } = true;
        public bool PhonePublic { get; set; } = true;
        public bool IsShowWorkExperiece { get; set; } = true;
        public bool IsShowEducationExperiece { get; set; } = true;
        public bool IsShowCompany { get; set; } = true;
        public bool IsShowPhoneNumber { get; set; } = true;
        public bool IsShowStudentCode { get; set; } = true;
        public bool IsShowClass { get; set; } = true;
        public bool IsShowMajor { get; set; } = true;
        public bool IsGraduated { get; set; } = true;
        public bool IsJob { get; set; } = true;
        public bool IsShowLinkedUrl { get; set; } = true;
        public bool IsShowDateOfBirth { get; set; } = true;
        public bool IsShowGraduationYear { get; set; } = true;
        public bool IsShowCountry { get; set; } = true;
        public bool IsShowCity { get; set; } = true;

        public PrivacySettings()
        {
            EmailPublic = true;
            PhonePublic = true;
            IsShowWorkExperiece = true;
            IsShowEducationExperiece = true;
            IsShowCompany = true;
            IsShowPhoneNumber = true;
            IsShowStudentCode = true;
            IsShowClass = true;
            IsShowMajor = true;
            IsGraduated = true;
            IsJob = true;
            IsShowLinkedUrl = true;
            IsShowDateOfBirth = true;
            IsShowGraduationYear = true;
            IsShowCountry = true;
            IsShowCity = true;
        }
    }

}
