﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;


namespace FPT_Alumni.Core.Interfaces.Repositories
{
    public interface IUserRepository : IBaseRepository<User>
    {
        /// <summary>
        /// Find user by Email
        /// </summary>
        /// <param name="email"> email to find account</param>
        /// <returns>Account match with this email</returns>
        /// create by: Nguyễn Thiện Thắng
        /// create at: 05/17/2024
        public Task<UserReturnDTO?> GetByEmailAsync(string email);

        /// <summary>
        /// Finds a user by their refresh token.
        /// </summary>
        /// <param name="RefreshToken">The refresh token to search for.</param>
        /// <returns>A task representing the asynchronous operation, containing the user that matches the refresh token.</returns>
        /// create by: Nguyễn Đình Trường
        /// create at: 05/20/2024
        Task<User> GetUserByTokenAsync(string? RefreshToken);

        /// Get all User Information
        /// </summary>
        /// <param name=""></param>
        /// <returns>A List information with type User or null if not found</returns>
        ///  created by: Vũ Xuân Trường
        ///  created at: 05/12/2024
        public Task<List<User>> GetAllAsync();

        /// <summary>
        /// Updates a user's information by their user ID.
        /// </summary>
        /// <param name="updateUser">The user entity containing the updated information.</param>
        /// <returns>A task representing the asynchronous operation, containing the number of affected rows.</returns>
        /// create by: Nguyễn Đình Trường
        /// create at: 05/20/2024
        Task<int> UpdateByUserIdAsync(User updateUser);
        Task<bool> CheckPasswordAsync(string currentPassword, Guid Id);
        Task<bool> IsEmailTakenByAnotherUserAsync(string mainEmail, Guid? userId);
        Task<bool> ApproveRole(UserStatusUpdateDTO userStatusUpdateDTO);
        /// <summary>
        /// Get user' email by user id
        /// </summary>
        /// <param name="id">User id to find email</param>
        /// <returns>user's email</returns>
        /// create by: Nguyễn Thiện Thắng
        /// create at: 06/02/2024
        Task<string> GetEmailByIdAsync(Guid id);

        Task<bool> DeleteUserAsync(Guid userId);
        Task<User> GetUserByIdAsync(Guid userId);

        Task<bool> ApproveRoles(List<UserStatusUpdateDTO> userStatusUpdateDTOs);
    }
}
