﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Repositories;

namespace FPT_Alumni.Infrastructure.Repositories
{
    public interface IAlumniRepository : IBaseRepository<Alumni>
    {
        /// <summary>
        /// Get full information of alumni join user table in db 
        /// </summary>
        ///  <param name="user">userId is foreign key of alumni table</param>
        /// <returns>a model contains all information of alumni join user table in db  </returns>
        ///  created by: Nguyễn Đình Trường
        ///  created at: 05/28/2024
        Task<UserModelView> GetAlumniByUserIdAsync(Guid userId);

        /// <summary>
        /// Update Alumni by user id 
        /// </summary>
        ///  <param name="user">alumni contains userId and all new information of alumni to update</param>
        /// <returns>updated status  </returns>
        ///  created by: Nguyễn Đình Trường
        ///  created at: 05/28/2024
        Task<int> UpdateAlumniByUserIdAsync(Alumni alumni);

        /// <summary>
        /// Update WorkExperienceByUserId in alumni table 
        /// </summary>
        ///  <param name="user">newWorkExperience contains all new information of work experience to update; userId is foreign key in alumni table</param>
        /// <returns>updated status  </returns>
        ///  created by: Nguyễn Đình Trường
        ///  created at: 05/28/2024
        Task<int> UpdateWorkExperienceByUserIdAsync(string newWorkExperience, Guid userId);


        /// <summary>
        /// Update EducationExperienceByUserId in alumni table 
        /// </summary>
        ///  <param name="user">newEducationExperience contains all new information of education experience to update; userId is foreign key in alumni table</param>
        /// <returns>updated status  </returns>
        ///  created by: Nguyễn Đình Trường
        ///  created at: 05/28/2024
        Task<int> UpdateEducationExperienceByUserIdAsync(string newEducationExperience, Guid userId);
        Task<List<UserModelView>> GetAlumniListAsync();
        Task<List<UserModelView>> GetAlumniesByStatusAsync(string? status);
        Task<int> UpdateAlumniFieldsAsync(Guid userId, List<Guid> fieldIds);
        Task<int> UpdatePrivacySettingsByUserIdAsync(string updatedPrivacySettingsJson, Guid userId);

        /// <summary>
        /// Find alumni by full name, studentCode, email
        /// </summary>
        /// <param name="key">key to search</param>
        /// <returns></returns>
        Task<List<AlumniSearchToTagDTO>> FindAlumniByKeyWordsAsync(string key);

        /// <summary>
        /// Get AlumniProfileDTO  by user id
        /// </summary>
        /// <param name="id">user id to find alumni</param>
        /// <returns>Existing alumni profile dto</returns>
        /// <exception cref="NotFoundException">ALumni not found</exception>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 01/07/2024 
        Task<AlumniProfileDTO> GetAlumniProfileByUserIdAsync(Guid id);
    }
}