﻿
namespace FPT_Alumni.Core.Interfaces.Repositories
{
    public interface IBaseRepository<TEntity>
    {

        /// <summary>
		/// Find entity by id
		/// </summary>
		/// <param name="id">Entity's id to find </param>
		/// <returns>An Entity with type T or null if not found</returns>
		///  created by: Nguyễn Thiện Thắng
		///  created at: 2023/12/2
        Task<TEntity?> FindByIdAsync(Guid id);

        /// <summary>
		/// Insert new entity
		/// </summary>
		/// <param name="entity">Entity to Update </param>
		/// <returns>nummber of record affected</returns>
		///  created by: Nguyễn Thiện Thắng
		///  created at: 05/12/2024
        Task<int> InsertAsync(TEntity entity);


        /// <summary>
        ///  Multiple Insert entity by List of Entity
        /// </summary>
        /// <param name="entities">Entity list to Insert </param>
        /// <returns>nummber of record affected</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/12/2024
        Task<int> MultipleInsertAsync(List<TEntity> entities);

        /// <summary>
        /// Update entity by id
        /// </summary>
        /// <param name="entity">Entity to update </param>
        /// <returns>nummber of record affected</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/12/2024
        Task<int> UpdateAsync(TEntity entity);

        /// <summary>
        ///  Multiple update entity by List of Entity
        /// </summary>
        /// <param name="entities">Entity list to update </param>
        /// <returns>nummber of record affected</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/12/2024
        Task<int> MultipleUpdateAsync(List<TEntity> entities);

        /// <summary>
        /// Delete entity by Id
        /// </summary>
        /// <param name="entity">Entity to Delete </param>
        /// <returns>nummber of record affected</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/12/2024#
        Task<int> DeleteAsync(Guid id);

        /// <summary>
        ///  Multiple Delete entity by List of Entity
        /// </summary>
        /// <param name="entity">Entity list to Delete </param>
        /// <returns>nummber of record affected</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/12/2024
        Task<int> MultipleDeleteAsync(List<TEntity> entities);

        /// <summary>
        /// Check is value exist in db
        /// </summary>
        /// <param name="enity">entity to get table name</param>
        /// <param name="collumName"> Collumn Name in db</param>
        /// <param name="value">entity's attribute</param>
        /// <returns>
        /// True - value exits
        /// False - value do not exist
        /// </returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/26/2024  
        Task<bool> IsParamValueExistAsync<T>(TEntity enity, string collumName, T value);

    }
}
