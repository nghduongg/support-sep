﻿using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Repositories
{
    public interface IFieldRepository : IBaseRepository<Field>
    {
        Task<List<Field>> GetAllFieldAsync();


        Task<List<Field>> GetFieldsByMentoringId(Guid id);

        Task<List<Field>> SearchFieldByNameAsync(string fieldName);
    }
}
