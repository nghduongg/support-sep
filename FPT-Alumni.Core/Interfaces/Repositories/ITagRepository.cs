﻿using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Repositories
{
    public interface ITagRepository : IBaseRepository<Tag>
    {
        /// <summary>
        /// Get tag info by tag name
        /// </summary>
        /// <param name="tagName">tag name to find by</param>
        /// <returns>Tag info</returns>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 03/06/2024
        public Task<Tag?> FindByTagNameAsync(string tagName);

        /// <summary>
        /// find tag name by key words
        /// </summary>
        /// <param name="key"> key words to find by</param>
        /// <returns>List Of tag match key word</returns>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 07/06/2024
        public Task<List<Tag>> FindByTagNameForTagingAsync(string key);

        /// <summary>
        /// Delete all tag match id in ids
        /// </summary>
        /// <param name="ids">ids to delete</param>
        /// <returns>number of record affected</returns>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 06/19/2024
        public Task<int> MultipleDeleteById(List<Guid> ids);

        public Task<List<Tag>> GetAllAsync();


    }
}
