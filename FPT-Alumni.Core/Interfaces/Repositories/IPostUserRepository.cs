﻿using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Repositories
{
    public interface IPostUserRepository : IBaseRepository<PostUser>
    {
        /// <summary>
        /// Get all PostUser by postId
        /// </summary>
        /// <param name="postId">postId</param>
        /// <returns>PostUser list</returns>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 06/02/2024
        public Task<List<PostUser>> GetByPostIdAsync(Guid postId);

        /// <summary>
        /// Delete all postUser match id in ids
        /// </summary>
        /// <param name="postId">id to check to delete</param>
        /// <param name="userIds">ids to delete</param>
        /// <returns>number of record affected</returns>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 06/29/2024
        public Task<int> MultipleDeleteByPostIdAndTagId(Guid postId, List<Guid> userIds);

        /// <summary>
        /// Delete all postUser by postId
        /// </summary>
        /// <param name="postId">postId</param>
        /// <returns>Number of record affected</returns>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 06/20/2024
        public Task<int> DeleteByPostIdAsync(Guid postId);
    }
}
