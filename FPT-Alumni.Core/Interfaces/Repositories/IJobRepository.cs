﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Repositories
{
    public interface IJobRepository : IBaseRepository<Job>
    {
        Task<List<JobDTO>> GetMyJob(Guid userId, int currentPage, int pageSize);
        Task<List<JobDTO>> GetJobNeedApprove(int currentPage, int pageSize);
        Task<List<JobDTO>> GetOtherJob(Guid userId, int currentPage, int pageSize);
        Task<bool> ApproveJobAsync(Guid postId);
    }
}
