﻿using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Repositories
{
    public interface IApplicationRepository : IBaseRepository<Application>
    {
        Task<List<Application>> GetAllCandidateByJobId(Guid jobId, int currentPage, int pageSize);
        Task<bool> DeleteApplicationByJobIdAsync(Guid jobId);
    }
}
