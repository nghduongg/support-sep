﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Enums;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Repositories
{
    public interface IPostRepository : IBaseRepository<Post>
    {
        /// <summary>
        /// Get tag with all related info ( media, user tagged, tag, create user, date)
        /// </summary>
        /// <param name="id">post id</param>
        /// <param name="option"> option to get data related post
        /// 1 - get relate created user id to check when update and delete ( permission to do )
        /// 2 - get for user view 
        /// </param>
        /// <returns></returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 04/06/2024 
        public Task<PostDTO> GetPostDTOByIdAsync(Guid id, int option);


        /// <summary>
        /// update post is public / private
        /// </summary>
        /// <param name="postId">post id to update</param>
        /// <param name="status"> status to update for post: 
        /// 1- public 
        /// 2- private
        /// </param>
        /// <returns>success or failed</returns>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 08/06/2024
        public Task<int> UpdatePostStatusAsync(Guid postId, int status);

        Task<int> SavePostAsync(Post p, string action);

        /// <summary>
        /// Get Post dto and filter paging base filed,key search, start date, end date
        /// </summary>
        /// <param name="currentPage">Current page</param>
        /// <param name="pageSize"> number of record per page </param>
        /// <param name="userId"> user id</param>
        /// <param name="filedIds">filed ids to filter</param>
        /// <param name="key"> key search </param>
        /// <param name="fromDate"> start date</param>
        /// <param name="toDate"> end date</param>
        /// <returns>List of post in user profile</returns>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 24/06/2024
        public Task<List<PostDTO>> FilterPagingInUserPorfile(int currentPage, int pageSize, Guid userId, Guid[]? filedIds, string key, DateTime? fromDate, DateTime? toDate);
   
        /// <summary>
        /// paging all public post in group by filter
        /// </summary>
        /// <param name="currentPage">Current page</param>
        /// <param name="pageSize"> number of record per page </param>
        /// <param name="groupId">Group id</param>
        /// <param name="filedIds">Filed list</param>
        /// <param name="key">search key</param>
        /// <param name="startDate">From date post</param>
        /// <param name="endDate">To date post</param>
        /// <returns>List PostDTO by filter and pagin</returns>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 24/06/2024
        public Task<List<PostDTO>> FilterPagingInGroup(int currentPage, int pageSize, Guid groupId, Guid[]? filedIds, string key, DateTime? startDate, DateTime? endDate);
    }
}
