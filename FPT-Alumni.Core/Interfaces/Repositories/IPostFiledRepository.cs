﻿using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Repositories
{
    public interface IPostFiledRepository : IBaseRepository<PostField>
    {

        /// <summary>
        /// delete all post field by postId
        /// </summary>
        /// <param name="postId">post id to check to delete</param>
        /// <returns>number of recod affected</returns>
        public Task<int> DeleteByPostIdAsync(Guid postId);
    }
}
