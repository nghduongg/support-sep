﻿using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Repositories
{
    public interface IPostCategoryRepository : IBaseRepository<PostCategory>
    {

        /// <summary>
        /// Get all post category in db
        /// </summary>
        /// <param name="viewMode"> base view mode to get data -> staff / alumni </param>
        /// <returns>All post category</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 06/24/2024
        public Task<List<PostCategory>> GetAllBaseViewModeAsync(ViewModeEnum viewMode);
    }
}
