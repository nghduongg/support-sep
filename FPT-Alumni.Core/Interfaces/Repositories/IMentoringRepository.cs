﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Repositories
{
    public interface IMentoringRepository : IBaseRepository<Mentoring>
    {
        Task<int> DeleteMentoringFieldsByMentoringId(Guid id);
        Task<List<Language>> GetLanguagesByMentoringId(Guid id);
        Task<Mentoring> GetMentoringByIdAsync(Guid userId);
        Task<List<ConnectionAndWorking>> GetTypesByMentoringId(Guid id);
        Task<int> SaveMentoringAsync(Mentoring mentoring, string? action);
        Task<int> SaveMentoringFieldsAsync(Guid mentoringId, List<Guid> Fields);
        Task<int> SaveMentoringTypesAsync(Guid id, List<Guid> mentoringTypes);
        Task<int> SaveMentoringLanguagesAsync(Guid id, List<Guid> mentoringLanguages);
        Task<int> DeleteMentoringLanguagesByMentoringId(Guid id);
        Task<int> DeleteMentoringByMentoringId(Guid id);
        Task<List<Mentoring>> FilterMentoringsAsync(MentoringFilterDTO filter, string mentoringType);
        Task<Mentoring> GetMentoringByUserIdAndRoleAsync(Guid userId, string mentoringRole);
        Task<int> DeleteMentoringConnectionAndWorkingByMentoringId(Guid id);
        Task<List<Language>> GetLanguageListAsync();
        Task<List<ConnectionAndWorking>> GetConnectionAndWorkingsAsync();
        Task<int> SaveMentoringReportAsync(MentoringReport report);
        Task<List<MentoringReport>> GetReportListAsync();
        Task<MentoringReportDetails> GetReportDetailsAsync(Guid id);
    }
}
