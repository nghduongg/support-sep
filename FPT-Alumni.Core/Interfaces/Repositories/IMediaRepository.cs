﻿using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Repositories
{
    public interface IMediaRepository : IBaseRepository<Media>
    {
        /// <summary>
        /// Get all media by postId
        /// </summary>
        /// <param name="postId">postId</param>
        /// <returns>media list</returns>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 06/02/2024
        public Task<List<Media>> GetByPostIdAsync(Guid postId);

        /// <summary>
        /// Delete all media by postId
        /// </summary>
        /// <param name="postId">postId</param>
        /// <returns>Number of record affected</returns>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 06/09/2024
        public Task<int> DeleteByPostIdAsync(Guid postId);
    }
}
