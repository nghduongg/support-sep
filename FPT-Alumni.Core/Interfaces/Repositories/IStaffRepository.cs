﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Repositories
{
    public interface IStaffRepository : IBaseRepository<Staff>
    {
        Task<int> DeleteStaffByUserIdAsync(Guid userId);
        Task<UserModelView> GetStaffByUserIdAsync(Guid userId);
        Task<List<StaffWithUserModelView>> GetStaffListAsync();
        Task<List<StaffWithUserModelView>> GetStaffsByStatusAsync(string? status);
        Task<int> UpdateStaffByUserIdAsync(Staff updateStaffDto);

        Task<int> AddAlumniAsync(Staff createAlumniDto);
    }

}
