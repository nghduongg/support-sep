﻿using FPT_Alumni.Core.Entities;


namespace FPT_Alumni.Core.Interfaces.Repositories
{
    public interface IRoleRepository : IBaseRepository<Role>
    {

        /// <summary>
        /// Get all Role Information
        /// </summary>
        /// <param name=""></param>
        /// <returns>A List information with type Role or null if not found</returns>
        ///  created by: Vũ Xuân Trường
        ///  created at: 05/12/2024
        public Task<List<Role>> GetAllAsync();

    }
}
