﻿using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Repositories
{
    public interface IPostTagRepository : IBaseRepository<PostTag>
    {
        /// <summary>
        /// Get all PostTag by postId
        /// </summary>
        /// <param name="postId">postId</param>
        /// <returns>PostTag list</returns>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 06/02/2024
        public Task<List<PostTag>> GetByPostIdAsync(Guid postId);


        /// <summary>
        /// Delete all tag match id in ids
        /// </summary>
        /// <param name="postId">id to check to delete</param>
        /// <param name="tagIds">ids to delete</param>
        /// <returns>number of record affected</returns>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 06/19/2024
        public Task<int> MultipleDeleteByPostIdAndTagId(Guid postId, List<Guid> tagIds);
    }
}
