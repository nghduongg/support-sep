﻿using FPT_Alumni.Core.Entities;


namespace FPT_Alumni.Core.Interfaces.Repositories
{
    public interface INewsCategoryRepository : IBaseRepository<NewsCategory>
    {
    }
}
