﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface ITagService : IBaseService<Tag>
    {
        /// <summary>
        /// Find tag by key words
        /// </summary>
        /// <param name="key">key words to find by</param>
        /// <returns>List of tag match with key words</returns>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 07/06/2024
        public Task<ServiceResult> FindTagByKeyWordForTagingServiceAsync(string key);

        public Task<ServiceResult> GetAllServiceAsync();
    }
}
