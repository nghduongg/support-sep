﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface IStaffService : IBaseService<Staff>
    {
        Task<ServiceResult> GetStaffListServiceAsync();
        Task<ServiceResult> SaveStaffServiceAsync(StaffWithUserModelView staffSaveDTO);
        Task<ServiceResult> DeleteStaffServiceAsync(Guid userId);
        Task<ServiceResult> GetStaffByStatusServiceAsync(string? status);
    }
}
