﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface IFieldService : IBaseService<Field>
    {
        Task<ServiceResult> AddFieldServiceAsync(FieldSaveDTO fieldSave);
        Task<ServiceResult> GetFieldListServiceAsync();
        Task<ServiceResult> UpdateFieldServiceAsync(FieldSaveDTO fieldSave);

        /// <summary>
        /// split key Search and search all field match with key search
        /// </summary>
        /// <param name="key">Search key word</param>
        /// <returns>All Field match with key search </returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 06/23/2024
        public Task<ServiceResult> SearchServiceAsync(string key);

        /// <summary>
        /// Get all field
        /// </summary>
        /// <returns>All field in db </returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 06/23/2024
        public Task<ServiceResult> GetAllServiceAsync();
    }
}
