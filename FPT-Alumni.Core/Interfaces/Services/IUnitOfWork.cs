﻿using FPT_Alumni.Core.Interfaces.Repositories;
using FPT_Alumni.Infrastructure.Repositories;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository User { get; }
        IRoleRepository Role { get; }
        IAlumniRepository Alumni { get; }
        INewsRepository News { get; }
        INewsCategoryRepository NewsCategory { get; }
        IFieldRepository Field { get; }
        IAlumniFieldRepository AlumniField { get; }
        IPostRepository Post { get; }
        IMediaRepository Media { get; }
        ITagRepository Tag { get; }
        IPostTagRepository PostTag { get; }
        IPostUserRepository PostUser { get; }
        IStaffRepository Staff { get; }
        IMentoringRepository Mentoring { get; }
        IPostFiledRepository PostFiled { get; }
        IPostCategoryRepository PostCategory { get; }
        ICommentRepository Comment { get; }
        IJobRepository Job { get; }
        IApplicationRepository Application { get; }

        /// <summary>
        /// Begin a Trasaction
        /// created by: Nguyễn Thiện Thắng
        /// created at:05/12/2024
        /// </summary>
        void BeginTransaction();
        /// <summary>
        /// Commit a Trasaction
        /// created by: Nguyễn Thiện Thắng
        /// created at:05/12/2024
        /// </summary>
        void Commit();
        /// <summary>
        /// Roll back Trasaction's data
        /// created by: Nguyễn Thiện Thắng
        /// created at:05/12/2024
        /// </summary>
        void RollBack();
    }
}
