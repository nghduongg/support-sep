﻿using FPT_Alumni.Core.DTOs;

namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface IAuthService
    {
        /// <summary>
        /// Authenticates a user using a single credential. (for SSO)
        /// </summary>
        /// <param name="credential">The credential (e.g., email or fullname) for authentication.</param>
        /// <returns>A service result indicating the outcome of the authentication process.</returns>
        /// created by: Nguyễn Đình Trường
        ///  created at: 2024/20/5
        Task<ServiceResult> AuthenticateAsync(string credential);

        /// <summary>
        /// Registers a new user with the provided unverified model.
        /// </summary>
        /// <param name="unauthenticatedModel">The model containing user information for registration.</param>
        /// <returns>A service result indicating the outcome of the registration process.</returns>
        /// created by: Nguyễn Đình Trường
        ///  created at: 2024/20/5
        Task<ServiceResult> RegisterServiceAsync(UnverifiedModel unauthenticatedModel);

        /// <summary>
        /// Authenticates a user using a login model containing credentials. (for login by form email+password)
        /// </summary>
        /// <param name="loginModel">The model containing the login credentials (e.g., username and password).</param>
        /// <returns>A service result indicating the outcome of the authentication process.</returns>
        /// created by: Nguyễn Đình Trường
        ///  created at: 2024/20/5
        Task<ServiceResult> AuthenticateAsync(LoginModel loginModel);
    }
}