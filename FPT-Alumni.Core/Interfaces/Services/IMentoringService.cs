﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface IMentoringService : IBaseService<Mentoring>
    {
        Task<ServiceResult> CreateMentoringServiceAsync(MentoringSaveDTO mentoringSave);

        Task<ServiceResult> CreateMentoringServiceAsync(MentoringSearchDTO mentoringSave);
        Task<ServiceResult> DeleteMentoringServiceAsync(Guid id);
        Task<ServiceResult> GetMentoringByIdServiceAsync(Guid userId);
        Task<ServiceResult> FilterMentorAsync(MentoringFilterDTO filter);
        Task<ServiceResult> GetMentoringFormServiceAsync(string mentoringRole);
        Task<ServiceResult> SaveMentoringReportServiceAsync(MentoringReportSaveDTO filter);
        Task<ServiceResult> GetMentoringReportsServiceAsync();
        Task<ServiceResult> GetMentoringReportByIdServiceAsync(Guid id);
    }
}
