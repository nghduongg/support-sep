﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface IApplicationService
    {
        public Task<ServiceResult> AddNewApplicationAsync(Application application, string accessToken);
        public Task<ServiceResult> GetAllCandidateByJobId(Guid jobId, int currentPage, int pageSize);

    }
}
