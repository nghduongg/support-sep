﻿using FPT_Alumni.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface IJobService
    {
        /// <summary>
        /// Split all data from JobDTO ( job, post) and map to Entity
        /// </summary>
        /// <param name="job">Job from user to split data</param>
        /// <param name="accessToken"> token to get own id</param>
        /// <returns>number off record affected and all realted infomation</returns>
        /// created by: Vũ Xuân Trường
        /// created at: 06/25/2024
        public Task<ServiceResult> AddNewJobAsync(JobDTO job, string accessToken);
        public Task<ServiceResult> GetMyJob(string accessToken, int currentPage, int pageSize);
        public Task<ServiceResult> GetJobNeedApprove(string accessToken, int currentPage, int pageSize);
        public Task<ServiceResult> GetOtherJob(string accessToken, int currentPage, int pageSize);
        public Task<ServiceResult> ApprovingJobServiceAsync(Guid postId);
        public Task<ServiceResult> DeleteJobAsync(Guid postId, Guid jobId);
    }
}
