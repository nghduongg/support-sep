﻿using FPT_Alumni.Core.DTOs;


namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface IBaseService<T>
    {

        /// <summary>
		/// Find entity by Id and map data to ServiceResutl
		/// </summary>
		/// <param name="id">Id for entity searching </param>
		/// <returns>Service result ( sucsess or failed with all details )</returns>
        /// <exception cref="NotFoundException">Data not found</exception>
		///  created by: Nguyễn Thiện Thắng
		///  created at: 05/12/2024
        Task<ServiceResult> FindByIdServiceAsync(Guid id);

        /// <summary>
		/// Check is all entity' properies is valid -> insert
		/// </summary>
		/// <param name="entity">Entity to check </param>
		/// <returns>Service result ( sucsess or failed with all details )</returns>
		///  created by: Nguyễn Thiện Thắng
		///  created at: 05/12/2024
		Task<ServiceResult> InsertServiceAsync(T entity);

        /// <summary>
        /// Check is all entity' properies is valid -> update
        /// </summary>
        /// <param name="entity">Entity to check </param>
        /// <returns>Service result ( sucsess or failed with all details )</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/12/2024
        Task<ServiceResult> UpdateServiceAsync(T entity);

        /// <summary>
        /// Check is all entity' properies is valid -> delete
        /// </summary>
        /// <param name="entity">Entity to check </param>
        /// <returns>Service result ( sucsess or failed with all details )</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/12/2024
        Task<ServiceResult> DeleteServiceAsync(Guid id);
    }
}
