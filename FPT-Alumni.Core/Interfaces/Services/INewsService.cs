﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;


namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface INewsService : IBaseService<News>
    {
        /// <summary>
        /// Search and paging data base key, page and size
        /// </summary>
        /// <param name="key"> keywords for searching</param>
        /// <param name="page"> current page</param>
        /// <param name="size"> number of item per page</param>
        /// <returns>News list was filter and paging inside service result</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/22/2024
        public Task<ServiceResult> FilterPagingServiceAsync(string key, int page, int size);
    }
}
