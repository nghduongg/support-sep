﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface ICacheService
    {
        /// <summary>
        /// Create key for data and save to cache server
        /// </summary>
        /// <typeparam name="T">Data type</typeparam>
        /// <param name="Data">data to save to cache</param>
        /// <returns>key to get data in the cache ( experied 30 min )</returns>
        public Guid CreateKey<T>(T Data);
    }
}
