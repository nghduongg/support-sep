﻿using Microsoft.AspNetCore.Http;


namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface IFileValidation
    {
        /// <summary>
        /// validate file base on expected extention and expected header
        /// </summary>
        /// <param name="file"> File to validate</param>
        /// <param name="expectedExtention"> Expected file extention</param>
        /// <param name="expectedHeader"> for excel file only </param>
        /// <exception cref="BadRequestException">Wrong file format</exception>
        /// <exception cref="Exception">Upload failed</exception>
        public Task ValidateFileAsync(IFormFile file, string expectedExtention, string[] expectedHeader);
    }
}
