﻿using FPT_Alumni.Core.Entities;

namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface INewsCategoryService : IBaseService<NewsCategory>
    {
    }

}
