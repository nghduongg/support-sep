﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;

namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface ITokenService
    {
        /// <summary>
        /// Sets a cookie with the specified name, value, and expiration time.
        /// </summary>
        /// <param name="variable">The name of the cookie.</param>
        /// <param name="value">The value of the cookie.</param>
        /// <param name="expires">The expiration date and time of the cookie.</param>
        ///   created by: Nguyễn Đình Trường
        ///  created at: 2024/20/5
        void SetCookies(string variable, string value, DateTime expires);

        /// <summary>
        /// Generates a new refresh token.
        /// </summary>
        /// <returns>A model representing the refresh token.</returns>
        ///   created by: Nguyễn Đình Trường
        ///  created at: 2024/20/5
        RefreshTokenModel GenerateRefreshToken();

        /// <summary>	
        /// Generates a JWT for the specified user.
        /// </summary>
        /// <param name="user">The user for whom to generate the JWT.</param>
        /// <returns>A DTO containing the token result.</returns>
        ///  created by: Nguyễn Đình Trường
        ///  created at: 2024/26/5
        Task<TokenResultDTO> JWTGeneratorAsync(User user);

        /// <summary>
        /// Generates an access token for the specified user.
        /// </summary>
        /// <param name="user">The user for whom to generate the access token.</param>
        /// <returns>The generated access token as a string.</returns>
        ///   created by: Nguyễn Đình Trường
        ///  created at: 2024/26/5
        string GenerateAccessToken(User user);

        /// <summary>
        /// Refreshes the access token using the provided refresh token.
        /// </summary>
        /// <param name="refreshToken">The refresh token used to obtain a new access token. if param = "logout", delete logged email from db and remove token from cookie</param>
        /// <param name="action">The action to perform, defaults to "refresh".</param>
        /// <returns>A service result indicating the outcome of the operation.</returns>
        /// created by: Nguyễn Đình Trường
        ///  created at: 2024/20/5
        Task<ServiceResult> RefreshServiceAsync(string? refreshToken, string? action = "refresh");

        /// <summary>
        /// Updates the refresh token for the specified user.
        /// </summary>
        /// <param name="user">The user whose refresh token is to be updated.</param>
        /// <returns>A task representing the asynchronous operation, containing the updated user.</returns>
        /// created by: Nguyễn Đình Trường
        ///  created at: 2024/20/5
        Task<User> UpdateUserRefreshTokenAsync(User user);

        /// <summary>	
        /// Validate a token to extract user information
        /// </summary>
        /// <param name="token">token to extract</param>
        /// <returns>User information as a principal</returns>
        ///  created by: Nguyễn Đình Trường
        ///  created at: 2024/26/5
        Task<ClaimsPrincipal> GetPrincipalFromExpiredTokenAsync(string token);

        string GenerateChangePasswordToken(Guid userId);

        bool IsTokenValid(string token);
    }
}