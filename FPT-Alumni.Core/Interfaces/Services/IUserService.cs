﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.JsonModels;

namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface IUserService : IBaseService<User>
    {
        /// <summary>
        /// Get all User Information
        /// </summary>
        /// <param name=""></param>
        /// <returns>Service result ( sucsess or failed with all details )</returns>
        ///  created by: Vũ Xuân Trường
        ///  created at: 05/12/2024
        Task<ServiceResult> GetAllAsync();

        /// <summary>
        /// Get full information of alumni with email and role from User table
        /// </summary>
        ///  <param name="accessToken">accessToken contains UserId</param>
        /// <returns>alumni information</returns>
        ///  created by: Nguyễn Đình Trường
        ///  created at: 05/28/2024
        Task<ServiceResult> GetUserServiceAsync(string? accessToken);

        /// <summary>
        /// Update information of alumni, if email not null, it will be update 
        /// </summary>
        ///  <param name="alumniWithUserModel">alumniWithUserModel contains all field of alumni and user Update</param>
        /// <returns>status updated</returns>
        ///  created by: Nguyễn Đình Trường
        ///  created at: 05/28/2024
        Task<ServiceResult> SettingUpdateServiceAsync(UserModelSave alumniWithUserModel, string accesstoken);

        /// <summary>
        /// Insert new work experience in alumni table to db by json string
        /// </summary>
        ///  <param name="workExperience">workExperience contains userid and all field of work experience to insert</param>
        /// <returns>status inserted</returns>
        ///  created by: Nguyễn Đình Trường
        ///  created at: 05/28/2024
        Task<ServiceResult> CreateNewWorkExperienceServiceAsync(WorkExperienceSaveDTO workExperience);

        /// <summary>
        /// Insert new education experience in alumni table to db by json string
        /// </summary>
        ///  <param name="educationExperienceSaveDTO">educationExperienceSaveDTO contains userid and all field of education experience to insert</param>
        /// <returns>status inserted</returns>
        ///  created by: Nguyễn Đình Trường
        ///  created at: 05/28/2024
        
        Task<ServiceResult> CreateNewEducationExperienceServiceAsync(EducationExperienceSaveDTO educationExperienceSaveDTO);
        
        Task<ServiceResult> GetUserByEmailServiceAsync(string? email);
        
        Task<ServiceResult> ChangePasswordServiceAsync(ChangePasswordModel changePasswordModel);

        Task<ServiceResult> ApprovingRoleServiceAsync(UserStatusUpdateDTO userStatusUpdateDTO);
        Task<ServiceResult> CreateNewPrivacySettingsServiceAsync(PrivacySettingsSaveDTO privacySettingsSaveDTO);
        Task<ServiceResult> UpdateWorkExperienceServiceAsync(UpdateWorkExperienceDTO workExperienceDTO);
        Task<ServiceResult> UpdateEducationExperienceServiceAsync(UpdateEducationExperienceDTO updateEducationExperienceDTO);

        Task<ServiceResult> DeleteUserServiceAsync(Guid userId);

        Task<ServiceResult> GetLoggedUser(string? accessToken);

        Task<ServiceResult> ApprovingRolesServiceAsync(List<UserStatusUpdateDTO> userStatusUpdateDTOs);
    }
}
