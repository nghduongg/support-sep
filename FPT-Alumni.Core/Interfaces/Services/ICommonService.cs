﻿using FPT_Alumni.Core.DTOs;


namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface ICommonService
    {
        /// <summary>
        /// Send email using SMTP Google Server (smtp.gmail.com)
        /// </summary>
        /// <param name="mail">MailDTO for user to send</param>
        /// <returns>true/false</returns>
        ///  created by: Vũ Xuân Trường
        ///  created at: 05/25/2024
        Task<bool> SendMailAsync(MailDTO mail);

        string GenerateRandomPassword(int length);

        /// <summary>
        /// Decode access token and get userId in token
        /// </summary>
        /// <param name="accessToken">access token to decode</param>
        /// <returns> user id </returns>
        /// <exception cref="BadRequestException">Access token is null</exception>
        /// created by: Nguyễn Thiện Thắng
        ///  created at: 06/06/2024
        public Task<Guid> GetUserIdByAccesTokenAsync(string? accessToken);

        /// <summary>
        /// Decode access token and get userId and role id in token
        /// </summary>
        /// <param name="accessToken">access token to decode</param>
        /// <returns> user id </returns>
        /// <exception cref="BadRequestException">Access token is null</exception>
        /// created by: Nguyễn Thiện Thắng
        ///  created at: 06/16/2024
        public Task<UserRoleDTO> GetUserIdAndRoleByAccesTokenAsync(string? accessToken);

        /// <summary>
        ///  Delete all extra spaces between words
        /// </summary>
        /// <param name="text">text to delete extra spaces</param>
        /// <returns>validated text</returns>
        /// created by: Nguyễn Thiện Thắng
        ///  created at: 06/06/2024
        public string RemoveTextSpace(string text);

        /// <summary>
        /// remove all unicode character in text string
        /// </summary>
        /// <param name="invalidText"> invalid text to remove unicode character </param>
        /// <returns>valid char</returns>
        /// created by: Nguyễn Thiện Thắng
        ///  created at: 06/06/2024
        public string RemoveUnicode(string invalidText);

        Task<string> GetAccessTokenAsync();
    }
}
