﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface IPostCategoryService : IBaseService<PostCategory>
    {
        /// <summary>
        /// Get all post category base view mode and map to service result
        /// </summary>
        /// <param name="accessToken">to get role -> get data base role</param>
        /// <returns>All post category</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 06/24/2024
        public Task<ServiceResult> GetAllBaseRoleServiceAsync(string accessToken);
    }
}
