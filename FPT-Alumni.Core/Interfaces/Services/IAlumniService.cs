﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using Microsoft.AspNetCore.Http;

namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface IAlumniService : IBaseService<Alumni>
    {
        /// <summary>
        /// Check is file valid -> preview file check all data in file is valid 
        /// </summary>
        /// <param name="file"> file to preview</param>
        /// <returns> Object list valid and invalid key  </returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/24/2024 
        Task<AlumniAndKeyDTO> PreviewFileServiceAsync(IFormFile file);

        /// <summary>
        /// Import data form excel file in to database
        /// </summary>
        /// <param name="file"> file to get data import</param>
        /// <exception cref="BadRequestException">key expired ( 30min )</exception>
        /// <exception cref="Exception">Error while insert alumni</exception>
        /// <returns> Number of succes data and invalid key to get invalid data file </returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/26/2024
        Task<ServiceResult> ImportFileServiceAsync(IFormFile file);

        /// <summary>
        /// get data from cache base key and export invalid alumni list
        /// </summary>
        /// <param name="key">key to get data from cache</param>
        /// <exception cref="BadRequestException">key expired ( 30min )|| key do not exist</exception>
        /// <returns> Invalid alumni list in service result</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/26/2024
        Task<ServiceResult> ExportFileServiceAsync(string key);
        Task<ServiceResult> GetAlumniListAsync();
        Task<ServiceResult> GetAlumniByStatusAsync(string? status);

        /// <summary>
        /// find alumni by full name, student code, email
        /// </summary>
        /// <param name="key">key words to search</param>
        /// <returns>alumni list math key word</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/26/2024
        Task<ServiceResult> FindAlumniByKeyWordsAsync(string key);

        Task<ServiceResult> SaveAlumniAsync(UserModelSave alumniSaveDTO);
        Task<ServiceResult> DeleteAlumniServiceAsync(Guid userId);
        Task<ServiceResult> GetAlumniByUserIdServiceAsync(Guid userId);
        /// <summary>
        /// Get alumni profile DTO by user Id
        /// </summary>
        /// <param name="userId">To find alumni</param>
        /// <returns>Existing alumni profile DTO</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 01/07/2024
        Task<AlumniProfileDTO> GetAlumniProfileByUserIdAsync(Guid userId);

        /// <summary>
        /// Hiddend alumni data base settings 
        /// </summary>
        /// <param name="alumniProfileDTO">Obj to hidden data</param>
        /// <returns>Alumni profile hiddend data base settings</returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 02/07/2024
        AlumniProfileDTO HiddenDataBaseSettings(AlumniProfileDTO alumniProfileDTO);
    }
}