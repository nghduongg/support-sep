﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;


namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface IRoleService : IBaseService<Role>
    {
        /// <summary>
		/// Get all Role Information
		/// </summary>
		/// <param name=""></param>
		/// <returns>Service result ( sucsess or failed with all details )</returns>
		///  created by: Vũ Xuân Trường
		///  created at: 05/12/2024
        Task<ServiceResult> GetAllAsync();

        string GetRoleNameById(Guid roleId);
    }
}
