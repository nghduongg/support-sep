﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface ICommentService
    {
        Task<ServiceResult> GetPageOfCommentsByPostIdAsync(Guid postId, int pageNumber, int pageSize);
        Task<ServiceResult> GetCommentsByPostIdAsync(Guid postId);
        Task<ServiceResult> GetNumberOfCommentsByPostIdAsync(Guid postId);
        Task<ServiceResult> GetCommentsByParentCommentIdAsync(string parentCommentId);
        Task<ServiceResult> DeleteServiceAsync(string id);
        Task<ServiceResult> InsertServiceAsync(Comment entity, string accessToken);
        Task<ServiceResult> UpdateServiceAsync(Comment entity);
    }
}
