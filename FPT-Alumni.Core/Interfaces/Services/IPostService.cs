﻿using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface IPostService : IBaseService<Post>
    {
        /// <summary>
        /// Split all data from Post InsertDTO ( post, media, tag, userTagged) and map to Entity
        /// </summary>
        /// <param name="post">Post from user to split data</param>
        /// <param name="accessToken"> token to get own id</param>
        /// <returns>number off record affected and all realted infomation</returns>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 06/02/2024
        public Task<ServiceResult> InsertPostDTOServiceAsync(PostInsertDTO post, string accessToken);

        /// <summary>
        /// Split all data from Post InsertDTO ( post, media, tag, userTagged) and map to Entity
        /// </summary>
        /// <param name="post">Post from user to split data</param>
        /// <param name="accessToken"> token to get own id</param>
        /// <returns>number off record affected and all realted infomation</returns>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 06/08/2024
        public Task<ServiceResult> BeforeUpdateAsync(PostUpdateDTO post, string accessToken);

        /// <summary>
        /// auto paging Post in newFeed 
        /// </summary>
        /// <param name="page">current page</param>
        /// <param name="pagesize">number of pageSize</param>
        /// <returns>PostDTO list</returns>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 06/04/2024
        public Task<List<PostDTO>> AutoPagingPostDTO(int page, int pagesize);

        /// <summary>
        /// check status & postId & ownId valid -> update post status to public / private
        /// </summary>
        /// <param name="postId">post id to update</param>
        /// <param name="status"> status to upate </param>
        /// <param name="accessToken"> token to get own id </param>
        /// <returns>update Success or failed</returns>
        /// <exception cref="BadRequestException">invalid status or post id</exception>
        /// <exception cref="Exception">Cannot update</exception>
        /// created by: Nguyễn Thiện Thắng
        /// created at: 08/06/2024
        public Task<ServiceResult> UpdatePostStatusServiceAsync(string accessToken, Guid postId, string status);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="accessToken"></param>
        /// <param name="ownId"></param>
        /// <param name="filedIds"></param>
        /// <param name="key"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public Task<ServiceResult> FilterPagingInUserProfile(int currentPage, int pageSize, string accessToken, Guid ownId, Guid[]? filedIds, string key, DateTime? fromDate, DateTime? toDate);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="accessToken"></param>
        /// <param name="groupId"></param>
        /// <param name="filedIds"></param>
        /// <param name="key"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public Task<ServiceResult> FilterPagingInGroup(int currentPage, int pageSize, string accessToken, Guid groupId, Guid[]? filedIds, string key, DateTime? fromDate, DateTime? toDate);

    }
}
