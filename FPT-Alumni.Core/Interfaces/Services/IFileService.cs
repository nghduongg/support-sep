﻿using FPT_Alumni.Core.DTOs;


namespace FPT_Alumni.Core.Interfaces.Services
{
    public interface IFileService
    {
        /// <summary>
        /// Write data to excel file
        /// </summary>
        /// <param name="headers">Header collumn in excel file</param>
        /// <param name="datas">Data to write to file</param>
        /// <returns> excel file with all data </returns>
        ///  created by: Nguyễn Thiện Thắng
        ///  created at: 05/26/2024
        Task<ServiceResult> ExportFileService<T>(string[] headers, List<T> datas);
    }
}
