﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Entities
{
    public class Specialization : BaseEntityId
    {
        public string Name { get; set; }
        public Guid FieldId { get; set; }
    }
}
