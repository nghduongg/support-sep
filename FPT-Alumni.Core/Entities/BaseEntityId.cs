﻿using System.ComponentModel.DataAnnotations;


namespace FPT_Alumni.Core.Entities
{
    public class BaseEntityId
    {
        [Required]
        public Guid Id { get; set; }

       

    }
}
