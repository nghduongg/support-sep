﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Entities
{
    public class Media : BaseEntityId
    {
        public Guid PostId { get; set; }

        [StringLength(30)]
        public string MediaType { get; set; }
        [StringLength(1000)]
        public string MediaUrl { get; set; }
    }
}
