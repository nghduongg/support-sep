﻿using FPT_Alumni.Core.Enums;
using System.ComponentModel.DataAnnotations;


namespace FPT_Alumni.Core.Entities
{
    public class EventAttendance : BaseEntityId
    {
        [Required]
        public Guid EventId { get; set; }
        /// <summary>
        /// Paticipant Id
        /// </summary>

        [Required]
        public Guid Participant { get; set; }

        /// <summary>
        /// 1- attend
        /// 2- absent
        /// </summary>

        [Required]
        public Boolean AttendanceStatus { get; set; }

        /// <summary>
        /// Participant Role
        /// </summary>
        [Required]
        ParticipantRoleEnum ParticipantRole { get; set; }

    }
}
