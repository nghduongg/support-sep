﻿using System.ComponentModel.DataAnnotations;


namespace FPT_Alumni.Core.Entities
{
    public class News : BaseEntityWithDate
    {
        [Required(ErrorMessage = "Title could not be null")]
        [StringLength(200, ErrorMessage = "Max length of title is 200")]
        public string? Title { get; set; }

        [Required(ErrorMessage = "Description could not be null")]
        [StringLength(500)]
        public string? Description { get; set; }

        [Required(ErrorMessage = "CoverPhoto could not be null")]
        [StringLength(200)]
        public string? CoverPhoto { get; set; }

        [Required(ErrorMessage = "BackGroundPhoto could not be null")]
        [StringLength(500)]
        public string? BackGroundPhoto { get; set; }

        [Required(ErrorMessage = "PostedDate could not be null")]
        public DateTime PostedDate { get; set; }

        [Required(ErrorMessage = "Details could not be null")]
        [StringLength(3000)]
        public string? Details { get; set; }

        /// <summary>
        /// show
        /// hidden
        /// </summary>
        public bool Status { get; set; }

        public Guid CategoryId { get; set; }
    }
}
