﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Entities
{
    public class Staff : BaseEntityId
    {
        public string FullName { get; set; }
        public string Campus { get; set; }
        public Guid UserId { get; set; }
    }
}
