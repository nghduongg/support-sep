﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Entities
{
    /// <summary>
    /// many to many relationship between User and post
    /// </summary>
    public class PostUser
    {
        public Guid? UserId { get; set; }
        public Guid PostId { get; set; }
    }
}
