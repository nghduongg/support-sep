﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Entities
{
    public class Post : BaseEntityWithDate
    {
        [StringLength(100)]
        public string? Title { get; set; }

        [StringLength(2000)]
        public string? Content { get; set; }
        public bool IsPublic { get; set; }
        public Guid CategoryId { get; set; }
        public Guid? GroupId { get; set; }
    }
}
