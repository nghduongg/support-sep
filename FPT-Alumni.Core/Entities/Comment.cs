﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Entities
{
    public class Comment
    {
        public string Id { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public DateTime ModifiedDate { get; set; } = DateTime.Now;
        public Guid CreatedBy { get; set; }
        public Guid ModifiedBy { get; set; }
        public string Content { get; set; }
        public string? ParentCommentId { get; set; }
        public string? Fullname { get; set; }
        public string? Avatar { get; set; }
        public Guid PostId { get; set; }
        public List<Comment>? ChildComments { get; set; }
    }
}
