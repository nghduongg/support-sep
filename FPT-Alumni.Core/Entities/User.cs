﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;


namespace FPT_Alumni.Core.Entities
{
    public class User : BaseEntityId
    {
        [Required(ErrorMessage = "Email is required.")]
        [EmailAddress(ErrorMessage = "Wrong email format.")]
        public string MainEmail { get; set; }

        [EmailAddress(ErrorMessage = "Wrong email format.")]
        public string? SubEmail { get; set; }

        [EmailAddress(ErrorMessage = "Wrong email format.")]
        public string? EmailLogged { get; set; }

        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$",
        ErrorMessage = "Password must be at least 8 characters long and contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character.")]
        public string? Password { get; set; }

        [StringLength(150)]
        public string? RefreshToken { get; set; }

        public DateTime? ExperiedDateToken { get; set; }

        public string? Status { get; set; }

        [Required]
        public Guid? RoleId { get; set; }

    }
}
