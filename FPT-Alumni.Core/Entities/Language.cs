﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Entities
{
    public class Language
    {
        public Guid Id { get; set; }

        public string? Name { get; set; }
    }
}
