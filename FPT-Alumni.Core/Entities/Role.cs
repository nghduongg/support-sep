﻿using System.ComponentModel.DataAnnotations;


namespace FPT_Alumni.Core.Entities
{
    public class Role : BaseEntityId
    {
        [Required]
        public string RoleName { get; set; }
    }
}
