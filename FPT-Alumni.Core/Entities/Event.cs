﻿using System.ComponentModel.DataAnnotations;


namespace FPT_Alumni.Core.Entities
{
    public class Event : BaseEntityWithDate
    {
        [Required(ErrorMessage = "Title could not be null")]
        [StringLength(200, ErrorMessage = "Max length of title is 200")]
        string Title { get; set; }

        [Required(ErrorMessage = "Description could not be null")]
        [StringLength(500)]
        string Description { get; set; }

        [Required(ErrorMessage = "CoverPhoto could not be null")]
        [StringLength(200)]
        string CoverPhoto { get; set; }

        [Required(ErrorMessage = "BackGroundPhoto could not be null")]
        [StringLength(500)]
        string BackGroundPhoto { get; set; }

        [Required(ErrorMessage = "StartDate could not be null")]
        DateTime StartDate { get; set; }

        [Required(ErrorMessage = "EndDate could not be null")]
        DateTime EndDate { get; set; }

        [Required(ErrorMessage = "Location could not be null")]
        [StringLength(200)]
        string Location { get; set; }

        [Required(ErrorMessage = "Details could not be null")]
        [StringLength(3000)]
        string Details { get; set; }
    }
}
