﻿

namespace FPT_Alumni.Core.Entities
{
    public class NewsCategory : BaseEntityWithDate
    {
        public string Name { get; set; }
    }
}
