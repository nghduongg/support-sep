﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Entities
{
    public class Tag : BaseEntityId
    {
        /// <summary>
        /// type of tag name ( all class / tag by field )
        /// </summary>
        public string TagName { get; set; }
    }
}
