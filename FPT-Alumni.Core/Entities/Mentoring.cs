﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Entities
{
    public class Mentoring : BaseEntityId
    {
        public Guid UserId { get; set; }
        [MaxLength(1000)]
        public string? BasicInformation { get; set; }
        public string? CurrentCountry { get; set; }
        public string? CurrentCity { get; set; }

        [MaxLength(1000)]
        public string? WorkAndEducation { get; set; }

        public string? OtherPreferences {  get; set; }

        public string? MentoringRole { get; set; }
    }
}
