﻿using System.ComponentModel.DataAnnotations;

namespace FPT_Alumni.Core.Entities
{
    public class BaseEntityWithDate
    {
        /// <summary>
        /// Entity Id
        /// </summary>
        [Required]
        public Guid Id { get; set; }

        /// <summary>
        /// Created date
        /// </summary>
        public DateTime CreatedDate { get; set; } = DateTime.UtcNow;

        /// <summary>
        /// Modified date
        /// </summary>
        public DateTime ModifiedDate { get; set; } = DateTime.UtcNow;

        /// <summary>
        /// Created's user id
        /// </summary>
        public Guid CreatedBy { get; set; }

        /// <summary>
        /// Modified's user id
        /// </summary>
        public Guid ModifiedBy { get; set; }

    }
}
