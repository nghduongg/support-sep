﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Entities
{
    public class Group : BaseEntityId
    {

        public string GroupName { get; set; }

        public bool IsPublic { get; set; }

        [MaxLength(1000)]
        public string Policy { get; set; }
    }
}
