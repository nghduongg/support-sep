﻿using FPT_Alumni.Core.Enums;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;


namespace FPT_Alumni.Core.Entities
{
    public class Alumni : BaseEntityWithDate
    {
        [StringLength(200)]
        public string? Avartar { get; set; }

        public string? StudentCode { get; set; }

        [StringLength(20)]
        public string? PhoneNumber { get; set; }

        /// <summary>
        /// School year
        /// </summary>
        [StringLength(10)]
        public string? Class { get; set; }

        [StringLength(50)]
        public string Major { get; set; }

        [Required]
        public bool IsGraduated { get; set; }

        [StringLength(100)]
        public string? Job { get; set; }

        [StringLength(100)]
        public string? Company { get; set; }

        [StringLength(100)]
        public string? LinkedUrl { get; set; }

        /// <summary>
        /// Work Experience with json format
        /// </summary>
        [StringLength(1000)]
        public string? WorkExperience { get; set; }

        /// <summary>
        /// Education Experience with json format
        /// </summary>
        [StringLength(1000)]
        public string? EducationExperience { get; set; }

        /// <summary>
        /// Privacy     Settings with json format
        /// </summary>
        [StringLength(1000)]
        public string? PrivacySettings { get; set; }

        [Required]
        [StringLength(100)]
        public string? FullName { get; set; }
        [Required]
        public DateTime? DateOfBirth { get; set; }
        public string? GraduationYear { get; set; }
        public GenderEnum? Gender {  get; set; } 

        [Required]
        [StringLength(100)]
        public string? Country { get; set; }

        [Required]
        [StringLength(100)]
        public string? City { get; set; }

        public Guid UserId { get; set; }
    }
}
