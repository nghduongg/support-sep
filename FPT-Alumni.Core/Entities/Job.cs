﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Entities
{
    public class Job : BaseEntityId
    {
        public string Position { get; set; }
        public string Salary { get; set; }
        public string Address { get; set; }
        public string? Requirement { get; set; }
        public string? Benefit { get; set; }
        public DateTime Deadline { get; set; }
        public string? Email { get; set; }
        public Guid PostId { get; set; }
        public Guid FieldId { get; set; }
        public string? CompanyDescription { get; set; }

    }
}
