﻿
namespace FPT_Alumni.Core.Entities
{
    public class MentoringReport
    {
        public Guid Id { get; set; }
        public Guid ReporterId { get; set; }
        public Guid VictimId { get; set; }
        public string? Description { get; set; }
        public string? AdditionalInformation { get; set; }  
        public DateTime CreatedAt { get; set; }
    }
}
