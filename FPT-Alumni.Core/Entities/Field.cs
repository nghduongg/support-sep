﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Entities
{
    public class Field : BaseEntityId
    {
        [StringLength(100)]
        public string FieldName { get; set; }

       
    }
}
