﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Entities
{
    public class Application
    {
        public Guid? CandidateId { get; set; }
        public Guid JobId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string CV { get; set; }
        public string? Note { get; set; }
        public string? RollNumber { get; set; }
        public string? Major { get; set; }
    }
}
