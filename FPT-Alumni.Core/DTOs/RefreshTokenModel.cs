﻿

namespace FPT_Alumni.Core.DTOs
{
    public class RefreshTokenModel
    {
        public string Token { get; set; }
        public DateTime? Expires { get; set; }
    }
}
