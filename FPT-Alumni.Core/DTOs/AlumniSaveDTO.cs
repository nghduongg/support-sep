﻿using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.DTOs
{
    public class AlumniSaveDTO :BaseEntityId
    {
        public string? Avartar { get; set; }
        public string MainEmail { get; set; }
        public string FullName { get; set; }
        public string? PhoneNumber { get; set; }
        public bool IsGraduated { get; set; }
        public string GraduationYear { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public GenderEnum? Gender { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Major { get; set; }
        public string? Class { get; set; }
        public string? StudentCode { get; set; }
        public Guid UserId {  get; set; }
    }
}
