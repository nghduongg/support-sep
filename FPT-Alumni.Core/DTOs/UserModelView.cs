﻿using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Enums;
using FPT_Alumni.Core.JsonModels;
using Newtonsoft.Json;


namespace FPT_Alumni.Core.DTOs
{
    public class UserModelView
    {
        public string? MainEmail { get; set; }

        public string? Password { get; set; }

        public Guid? RoleId { get; set; }

        public string? RoleName { get; set; }

        public string? Avartar { get; set; }

        public string? StudentCode { get; set; }

        public string? PhoneNumber { get; set; }

        public string? Class { get; set; }

        public string? Major { get; set; }

        public bool? IsGraduated { get; set; }

        public string? Job { get; set; }

        public string? Company { get; set; }

        public string? LinkedUrl { get; set; }
        [JsonIgnore]

        public string? WorkExperience { get; set; }

        [JsonIgnore]
        public string? EducationExperience { get; set; }

        [JsonIgnore]
        public string? PrivacySettings { get; set; }

        public string? FullName { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string? GraduationYear { get; set; }

        public GenderEnum Gender { get; set; }
        public string? Country { get; set; }

        public string? City { get; set; }

        public string? Campus { get; set; }


        public Guid UserId { get; set; }


        // Các thuộc tính để giữ các đối tượng đã giải mã từ JSON
        public List<WorkExperience>? WorkExperiences { get; set; }
        public List<EducationExperience>? EducationExperiences { get; set; }
        public PrivacySettings? PrivacySetting { get; set; }

        public void DecodeWorkExperienceJsonFields()
        {
            if (!string.IsNullOrEmpty(WorkExperience))
            {
                WorkExperiences = JsonConvert.DeserializeObject<List<WorkExperience>>(WorkExperience);
            }
        }

        public void DecodeEducationExperienceJsonFields()
        {
            if (!string.IsNullOrEmpty(EducationExperience))
            {
                EducationExperiences = JsonConvert.DeserializeObject<List<EducationExperience>>(EducationExperience);
            }
        }

        public void DecodePrivacySettingsJsonFields()
        {
            if (!string.IsNullOrEmpty(PrivacySettings))
            {
                PrivacySetting = JsonConvert.DeserializeObject<PrivacySettings>(PrivacySettings);
            }
        }

        public string? Status { get; set; }

        public List<Field>? Fields {  get; set; }

    }
}
