﻿using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.DTOs
{
    public class PostDTO
    {
        public Guid Id { get; set; }
        [StringLength(100)]
        public string? Title { get; set; }

        [StringLength(2000)]
        public string? Content { get; set; }
        public bool IsPublic { get; set; }
        public Guid CategoryId { get; set; }
        public int NumberOfComment { get; set; }
        /// <summary>
        /// video and image
        /// </summary>
        public List<MediaDTO>? Medias { get; set; }
        public string? Avartar { get; set; }
        public List<Tag>? Tags { get; set; }
        public List<UserTaggedInPostDTO>? UserTaggedInPostDTOs { get; set; }
        public List<Field> Fields { get; set; }
        public DateTime CreatedDate { get; set; }
        /// <summary>
        /// Created user's full name
        /// </summary>
        public string CreatedBy { get; set; }
        /// <summary>
        ///  special field to check when update/ delete post
        /// </summary>
        public Guid CreatedId { get; set; }
    }
}
