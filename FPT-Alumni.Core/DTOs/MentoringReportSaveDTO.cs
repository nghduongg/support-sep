﻿using Microsoft.AspNetCore.Http;

namespace FPT_Alumni.Core.DTOs
{
    public class MentoringReportSaveDTO
    {
        public Guid ReporterId {  get; set; }
        public Guid VictimId { get; set; }
        public string? Description { get; set; }
        public IFormFile? AdditionalInformation { get; set; }
    }
}
