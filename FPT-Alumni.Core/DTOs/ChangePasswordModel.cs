﻿

namespace FPT_Alumni.Core.DTOs
{
    public class ChangePasswordModel
    {
        public Guid Id { get; set; }
        public string? CurrentPassword { get; set; }

        public string? Password {  get; set; }

        public string? token {  get; set; }
    }
}
