﻿using FPT_Alumni.Core.JsonModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.DTOs
{
    public class MentoringReportDetailsModelView
    {

        public Guid Id { get; set; }
        public Guid ReporterId { get; set; }
        public BasicInformation? ReporterBasicInformationView { get; set; }
        public Guid VictimId { get; set; }

        public BasicInformation? VictimBasicInformationView { get; set; }
        public string? Description { get; set; }
        public string? AdditionalInformation { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
