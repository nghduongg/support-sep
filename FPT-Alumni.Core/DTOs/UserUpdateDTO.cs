﻿using FPT_Alumni.Core.Enums;
using System.ComponentModel.DataAnnotations;


namespace FPT_Alumni.Core.DTOs
{
    public class UserUpdateDTO
    {
        [Required]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Email is required.")]
        [EmailAddress(ErrorMessage = "Wrong email format.")]
        public string Email { get; set; }

        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$",
        ErrorMessage = "Password must be at least 8 characters long and contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character.")]
        public string? Password { get; set; }

        [Required(ErrorMessage = "Status is required.")]
        public UserStatusEnum Status { get; set; }

        [Required(ErrorMessage = "Role is required.")]
        public Guid RoleId { get; set; }
    }
}
