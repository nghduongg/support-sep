﻿
namespace FPT_Alumni.Core.DTOs
{
    public class PrivacySettingsSaveDTO
    {
        public Guid UserId {  get; set; }
        public bool EmailPublic { get; set; } = true;
        public bool PhonePublic { get; set; } = true;
        public bool IsShowWorkExperiece { get; set; } = true;
        public bool IsShowEducationExperiece { get; set; } = true;
        public bool IsShowCompany { get; set; } = true;
        public bool IsShowPhoneNumber { get; set; } = true;
        public bool IsShowStudentCode { get; set; } = true;
        public bool IsShowClass { get; set; } = true;
        public bool IsShowMajor { get; set; } = true;
        public bool IsGraduated { get; set; } = true;
        public bool IsJob { get; set; } = true;
        public bool IsShowLinkedUrl { get; set; } = true;
        public bool IsShowDateOfBirth { get; set; } = true;
        public bool IsShowGraduationYear { get; set; } = true;
        public bool IsShowCountry { get; set; } = true;
        public bool IsShowCity { get; set; } = true;
    }
}
