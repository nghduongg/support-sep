﻿
namespace FPT_Alumni.Core.DTOs
{
    public class TokenModelDTO
    {
        public string? AccessToken { get; set; }

        public string? RefreshToken {  get; set; }
    }
}
