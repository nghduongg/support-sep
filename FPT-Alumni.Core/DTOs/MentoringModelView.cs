﻿using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.JsonModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.DTOs
{
    public class MentoringModelView
    {
        public Guid Id { get; set; }
        public Guid? UserId { get; set; }

        public BasicInformation? BasicInformation { get; set; }

        public string? CurrentCountry { get; set; }
        public string? CurrentCity { get; set; }
        public WorkAndEducation? WorkAndEducation { get; set; }

        public List<Field>? MentoringFields { get; set; }

        public List<ConnectionAndWorking>? ConnectionAndWorkings { get; set; }

        public List<Language>? LanguageSkills { get; set; }
        public string? OtherPreferences { get; set; }

        public string? MentoringRole { get; set; }
    }
}
