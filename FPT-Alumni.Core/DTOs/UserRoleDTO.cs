﻿using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.DTOs
{
    public class UserRoleDTO : BaseEntityId
    {
        /// <summary>
        /// User Id
        /// </summary>

        /// <summary>
        /// Role name
        /// </summary>
        public string Role { get; set; }
    }
}
