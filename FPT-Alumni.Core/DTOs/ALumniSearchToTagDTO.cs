﻿using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.DTOs
{
    public class AlumniSearchToTagDTO : BaseEntityId
    {
        public string? Avartar { get; set; }
        public string? MainEmail { get; set; }
        public string? StudentCode { get; set; }
        public string FullName { get; set; }
    }
}
