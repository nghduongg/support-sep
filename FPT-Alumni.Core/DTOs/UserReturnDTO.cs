﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.DTOs
{
    public class UserReturnDTO
    {
        public Guid Id { get; set; }
        public string? FullName {  get; set; }
        public string? MainEmail {  get; set; }

        public string? EmailLogged { get; set; }
        public Guid? RoleId {  get; set; }
        public string? RoleName {  get; set; }
        public string? Status {  get; set; }
        public string? Password {  get; set; }
    }
}
