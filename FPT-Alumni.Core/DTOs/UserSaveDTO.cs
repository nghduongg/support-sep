﻿using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Enums;
using System.ComponentModel.DataAnnotations;


namespace FPT_Alumni.Core.DTOs
{
    public class UserSaveDTO :BaseEntityId
    {
        [Required(ErrorMessage = "Email must be not empty")]
        public string? Email { get; set; }
        public string? Password { get; set; }
        public string? RefreshToken { get; set; }
        public DateTime? ExperiedDateToken { get; set; }
        public UserStatusEnum Status { get; set; }

        public Guid? RoleId { get; set; }
    }
}
