﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.DTOs
{
    public class MediaInsertDTO
    {
        [StringLength(1000)]
        public string MediaUrl { get; set; }
    }
}
