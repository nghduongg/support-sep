﻿using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.JsonModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.DTOs
{
    public class MentoringSaveDTO
    {
        public Guid Id { get; set; }
        public BasicInformation? BasicInformation { get; set; }
        public string? CurrentCountry { get; set; }
        public string? CurrentCity { get; set; }
        public WorkAndEducation? WorkAndEducation { get; set;}

        public List<Guid>? MentoringFields { get; set; }

        public List<Guid>? Languages { get; set; }

        public List<Guid>? ConnectionAndWorkings { get; set; }

        public string? OtherPreferences {  get; set; }

    }
}
