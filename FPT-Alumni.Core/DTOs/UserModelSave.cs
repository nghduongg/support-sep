﻿using FPT_Alumni.Core.Enums;


namespace FPT_Alumni.Core.DTOs
{
    public class UserModelSave
    {
        public string? Tab { get; set; }
        public string? MainEmail { get; set; }

        public string? Password { get; set; }

        public Guid? RoleId { get; set; }

        public string? RoleName { get; set; }

        public string? Avartar { get; set; }

        public string? StudentCode { get; set; }

        public string? PhoneNumber { get; set; }

        public string? Class { get; set; }

        public string? Major { get; set; }

        public bool? IsGraduated { get; set; }

        public string? Job { get; set; }

        public string? Company { get; set; }

        public string? LinkedUrl { get; set; }

        public string? FullName { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string? GraduationYear { get; set; }

        public GenderEnum? Gender { get; set; }
        public string? Country { get; set; }

        public string? City { get; set; }

        public Guid UserId { get; set; }

        public string? Status { get; set; }

        public string? Campus { get; set; }

        public List<Guid>? FieldIds {  get; set; }
    }
}
