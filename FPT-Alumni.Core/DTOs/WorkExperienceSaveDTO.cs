﻿

namespace FPT_Alumni.Core.DTOs
{
    public class WorkExperienceSaveDTO
    {
        public Guid UserId {  get; set; }
        public string? Company { get; set; }
        public string? Position { get; set; }
        public DateTime? StartDate { get; set; } = DateTime.MinValue;
        public DateTime? EndDate { get; set; } = DateTime.UtcNow;

        public string? Description { get; set; }
    }
}
