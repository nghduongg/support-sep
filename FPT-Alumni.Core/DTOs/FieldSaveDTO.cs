﻿using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.DTOs
{
    public class FieldSaveDTO : BaseEntityId
    {
        [StringLength(100)]
        public string FieldName { get; set; }
    }
}
