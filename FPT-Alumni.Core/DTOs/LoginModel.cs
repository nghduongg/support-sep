﻿

namespace FPT_Alumni.Core.DTOs
{
    public class LoginModel
    {
        public string? EmailLogged {  get; set; }
        public string? Password { get; set; }
    }
}
