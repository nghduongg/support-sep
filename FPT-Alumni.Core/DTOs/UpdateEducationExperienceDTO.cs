﻿using FPT_Alumni.Core.JsonModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.DTOs
{
    public class UpdateEducationExperienceDTO
    {
        public Guid UserId {  get; set; }

        public List<EducationExperience> EducationExperiences { get; set; }
    }
}
