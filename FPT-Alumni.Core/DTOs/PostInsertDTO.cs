﻿using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.DTOs
{
    public class PostInsertDTO
    {
        public PostInsertDTO()
        {
            MediaInsertDTOs = new();
            UserTagedIds = new();
            MajorTagIds = new();
            FiledIds = new();
        }

        [StringLength(100)]
        public string? Title { get; set; }

        [StringLength(2000)]
        public string? Content { get; set; }
        public bool IsPublic { get; set; }

        /// <summary>
        /// post category
        /// </summary>
        [Required(ErrorMessage = "Vui lòng chọn loại bài đăng")]
        public Guid CategoryId { get; set; }

        /// <summary>
        /// video and image
        /// </summary>
        public List<MediaInsertDTO>? MediaInsertDTOs { get; set; }

        /// <summary>
        /// user was tagged
        /// </summary>
        public List<Guid>? UserTagedIds { get; set; }

        /// <summary>
        /// Tag name when tag all of class 
        /// </summary>
        public List<Guid>? MajorTagIds { get; set; }
        public List<Guid> FiledIds { get; set; }

        public Guid? GroupId { get; set; }

    }
}
