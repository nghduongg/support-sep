﻿
namespace FPT_Alumni.Core.DTOs
{
    public class MentoringReportDetails
    {
        public Guid Id { get; set; }
        public Guid ReporterId { get; set; }
        public string? ReporterBasicInformation {  get; set; }
        public Guid VictimId{ get; set; }

        public string? VictimBasicInformation { get; set; }
        public string? Description { get; set; }
        public string? AdditionalInformation { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
