﻿

namespace FPT_Alumni.Core.DTOs
{
    public class ForgotPasswordRequest
    {
        public string? Email {  get; set; }
    }
}
