﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.DTOs
{
    public class MentoringFilterDTO
    {
        public List<Guid>? FieldIds { get; set; }
        public List<Guid>? LanguageIds { get; set; }
        public List<Guid>? ConnectionAndWorkings { get; set; }
    }
}
