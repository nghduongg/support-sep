﻿
using System.ComponentModel.DataAnnotations;


namespace FPT_Alumni.Core.DTOs
{
    public class AlumniImportDTO
    {
        public AlumniImportDTO()
        {
            this.ErrorList = new List<string>();
        }

        public string? StudentCode { get; set; }

        [Required]
        [StringLength(100)]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Email is required.")]
        [EmailAddress(ErrorMessage = "Wrong email format.")]
        [StringLength(100)]
        public string Email { get; set; }

        [StringLength(20)]
        public string? PhoneNumber { get; set; }

        public DateTime DateOfBirth { get; set; }

        [Required]
        [StringLength(50)]
        public string Major { get; set; }

        /// <summary>   
        /// School year
        /// </summary>
        [Required]
        [StringLength(10)]
        public string? Class { get; set; }

        [Required]
        public bool IsGraduated { get; set; }

        [StringLength(5)]
        public string GraduationYear { get; set; }

        /// <summary>
        /// current country
        /// </summary>
        [Required]
        [StringLength(100)]
        public string? Country { get; set; }

        /// <summary>
        /// current city
        /// </summary>
        [Required]
        [StringLength(100)]
        public string? City { get; set; }

        /// <summary>
        /// current job
        /// </summary>
        [Required]
        [StringLength(100)]
        public string Job { get; set; }

        /// <summary>
        /// current company
        /// </summary>
        [Required]
        [StringLength(100)]
        public string Company { get; set; }

        public List<string?> ErrorList { get; set; }
    }
}
