﻿

namespace FPT_Alumni.Core.DTOs
{
    public class TokenResultDTO
    {
        public string AccessToken { get; set; }
        public RefreshTokenModel RefreshToken { get; set; }
    }
}
