﻿using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.DTOs
{
    public class AlumniProfileDTO
    {
        [StringLength(200)]
        public string? Avartar { get; set; }

        public string? StudentCode { get; set; }

        [StringLength(20)]
        public string? PhoneNumber { get; set; }


        [StringLength(20)]
        public string Email { get; set; }

        /// <summary>
        /// School year
        /// </summary>
        [StringLength(10)]
        public string? Class { get; set; }

        [StringLength(50)]
        public string Major { get; set; }

        [Required]
        public bool IsGraduated { get; set; }

        [StringLength(100)]
        public string? Job { get; set; }

        [StringLength(100)]
        public string? Company { get; set; }

        [StringLength(100)]
        public string? LinkedUrl { get; set; }

        /// <summary>
        /// Work Experience with json format
        /// </summary>
        [StringLength(1000)]
        public string? WorkExperience { get; set; }

        /// <summary>
        /// Education Experience with json format
        /// </summary>
        [StringLength(1000)]
        public string? EducationExperience { get; set; }

        [StringLength(1000)]
        public string? PrivacySettings { get; set; }

        [StringLength(100)]
        public string FullName { get; set; }
        [Required]
        public DateTime? DateOfBirth { get; set; }
        public string? GraduationYear { get; set; }

        [StringLength(100)]
        public string Country { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        public List<Field> Fields { get; set; }
    }
}
