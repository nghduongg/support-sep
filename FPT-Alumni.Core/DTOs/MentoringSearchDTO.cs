﻿using FPT_Alumni.Core.JsonModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.DTOs
{
    public class MentoringSearchDTO
    {
        public Guid Id {  get; set; }

        public BasicInformation? BasicInformation { get; set; }

        public WorkAndEducation? WorkAndEducation { get; set; }

        public List<Guid>? MentoringFields {  get; set; }

        public List<Guid>? ConnectionAndWorkings {  get; set; }

        public string? OtherPreferences {  get; set; }

    }
}
