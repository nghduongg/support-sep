﻿using FPT_Alumni.Core.Enums;


namespace FPT_Alumni.Core.DTOs
{
    public class UnverifiedModel
    {
        public string? EmailLogged { get; set; }

        public Guid? RoleId { get; set; }

        public bool? IsExisted { get; set; } = false;

        public string? FullName { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public GenderEnum? Gender {  get; set; }

        public string? PhoneNumber { get; set; }

        public string? StudentCode { get; set; }

        public bool? IsGraduated { get; set; }

        public string? GraduationYear { get; set; }

        public string? Major { get; set; }

        public string? Class {  get; set; }

        public string? Country { get; set; }

        public string? City { get; set; }

        public string? Avatar { get; set; }

        public string? Status {  get; set; }


        public List<Guid> FieldIds { get; set; } = new List<Guid>();

    }
}
