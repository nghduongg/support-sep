﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.DTOs
{
    public class AlumniAndKeyDTO
    {

        public string InvalidKey { get; set; }
        public int FailedNumber { get; set; }

        /// <summary>
        ///  valid alumni to import
        /// </summary>
        public List<AlumniImportDTO> AlumniImportDTOs;

    }
}
