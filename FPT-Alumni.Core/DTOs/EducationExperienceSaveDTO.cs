﻿

namespace FPT_Alumni.Core.DTOs
{
    public class EducationExperienceSaveDTO
    {
        public Guid UserId {  get; set; }
        public string Institution { get; set; }
        public string Major { get; set; }
        public DateTime StartDate { get; set; } = DateTime.MinValue;
        public DateTime EndDate { get; set; } = DateTime.UtcNow;

        public string? Description { get; set; }
    }
}
