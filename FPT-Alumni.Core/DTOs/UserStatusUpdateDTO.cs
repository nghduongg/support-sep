﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.DTOs
{
    public class UserStatusUpdateDTO
    {
        public Guid Id { get; set; }

        public string? Status {  get; set; }
    }
}
