﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.DTOs
{
    public class StaffWithUserModelView
    {
        public string? MainEmail { get; set; }

        public string? Password { get; set; }

        public Guid? RoleId { get; set; }

        public string? RoleName { get; set; }

        public string? Fullname {  get; set; }

        public string? Campus {  get; set; }
        public Guid? UserId { get; set; }

        public string? Status { get; set; }
    }
}
