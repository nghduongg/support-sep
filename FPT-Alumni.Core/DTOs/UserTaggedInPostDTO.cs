﻿using FPT_Alumni.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.DTOs
{
    public class UserTaggedInPostDTO
    {
        public Guid UserId { get; set; }
        public string FullName { get; set; }
    }
}
