﻿

namespace FPT_Alumni.Core.Constants
{
    public static class RoleConstants
    {

        public static readonly Guid ADMIN = new Guid("4421dab0-0e97-4537-af4e-66e107c64205");

        public static readonly Guid ALUMNI = new Guid("3485d189-af4d-4044-b1ce-0260d9ef8fdb");

        public static readonly Guid STAFF = new Guid("1d3acd89-121c-42d7-bc04-aebd911f0d81");

    }
}
