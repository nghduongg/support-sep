﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Constants
{
    public static class PostCategoryConstant
    {
        public static readonly Guid STATUS = new Guid("5C6655CF-C22F-4D8E-85B7-3FB1C9A7E704");
        public static readonly Guid JOB_OFFER = new Guid("BA77FE63-8B6C-48A1-B5A7-0E167B9B5A4D");
        public static readonly Guid MENTORING = new Guid("74DA75C7-C122-4EC0-90AD-40C97A3D5288");
        public static readonly Guid COUPON = new Guid("C5F1070C-5D91-427F-8F5C-ABF6381C9D0C");

    }
}
