﻿

using FPT_Alumni.Core.Enums;

namespace FPT_Alumni.Core.Constants
{
    public class HelperConstants
    {
        public static readonly List<string> Tabs = new List<string>
        {
            "Basic details",
            "Contact details",
            "Work experience",
            "Education experience",
            "Privacy Settings",
            "Account & Password Settings"
        };

        public static List<string> UserStatusList()
        {
            return Enum.GetNames(typeof(UserStatusEnum)).ToList();
        }
    }
}
