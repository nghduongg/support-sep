﻿using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.Interfaces.Services;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace FPT_Alumni.Core.Constants
{
    public static class FirebaseConstants
    {
        private static IFirebaseConfig config = new FirebaseConfig
        {
            AuthSecret = "ntCqUc1igDDljX4vBo4mVfHTSrAsev8XkEFc1Oe8",
            BasePath = "https://fptalumni-e0f8b-default-rtdb.firebaseio.com/",
        };

        public static readonly IFirebaseClient client = new FireSharp.FirebaseClient(config);
    }
}
