﻿using AutoMapper;
using FPT_Alumni.Core.DTOs;
using FPT_Alumni.Core.Entities;
using FPT_Alumni.Core.JsonModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPT_Alumni.Core.Mapper
{
    public class AutomapConfig : Profile
    {
        public AutomapConfig()
        {
            CreateMap<User, UserSaveDTO>().ReverseMap();
            CreateMap<User, UnverifiedModel>().ReverseMap();
            CreateMap<Alumni, UnverifiedModel>().ReverseMap();
            CreateMap<User, UserUpdateDTO>().ReverseMap();
            CreateMap<UserModelSave, UserModelView>().ReverseMap();
            CreateMap<Alumni, UserModelSave>().ReverseMap();
            CreateMap<User, UserModelSave>().ReverseMap();
            CreateMap<Alumni, UserModelView>().ReverseMap();
            CreateMap<WorkExperience, WorkExperienceSaveDTO>().ReverseMap();
            CreateMap<PrivacySettings, PrivacySettingsSaveDTO>().ReverseMap();
            CreateMap<Alumni, WorkExperienceSaveDTO>().ReverseMap();
            CreateMap<Alumni, EducationExperienceSaveDTO>().ReverseMap();
            CreateMap<EducationExperience, EducationExperienceSaveDTO>().ReverseMap();
            CreateMap<User, ChangePasswordModel>().ReverseMap();
            CreateMap<Field, FieldSaveDTO>().ReverseMap();
            CreateMap<Media, MediaInsertDTO>().ReverseMap();
            CreateMap<Post, PostInsertDTO>().ReverseMap();
            CreateMap<Post, PostUpdateDTO>().ReverseMap();
            CreateMap<Alumni, AlumniSaveDTO>().ReverseMap();
            CreateMap<User, AlumniSaveDTO>().ReverseMap();
            CreateMap<User, StaffWithUserModelView>().ReverseMap();
            CreateMap<Staff, StaffWithUserModelView>().ReverseMap();
            CreateMap<Alumni, AlumniSearchToTagDTO>().ReverseMap();
            CreateMap<User, UserReturnDTO>().ReverseMap();
            CreateMap<Staff, UserModelView>().ReverseMap();
            CreateMap<Staff, UserModelSave>().ReverseMap();
            CreateMap<Mentoring, MentoringSaveDTO>().ReverseMap();
            CreateMap<Post, JobDTO>().ReverseMap();
            CreateMap<Job, JobDTO>().ReverseMap();
            CreateMap<MentoringReport, MentoringReportSaveDTO>().ReverseMap();
            CreateMap<MentoringReportDetailsModelView, MentoringReportDetails>().ReverseMap();
        }
    }
}
