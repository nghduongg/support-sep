USE [master]
GO
/****** Object:  Database [FPT_Alumni]    Script Date: 7/3/2024 11:41:20 AM ******/
CREATE DATABASE [FPT_Alumni]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'FPT_Alumni', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\FPT_Alumni.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'FPT_Alumni_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\FPT_Alumni_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [FPT_Alumni] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [FPT_Alumni].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [FPT_Alumni] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [FPT_Alumni] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [FPT_Alumni] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [FPT_Alumni] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [FPT_Alumni] SET ARITHABORT OFF 
GO
ALTER DATABASE [FPT_Alumni] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [FPT_Alumni] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [FPT_Alumni] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [FPT_Alumni] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [FPT_Alumni] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [FPT_Alumni] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [FPT_Alumni] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [FPT_Alumni] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [FPT_Alumni] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [FPT_Alumni] SET  ENABLE_BROKER 
GO
ALTER DATABASE [FPT_Alumni] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [FPT_Alumni] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [FPT_Alumni] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [FPT_Alumni] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [FPT_Alumni] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [FPT_Alumni] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [FPT_Alumni] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [FPT_Alumni] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [FPT_Alumni] SET  MULTI_USER 
GO
ALTER DATABASE [FPT_Alumni] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [FPT_Alumni] SET DB_CHAINING OFF 
GO
ALTER DATABASE [FPT_Alumni] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [FPT_Alumni] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [FPT_Alumni] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [FPT_Alumni] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [FPT_Alumni] SET QUERY_STORE = OFF
GO
USE [FPT_Alumni]
GO
/****** Object:  Table [dbo].[Alumni]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Alumni](
	[Id] [uniqueidentifier] NOT NULL,
	[Avartar] [varchar](200) NULL,
	[StudentCode] [varchar](10) NULL,
	[PhoneNumber] [varchar](20) NULL,
	[Class] [varchar](10) NULL,
	[Major] [varchar](50) NULL,
	[IsGraduated] [bit] NOT NULL,
	[Job] [nvarchar](100) NULL,
	[Company] [nvarchar](100) NULL,
	[LinkedUrl] [nvarchar](200) NULL,
	[WorkExperience] [nvarchar](1000) NULL,
	[EducationExperience] [nvarchar](1000) NULL,
	[PrivacySettings] [nvarchar](500) NULL,
	[FullName] [nvarchar](100) NOT NULL,
	[DateOfBirth] [date] NOT NULL,
	[GraduationYear] [nchar](10) NULL,
	[Gender] [nvarchar](10) NOT NULL,
	[Country] [nvarchar](100) NOT NULL,
	[City] [nvarchar](100) NOT NULL,
	[UserId] [uniqueidentifier] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[ModifiedBy] [uniqueidentifier] NULL,
 CONSTRAINT [PK__Alumni__3214EC07BD4AD3CD] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AlumniField]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AlumniField](
	[UserId] [uniqueidentifier] NOT NULL,
	[FieldId] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[FieldId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Application]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Application](
	[CandidateId] [uniqueidentifier] NOT NULL,
	[JobId] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Email] [nvarchar](100) NULL,
	[Phone] [nvarchar](20) NULL,
	[CV] [nvarchar](1000) NULL,
	[Note] [nvarchar](1000) NULL,
	[RollNumber] [nvarchar](20) NULL,
	[Major] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[CandidateId] ASC,
	[JobId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ConnectionAndWorking]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConnectionAndWorking](
	[Id] [uniqueidentifier] NOT NULL,
	[TypeName] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Event]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Event](
	[Id] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[CoverPhoto] [nvarchar](200) NOT NULL,
	[BackGroundPhoto] [nvarchar](200) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[Location] [nvarchar](200) NOT NULL,
	[Details] [nvarchar](3000) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[ModifiedBy] [uniqueidentifier] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EventAttendance]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventAttendance](
	[Id] [uniqueidentifier] NOT NULL,
	[EventId] [uniqueidentifier] NOT NULL,
	[Participant] [uniqueidentifier] NOT NULL,
	[AttendaceStatus] [bit] NULL,
	[ParticipantRole] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Field]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Field](
	[Id] [uniqueidentifier] NOT NULL,
	[FieldName] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Group]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Group](
	[Id] [uniqueidentifier] NOT NULL,
	[GroupName] [nvarchar](100) NOT NULL,
	[IsPublic] [bit] NOT NULL,
	[Policy] [nvarchar](1000) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Job]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Job](
	[Id] [uniqueidentifier] NOT NULL,
	[Position] [nvarchar](200) NULL,
	[Salary] [nvarchar](100) NULL,
	[Requirement] [nvarchar](2000) NULL,
	[Benefit] [nvarchar](2000) NULL,
	[Deadline] [datetime] NULL,
	[Email] [varchar](100) NULL,
	[PostId] [uniqueidentifier] NULL,
	[FieldId] [uniqueidentifier] NULL,
	[CompanyDescription] [nvarchar](2000) NULL,
	[Address] [nvarchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Language]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Language](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Media]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Media](
	[Id] [uniqueidentifier] NOT NULL,
	[PostId] [uniqueidentifier] NULL,
	[MediaType] [varchar](30) NULL,
	[MediaUrl] [varchar](1000) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Mentoring]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mentoring](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NULL,
	[BasicInformation] [nvarchar](1000) NULL,
	[CurrentCountry] [nvarchar](200) NULL,
	[CurrentCity] [nvarchar](200) NULL,
	[WorkAndEducation] [nvarchar](1000) NULL,
	[OtherPreferences] [nvarchar](1000) NULL,
	[MentoringRole] [nvarchar](50) NULL,
 CONSTRAINT [PK__Mentorin__3214EC07EE0FFD6C] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MentoringConnectionAndWorking]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MentoringConnectionAndWorking](
	[MentoringId] [uniqueidentifier] NOT NULL,
	[ConnectionAndWorkingId] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MentoringId] ASC,
	[ConnectionAndWorkingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MentoringField]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MentoringField](
	[MentoringId] [uniqueidentifier] NOT NULL,
	[FieldId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_MentoringField] PRIMARY KEY CLUSTERED 
(
	[MentoringId] ASC,
	[FieldId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MentoringLanguage]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MentoringLanguage](
	[MentoringId] [uniqueidentifier] NOT NULL,
	[LanguageId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_MentoringLanguage] PRIMARY KEY CLUSTERED 
(
	[MentoringId] ASC,
	[LanguageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MentoringReport]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MentoringReport](
	[Id] [uniqueidentifier] NOT NULL,
	[ReporterId] [uniqueidentifier] NULL,
	[VictimId] [uniqueidentifier] NULL,
	[Description] [nvarchar](max) NULL,
	[AdditionalInformation] [nvarchar](1000) NULL,
	[CreatedAt] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[News]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[News](
	[Id] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[CoverPhoto] [nvarchar](200) NOT NULL,
	[BackGroundPhoto] [nvarchar](200) NOT NULL,
	[PostedDate] [datetime] NOT NULL,
	[Details] [nvarchar](3000) NOT NULL,
	[Status] [bit] NULL,
	[CategoryId] [uniqueidentifier] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[ModifiedBy] [uniqueidentifier] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NewsCategory]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsCategory](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[ModifiedBy] [uniqueidentifier] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Post]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Post](
	[Id] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](100) NULL,
	[Content] [nvarchar](2000) NULL,
	[IsPublic] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[ModifiedBy] [uniqueidentifier] NULL,
	[GroupId] [uniqueidentifier] NULL,
	[CategoryId] [uniqueidentifier] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PostCategory]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PostCategory](
	[Id] [uniqueidentifier] NOT NULL,
	[CategoryName] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PostField]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PostField](
	[PostId] [uniqueidentifier] NOT NULL,
	[FieldId] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PostId] ASC,
	[FieldId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PostTag]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PostTag](
	[PostId] [uniqueidentifier] NOT NULL,
	[TagId] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PostId] ASC,
	[TagId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PostUser]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PostUser](
	[PostId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [uniqueidentifier] NOT NULL,
	[RoleName] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Staff]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Staff](
	[Id] [uniqueidentifier] NOT NULL,
	[FullName] [nvarchar](100) NOT NULL,
	[Campus] [nvarchar](100) NOT NULL,
	[UserId] [uniqueidentifier] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tag]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tag](
	[Id] [uniqueidentifier] NOT NULL,
	[TagName] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [uniqueidentifier] NOT NULL,
	[MainEmail] [varchar](100) NOT NULL,
	[SubEmail] [varchar](100) NULL,
	[EmailLogged] [varchar](100) NULL,
	[Password] [nvarchar](200) NULL,
	[RefreshToken] [varchar](200) NULL,
	[ExperiedDateToken] [datetime] NULL,
	[Status] [varchar](20) NULL,
	[RoleId] [uniqueidentifier] NULL,
 CONSTRAINT [PK__User__3214EC078905E23C] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserGroup]    Script Date: 7/3/2024 11:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserGroup](
	[UserId] [uniqueidentifier] NOT NULL,
	[GroupId] [uniqueidentifier] NOT NULL,
	[IsAdmin] [bit] NULL,
 CONSTRAINT [PK_UserGroup] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[GroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Alumni] ([Id], [Avartar], [StudentCode], [PhoneNumber], [Class], [Major], [IsGraduated], [Job], [Company], [LinkedUrl], [WorkExperience], [EducationExperience], [PrivacySettings], [FullName], [DateOfBirth], [GraduationYear], [Gender], [Country], [City], [UserId], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy]) VALUES (N'63581304-cfab-490f-aa7c-1a76ea22c718', NULL, N'HE160137', NULL, N'K333', N'SE', 0, N'SE FULL COMBO', N'MoDam group', NULL, NULL, NULL, NULL, N'Do Van Chung', CAST(N'2024-11-11' AS Date), NULL, N'2', N'Vietnam', N'Bắc Giang', NULL, CAST(N'2024-06-17T13:55:15.933' AS DateTime), CAST(N'2024-06-19T16:28:40.557' AS DateTime), NULL, NULL)
INSERT [dbo].[Alumni] ([Id], [Avartar], [StudentCode], [PhoneNumber], [Class], [Major], [IsGraduated], [Job], [Company], [LinkedUrl], [WorkExperience], [EducationExperience], [PrivacySettings], [FullName], [DateOfBirth], [GraduationYear], [Gender], [Country], [City], [UserId], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy]) VALUES (N'18bdf8cd-1afb-49a4-83ff-3b0cade429e9', N'ndt.png', N'HE123', N'0123123', N'K15', N'string', 0, N'Dev', N'FPT', N'string', N'[{"Company":"Viettel","Position":"Developer","StartDate":"2022-05-27T00:00:00","EndDate":"2024-04-27T00:00:00"},{"Company":"FPT","Position":"SA","StartDate":"2020-05-28T00:00:00","EndDate":"2024-05-28T00:00:00"}]', N'[{"Institution":"FPT Academy","Major":"CNTT","StartDate":"2024-07-27T00:00:00","EndDate":"2026-05-27T00:00:00"}]', N'{"EmailPublic":true,"PhonePublic":true}', N'NDT', CAST(N'2024-05-27' AS Date), N'string    ', N'1', N'string', N'string', N'ac86972b-4a76-425b-b1d8-00dafcdb9157', CAST(N'2024-05-26T18:46:04.243' AS DateTime), CAST(N'2024-05-28T15:05:16.207' AS DateTime), N'ac86972b-4a76-425b-b1d8-00dafcdb9157', N'ac86972b-4a76-425b-b1d8-00dafcdb9157')
INSERT [dbo].[Alumni] ([Id], [Avartar], [StudentCode], [PhoneNumber], [Class], [Major], [IsGraduated], [Job], [Company], [LinkedUrl], [WorkExperience], [EducationExperience], [PrivacySettings], [FullName], [DateOfBirth], [GraduationYear], [Gender], [Country], [City], [UserId], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy]) VALUES (N'801bbc52-3844-4ec4-80af-5ab505145f31', NULL, N'string', N'string', N'string', N'string', 1, NULL, NULL, NULL, NULL, NULL, N'{"EmailPublic":true,"PhonePublic":true,"IsShowWorkExperiece":true,"IsShowEducationExperiece":true,"IsShowCompany":true,"IsShowPhoneNumber":true,"IsShowStudentCode":true,"IsShowClass":true,"IsShowMajor":true,"IsGraduated":true,"IsJob":true,"IsShowLinkedUrl":true,"IsShowDateOfBirth":true,"IsShowGraduationYear":true,"IsShowCountry":true,"IsShowCity":true}', N'Dinh Son', CAST(N'2001-10-10' AS Date), N'string    ', N'0', N'string', N'string', N'27681e2a-e3c9-4270-8daf-a3e130686da2', CAST(N'2024-07-01T10:36:17.447' AS DateTime), CAST(N'2024-07-01T10:36:17.447' AS DateTime), N'27681e2a-e3c9-4270-8daf-a3e130686da2', N'27681e2a-e3c9-4270-8daf-a3e130686da2')
INSERT [dbo].[Alumni] ([Id], [Avartar], [StudentCode], [PhoneNumber], [Class], [Major], [IsGraduated], [Job], [Company], [LinkedUrl], [WorkExperience], [EducationExperience], [PrivacySettings], [FullName], [DateOfBirth], [GraduationYear], [Gender], [Country], [City], [UserId], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy]) VALUES (N'58584250-8276-4117-82d1-a0d354b9fbb5', N'chung.png', N'HA123', N'0123123123', N'K16', N'CNTT', 0, N'IT', N'Vit teo', NULL, NULL, NULL, NULL, N'VAN CHUNG', CAST(N'2002-12-12' AS Date), N'2021      ', N'1', N'string', N'string', N'3a1be5b9-0e74-4071-b302-dd12ffa0d363', CAST(N'2024-06-11T16:42:46.273' AS DateTime), CAST(N'2024-06-11T16:50:04.827' AS DateTime), N'3a1be5b9-0e74-4071-b302-dd12ffa0d363', N'3a1be5b9-0e74-4071-b302-dd12ffa0d363')
GO
INSERT [dbo].[AlumniField] ([UserId], [FieldId]) VALUES (N'737ca019-d42c-4258-a255-9b885b58e662', N'd320f2cf-d49c-48b7-9809-0f51ebf3d5b5')
INSERT [dbo].[AlumniField] ([UserId], [FieldId]) VALUES (N'3a1be5b9-0e74-4071-b302-dd12ffa0d363', N'3a5d7ef9-1e5c-4db7-9345-0b8f31c06e67')
INSERT [dbo].[AlumniField] ([UserId], [FieldId]) VALUES (N'3a1be5b9-0e74-4071-b302-dd12ffa0d363', N'3fa85f64-5717-4562-b3fc-2c963f66afa6')
GO
INSERT [dbo].[ConnectionAndWorking] ([Id], [TypeName]) VALUES (N'bb7e8a1e-8f0b-4687-bd56-0a1e2c7b8d6e', N'Offline Mentoring')
INSERT [dbo].[ConnectionAndWorking] ([Id], [TypeName]) VALUES (N'1d8d8a7b-d5c3-40b7-953e-1aebcce2a6d3', N'1:1 (One-to-One)')
INSERT [dbo].[ConnectionAndWorking] ([Id], [TypeName]) VALUES (N'56a4d729-7c4b-42a8-9a5f-3d5a7b6e4e3f', N'1:n (Group)')
INSERT [dbo].[ConnectionAndWorking] ([Id], [TypeName]) VALUES (N'0c5f3b4b-9f5a-40d5-85a2-6b3d7c8a4b9d', N'Online Mentoring')
GO
INSERT [dbo].[Field] ([Id], [FieldName]) VALUES (N'3a5d7ef9-1e5c-4db7-9345-0b8f31c06e67', N'Marketing')
INSERT [dbo].[Field] ([Id], [FieldName]) VALUES (N'd320f2cf-d49c-48b7-9809-0f51ebf3d5b5', N'Economics')
INSERT [dbo].[Field] ([Id], [FieldName]) VALUES (N'3fa85f64-5717-4562-b3fc-2c963f66afa6', N'Languages')
INSERT [dbo].[Field] ([Id], [FieldName]) VALUES (N'2fb85f64-5717-4562-b3fc-2c963f66afa6', N'abc')
INSERT [dbo].[Field] ([Id], [FieldName]) VALUES (N'8b8774ac-38a5-4203-99e4-57cb26245450', N'abcd')
INSERT [dbo].[Field] ([Id], [FieldName]) VALUES (N'6f1cfe98-87b2-49b0-a8b1-6b0e0db2d2b9', N'IT')
INSERT [dbo].[Field] ([Id], [FieldName]) VALUES (N'1bb27ad7-5d67-43c8-9ba5-f39463f15e64', N'Business')
GO
INSERT [dbo].[Job] ([Id], [Position], [Salary], [Requirement], [Benefit], [Deadline], [Email], [PostId], [FieldId], [CompanyDescription], [Address]) VALUES (N'c1aa00a8-1aaa-4444-b6a2-f86c90ad71a8', N'Lao công', N'1000', N'không có', N'nhiều', CAST(N'2024-07-01T16:10:44.500' AS DateTime), N'hr@gmail.com', N'515ea602-f34d-477c-a58c-54b4e0331cfb', N'1bb27ad7-5d67-43c8-9ba5-f39463f15e64', N'Công ty tốt lắm', N'Hà Nội')
GO
INSERT [dbo].[Language] ([Id], [Name]) VALUES (N'e19e1d4e-1b21-4a8a-a447-2b8b09364e1b', N'Vietnameses')
INSERT [dbo].[Language] ([Id], [Name]) VALUES (N'8d77b2eb-0b74-4ec9-b23e-4cf7e2f013d7', N'Korean')
INSERT [dbo].[Language] ([Id], [Name]) VALUES (N'7e973b1b-05c7-4e35-9434-5e6e2d3db3e9', N'Chinese')
INSERT [dbo].[Language] ([Id], [Name]) VALUES (N'f6045a6f-2c1d-45b0-9758-6a4c6d2a7e84', N'Japanese')
INSERT [dbo].[Language] ([Id], [Name]) VALUES (N'2f47a66c-65df-4e8b-87ad-8e9b8d46b4d2', N'English')
GO
INSERT [dbo].[Mentoring] ([Id], [UserId], [BasicInformation], [CurrentCountry], [CurrentCity], [WorkAndEducation], [OtherPreferences], [MentoringRole]) VALUES (N'1d173f35-737c-4059-be07-2f31c866444b', N'ac86972b-4a76-425b-b1d8-00dafcdb9157', N'{"Avatar":"vanchung.png","FullName":"Do Van Chunggg","Email":"chungdvhe160136@fpt.edu.vn","Phone":"0123123123","Gender":"1","StudentCode":"HE123","Class":"K15","GeneralIntroduction":"Hi, My name is Chung"}', NULL, NULL, N'{"Experiences":[{"Organisation":"FU","Position":"Student","StartDate":"1/2012","EndDate":"2/2016","PositionSummary":"Student hihi"},{"Organisation":"FS","Position":"Dev","StartDate":"1/2016","EndDate":"2/2017","PositionSummary":"Dev hihi"}],"LinkedInUrl":null,"CVUrl":null,"SupervisoryExperience":null}', N'abc', N'mentee')
INSERT [dbo].[Mentoring] ([Id], [UserId], [BasicInformation], [CurrentCountry], [CurrentCity], [WorkAndEducation], [OtherPreferences], [MentoringRole]) VALUES (N'6890b44f-c26b-4937-8242-321738c28998', N'ac86972b-4a76-425b-b1d8-00dafcdb9157', N'{"Avatar":"truong123.png","FullName":"NDT1","Email":"truongx62001@gmail.com","Phone":"0123457661","Gender":"1","StudentCode":null,"Class":null,"GeneralIntroduction":null}', N'VN', N'HN', N'{"Experiences":[{"Organisation":"FS","Position":"DEV","StartDate":"2015","EndDate":"2017","PositionSummary":null}],"LinkedInUrl":"truongx6.linkedIn","CVUrl":null,"SupervisoryExperience":3}', NULL, N'mentor')
INSERT [dbo].[Mentoring] ([Id], [UserId], [BasicInformation], [CurrentCountry], [CurrentCity], [WorkAndEducation], [OtherPreferences], [MentoringRole]) VALUES (N'c2a44d49-b937-458f-9c38-5c746c2eda4b', N'3a1be5b9-0e74-4071-b302-dd12ffa0d363', N'{"Avatar":"chungcacbon.png","FullName":"Chung Cac Bon","Email":"chungcacbon@gmail.com","Phone":"0441442443","Gender":"1","StudentCode":null,"Class":null,"GeneralIntroduction":null}', NULL, NULL, N'{"Experiences":[{"Organisation":"Viettel","Position":"DEV","StartDate":"2015","EndDate":"Now","PositionSummary":null}],"LinkedInUrl":"chungcacbon.linkedIn","CVUrl":null,"SupervisoryExperience":5}', NULL, N'mentor')
GO
INSERT [dbo].[MentoringConnectionAndWorking] ([MentoringId], [ConnectionAndWorkingId]) VALUES (N'1d173f35-737c-4059-be07-2f31c866444b', N'1d8d8a7b-d5c3-40b7-953e-1aebcce2a6d3')
INSERT [dbo].[MentoringConnectionAndWorking] ([MentoringId], [ConnectionAndWorkingId]) VALUES (N'6890b44f-c26b-4937-8242-321738c28998', N'1d8d8a7b-d5c3-40b7-953e-1aebcce2a6d3')
INSERT [dbo].[MentoringConnectionAndWorking] ([MentoringId], [ConnectionAndWorkingId]) VALUES (N'6890b44f-c26b-4937-8242-321738c28998', N'56a4d729-7c4b-42a8-9a5f-3d5a7b6e4e3f')
INSERT [dbo].[MentoringConnectionAndWorking] ([MentoringId], [ConnectionAndWorkingId]) VALUES (N'c2a44d49-b937-458f-9c38-5c746c2eda4b', N'1d8d8a7b-d5c3-40b7-953e-1aebcce2a6d3')
INSERT [dbo].[MentoringConnectionAndWorking] ([MentoringId], [ConnectionAndWorkingId]) VALUES (N'c2a44d49-b937-458f-9c38-5c746c2eda4b', N'56a4d729-7c4b-42a8-9a5f-3d5a7b6e4e3f')
INSERT [dbo].[MentoringConnectionAndWorking] ([MentoringId], [ConnectionAndWorkingId]) VALUES (N'c2a44d49-b937-458f-9c38-5c746c2eda4b', N'0c5f3b4b-9f5a-40d5-85a2-6b3d7c8a4b9d')
GO
INSERT [dbo].[MentoringField] ([MentoringId], [FieldId]) VALUES (N'1d173f35-737c-4059-be07-2f31c866444b', N'd320f2cf-d49c-48b7-9809-0f51ebf3d5b5')
INSERT [dbo].[MentoringField] ([MentoringId], [FieldId]) VALUES (N'1d173f35-737c-4059-be07-2f31c866444b', N'6f1cfe98-87b2-49b0-a8b1-6b0e0db2d2b9')
INSERT [dbo].[MentoringField] ([MentoringId], [FieldId]) VALUES (N'1d173f35-737c-4059-be07-2f31c866444b', N'1bb27ad7-5d67-43c8-9ba5-f39463f15e64')
INSERT [dbo].[MentoringField] ([MentoringId], [FieldId]) VALUES (N'6890b44f-c26b-4937-8242-321738c28998', N'6f1cfe98-87b2-49b0-a8b1-6b0e0db2d2b9')
INSERT [dbo].[MentoringField] ([MentoringId], [FieldId]) VALUES (N'6890b44f-c26b-4937-8242-321738c28998', N'1bb27ad7-5d67-43c8-9ba5-f39463f15e64')
INSERT [dbo].[MentoringField] ([MentoringId], [FieldId]) VALUES (N'c2a44d49-b937-458f-9c38-5c746c2eda4b', N'd320f2cf-d49c-48b7-9809-0f51ebf3d5b5')
INSERT [dbo].[MentoringField] ([MentoringId], [FieldId]) VALUES (N'c2a44d49-b937-458f-9c38-5c746c2eda4b', N'6f1cfe98-87b2-49b0-a8b1-6b0e0db2d2b9')
GO
INSERT [dbo].[MentoringLanguage] ([MentoringId], [LanguageId]) VALUES (N'6890b44f-c26b-4937-8242-321738c28998', N'8d77b2eb-0b74-4ec9-b23e-4cf7e2f013d7')
INSERT [dbo].[MentoringLanguage] ([MentoringId], [LanguageId]) VALUES (N'6890b44f-c26b-4937-8242-321738c28998', N'7e973b1b-05c7-4e35-9434-5e6e2d3db3e9')
INSERT [dbo].[MentoringLanguage] ([MentoringId], [LanguageId]) VALUES (N'c2a44d49-b937-458f-9c38-5c746c2eda4b', N'7e973b1b-05c7-4e35-9434-5e6e2d3db3e9')
GO
INSERT [dbo].[MentoringReport] ([Id], [ReporterId], [VictimId], [Description], [AdditionalInformation], [CreatedAt]) VALUES (N'fcda0e65-9937-46a1-97c8-057cb9fb824c', N'1d173f35-737c-4059-be07-2f31c866444b', N'6890b44f-c26b-4937-8242-321738c28998', N'scammer', N'scammer.docx', CAST(N'2024-07-03T03:02:37.173' AS DateTime))
GO
INSERT [dbo].[Post] ([Id], [Title], [Content], [IsPublic], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [GroupId], [CategoryId]) VALUES (N'e282fb35-a5ec-4643-98b8-116e1ee51f75', N'son tao va tag truongx', N'son tao va tag truongx', 1, CAST(N'2024-06-04T18:26:27.997' AS DateTime), CAST(N'2024-06-04T18:26:27.997' AS DateTime), N'6a981ab9-1bc4-43a4-9e80-60cd5c11e8a7', N'6a981ab9-1bc4-43a4-9e80-60cd5c11e8a7', NULL, NULL)
INSERT [dbo].[Post] ([Id], [Title], [Content], [IsPublic], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [GroupId], [CategoryId]) VALUES (N'1e5a149d-2d4f-4181-85fe-25206b9dce6a', N'truongx tao va tag son', N'truongx tao va tag son', 1, CAST(N'2024-06-04T18:22:42.063' AS DateTime), CAST(N'2024-06-04T18:22:42.063' AS DateTime), N'ac86972b-4a76-425b-b1d8-00dafcdb9157', N'ac86972b-4a76-425b-b1d8-00dafcdb9157', NULL, NULL)
INSERT [dbo].[Post] ([Id], [Title], [Content], [IsPublic], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [GroupId], [CategoryId]) VALUES (N'515ea602-f34d-477c-a58c-54b4e0331cfb', N'Công ty SDS', N'Quét dọn', 0, CAST(N'2024-07-01T16:14:30.433' AS DateTime), CAST(N'2024-07-01T16:14:30.433' AS DateTime), N'1234abc0-0e97-4537-af4e-66e107c64205', N'1234abc0-0e97-4537-af4e-66e107c64205', NULL, N'3eebb25a-a866-463e-8bdd-c6293a8025b6')
INSERT [dbo].[Post] ([Id], [Title], [Content], [IsPublic], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [GroupId], [CategoryId]) VALUES (N'26efa5e3-5011-4887-83b3-f03981f195c8', N'Công ty SDS', N'Quét dọn', 0, CAST(N'2024-07-01T16:13:58.863' AS DateTime), CAST(N'2024-07-01T16:13:58.863' AS DateTime), N'1234abc0-0e97-4537-af4e-66e107c64205', N'1234abc0-0e97-4537-af4e-66e107c64205', NULL, N'3eebb25a-a866-463e-8bdd-c6293a8025b6')
GO
INSERT [dbo].[PostCategory] ([Id], [CategoryName]) VALUES (N'58555c3c-0c4c-4b63-a829-2ffa2f4a141a', N'Mentoring')
INSERT [dbo].[PostCategory] ([Id], [CategoryName]) VALUES (N'579be26d-6ef1-41c2-9739-5fd14eccada6', N'Coupon')
INSERT [dbo].[PostCategory] ([Id], [CategoryName]) VALUES (N'3eebb25a-a866-463e-8bdd-c6293a8025b6', N'Job')
GO
INSERT [dbo].[Role] ([Id], [RoleName]) VALUES (N'3485d189-af4d-4044-b1ce-0260d9ef8fdb', N'Alumni')
INSERT [dbo].[Role] ([Id], [RoleName]) VALUES (N'ac90deee-39f6-469e-8176-455b5db8267b', N'Guest')
INSERT [dbo].[Role] ([Id], [RoleName]) VALUES (N'4421dab0-0e97-4537-af4e-66e107c64205', N'Admin')
INSERT [dbo].[Role] ([Id], [RoleName]) VALUES (N'1d3acd89-121c-42d7-bc04-aebd911f0d81', N'Staff')
GO
INSERT [dbo].[Staff] ([Id], [FullName], [Campus], [UserId]) VALUES (N'5bc85f64-5717-4562-b3fc-2c963f66afb6', N'Son Nguyen', N'DN', N'6a981ab9-1bc4-43a4-9e80-60cd5c11e8a7')
INSERT [dbo].[Staff] ([Id], [FullName], [Campus], [UserId]) VALUES (N'970105b1-b80a-4597-98b0-bd811c661960', N'ABCD', N'HCMM', N'98d5405a-09c1-459c-8769-43467d4963ae')
GO
INSERT [dbo].[Tag] ([Id], [TagName]) VALUES (N'c638cd32-fe0b-4b6b-ba81-53624f192333', N'IT')
INSERT [dbo].[Tag] ([Id], [TagName]) VALUES (N'694aad52-96aa-402e-89d8-6c89664f1ebe', N'K1')
INSERT [dbo].[Tag] ([Id], [TagName]) VALUES (N'8d6a981a-7112-4bb8-80e5-cee012170161', N'K2')
GO
INSERT [dbo].[User] ([Id], [MainEmail], [SubEmail], [EmailLogged], [Password], [RefreshToken], [ExperiedDateToken], [Status], [RoleId]) VALUES (N'ac86972b-4a76-425b-b1d8-00dafcdb9157', N'truongx62001@gmail.com', N'truongx62001@gmail.com', N'truongx62001@gmail.com', N'$2a$11$3VQ.jp14jhhUBCsbCAcUIuBkOAW19dWrm1BIQckcGmAcQADv04bNe', N'GfwlftUfE2bG3vPjs8RbYUPeiwUlSwatRlhrqJOTHUGmyvcxTfizno9WdYLjZlMX', CAST(N'2024-07-03T03:42:22.897' AS DateTime), N'ACTIVATED', N'3485d189-af4d-4044-b1ce-0260d9ef8fdb')
INSERT [dbo].[User] ([Id], [MainEmail], [SubEmail], [EmailLogged], [Password], [RefreshToken], [ExperiedDateToken], [Status], [RoleId]) VALUES (N'98d5405a-09c1-459c-8769-43467d4963ae', N'abcd123@gmail.com', N'abc123@gmail.com', NULL, N'$2a$11$IgtPNWlGHnYs3CGXpS0e4OMYlu2PDGJTrgMnv7tRI2HocS1wn90Iu', NULL, NULL, N'ACTIVATED', N'1d3acd89-121c-42d7-bc04-aebd911f0d81')
INSERT [dbo].[User] ([Id], [MainEmail], [SubEmail], [EmailLogged], [Password], [RefreshToken], [ExperiedDateToken], [Status], [RoleId]) VALUES (N'6a981ab9-1bc4-43a4-9e80-60cd5c11e8a7', N'sonnguyen2332007@gmail.com', N'sonnguyen2332007@gmail.com', N'sonnguyen2332007@gmail.com', N'$2a$11$3VQ.jp14jhhUBCsbCAcUIuBkOAW19dWrm1BIQckcGmAcQADv04bNe', N'2fc2LuceDIc3C3N8NmRn77GtEsYsPEF6kC2pA5jvSgG2LJqdOUpRFnapycDTdGjv', CAST(N'2024-06-30T13:00:11.423' AS DateTime), N'ACTIVATED', N'1d3acd89-121c-42d7-bc04-aebd911f0d81')
INSERT [dbo].[User] ([Id], [MainEmail], [SubEmail], [EmailLogged], [Password], [RefreshToken], [ExperiedDateToken], [Status], [RoleId]) VALUES (N'1234abc0-0e97-4537-af4e-66e107c64205', N'truongndhe150878@fpt.edu.vn', N'truongndhe150878@fpt.edu.vn', N'truongndhe150878@fpt.edu.vn', N'$2a$11$pad.J3SlzILRFKrlYTIRa.jXMBxHERgOdp0mdn85zUl3tUC2JvJfO', N'2yNzpBOPLFioFZn7I3cpuuaYUjZuyhBu9hAtuSOURViRGAqDi5OZ6EK84UOvUCNt', CAST(N'2024-07-03T04:40:35.677' AS DateTime), N'ACTIVATED', N'4421dab0-0e97-4537-af4e-66e107c64205')
INSERT [dbo].[User] ([Id], [MainEmail], [SubEmail], [EmailLogged], [Password], [RefreshToken], [ExperiedDateToken], [Status], [RoleId]) VALUES (N'737ca019-d42c-4258-a255-9b885b58e662', N'chungdvhe160136@fpt.edu.vn', N'chungdvhe160136@fpt.edu.vn', N'chungdvhe160136@fpt.edu.vn', N'$2a$12$brSdw4kqhA.dq4mz9wufDOLt7SUqzuOyvt/Rkx5rwYzJNhgY0yh.i', N'GWkaW4XIsRS28J0tmfUpKCr6Z3arpnW0xhzJlF3LX1vkDGUh4KbMs3R1QIbzJkNy', CAST(N'2024-06-30T12:11:04.783' AS DateTime), N'ACTIVATED', N'3485d189-af4d-4044-b1ce-0260d9ef8fdb')
INSERT [dbo].[User] ([Id], [MainEmail], [SubEmail], [EmailLogged], [Password], [RefreshToken], [ExperiedDateToken], [Status], [RoleId]) VALUES (N'27681e2a-e3c9-4270-8daf-a3e130686da2', N'dinhson1032001@gmail.com', N'dinhson1032001@gmail.com', N'dinhson1032001@gmail.com', N'$2a$11$Up1CJHs6BoMYnUetFdTvhOVdieMA5c.EaayHf2N/LVGTtRdxZ.fHe', NULL, NULL, N'UNAUTHORIZE', N'3485d189-af4d-4044-b1ce-0260d9ef8fdb')
INSERT [dbo].[User] ([Id], [MainEmail], [SubEmail], [EmailLogged], [Password], [RefreshToken], [ExperiedDateToken], [Status], [RoleId]) VALUES (N'3a1be5b9-0e74-4071-b302-dd12ffa0d363', N'chungcacbon@gmail.com', NULL, N'chungcacbon@gmail.com', N'$2a$11$OIjKoQMPbeKqckDbVqAOSe/LtqQlKwOGGEq2qOxRmf00/NIQOzcnO', N'iQwpeuTyLW76YXfJYeSSVjVEyJyoKQw3bCrvtWJavc3SZpsWx8PGRLkNILEpnnJK', CAST(N'2024-06-30T12:30:25.453' AS DateTime), N'ACTIVATED', N'3485d189-af4d-4044-b1ce-0260d9ef8fdb')
GO
ALTER TABLE [dbo].[EventAttendance] ADD  DEFAULT ((0)) FOR [AttendaceStatus]
GO
ALTER TABLE [dbo].[EventAttendance] ADD  DEFAULT ('PARTICIPANT') FOR [ParticipantRole]
GO
ALTER TABLE [dbo].[News] ADD  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[Role] ADD  DEFAULT ('GUEST') FOR [RoleName]
GO
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF__User__Status__398D8EEE]  DEFAULT ('UNAUTHORIZED') FOR [Status]
GO
ALTER TABLE [dbo].[Alumni]  WITH CHECK ADD  CONSTRAINT [FK__Alumni__CreatedB__3D5E1FD2] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Alumni] CHECK CONSTRAINT [FK__Alumni__CreatedB__3D5E1FD2]
GO
ALTER TABLE [dbo].[Alumni]  WITH CHECK ADD  CONSTRAINT [FK__Alumni__Modified__3E52440B] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Alumni] CHECK CONSTRAINT [FK__Alumni__Modified__3E52440B]
GO
ALTER TABLE [dbo].[Alumni]  WITH CHECK ADD  CONSTRAINT [FK__Alumni__UserId__3F466844] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Alumni] CHECK CONSTRAINT [FK__Alumni__UserId__3F466844]
GO
ALTER TABLE [dbo].[AlumniField]  WITH CHECK ADD FOREIGN KEY([FieldId])
REFERENCES [dbo].[Field] ([Id])
GO
ALTER TABLE [dbo].[AlumniField]  WITH CHECK ADD  CONSTRAINT [FK_AlumniField_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[AlumniField] CHECK CONSTRAINT [FK_AlumniField_User]
GO
ALTER TABLE [dbo].[Application]  WITH CHECK ADD FOREIGN KEY([CandidateId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Application]  WITH CHECK ADD FOREIGN KEY([JobId])
REFERENCES [dbo].[Job] ([Id])
GO
ALTER TABLE [dbo].[Event]  WITH CHECK ADD  CONSTRAINT [FK__Event__ModifiedB__45F365D3] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Event] CHECK CONSTRAINT [FK__Event__ModifiedB__45F365D3]
GO
ALTER TABLE [dbo].[EventAttendance]  WITH CHECK ADD FOREIGN KEY([EventId])
REFERENCES [dbo].[Event] ([Id])
GO
ALTER TABLE [dbo].[EventAttendance]  WITH CHECK ADD  CONSTRAINT [FK__EventAtte__Parti__49C3F6B7] FOREIGN KEY([Participant])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[EventAttendance] CHECK CONSTRAINT [FK__EventAtte__Parti__49C3F6B7]
GO
ALTER TABLE [dbo].[Job]  WITH CHECK ADD FOREIGN KEY([PostId])
REFERENCES [dbo].[Post] ([Id])
GO
ALTER TABLE [dbo].[Media]  WITH CHECK ADD FOREIGN KEY([PostId])
REFERENCES [dbo].[Post] ([Id])
GO
ALTER TABLE [dbo].[Mentoring]  WITH CHECK ADD  CONSTRAINT [FK__Mentoring__UserI__6477ECF3] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Mentoring] CHECK CONSTRAINT [FK__Mentoring__UserI__6477ECF3]
GO
ALTER TABLE [dbo].[MentoringConnectionAndWorking]  WITH CHECK ADD FOREIGN KEY([ConnectionAndWorkingId])
REFERENCES [dbo].[ConnectionAndWorking] ([Id])
GO
ALTER TABLE [dbo].[MentoringConnectionAndWorking]  WITH CHECK ADD FOREIGN KEY([MentoringId])
REFERENCES [dbo].[Mentoring] ([Id])
GO
ALTER TABLE [dbo].[MentoringField]  WITH CHECK ADD  CONSTRAINT [FK__Mentoring__Field__29221CFB] FOREIGN KEY([FieldId])
REFERENCES [dbo].[Field] ([Id])
GO
ALTER TABLE [dbo].[MentoringField] CHECK CONSTRAINT [FK__Mentoring__Field__29221CFB]
GO
ALTER TABLE [dbo].[MentoringField]  WITH CHECK ADD  CONSTRAINT [FK__Mentoring__Mento__282DF8C2] FOREIGN KEY([MentoringId])
REFERENCES [dbo].[Mentoring] ([Id])
GO
ALTER TABLE [dbo].[MentoringField] CHECK CONSTRAINT [FK__Mentoring__Mento__282DF8C2]
GO
ALTER TABLE [dbo].[MentoringLanguage]  WITH CHECK ADD  CONSTRAINT [FK__Mentoring__Langu__690797E6] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Language] ([Id])
GO
ALTER TABLE [dbo].[MentoringLanguage] CHECK CONSTRAINT [FK__Mentoring__Langu__690797E6]
GO
ALTER TABLE [dbo].[MentoringLanguage]  WITH CHECK ADD  CONSTRAINT [FK_MentoringLanguage_MentoringProfile] FOREIGN KEY([MentoringId])
REFERENCES [dbo].[Mentoring] ([Id])
GO
ALTER TABLE [dbo].[MentoringLanguage] CHECK CONSTRAINT [FK_MentoringLanguage_MentoringProfile]
GO
ALTER TABLE [dbo].[MentoringReport]  WITH CHECK ADD FOREIGN KEY([VictimId])
REFERENCES [dbo].[Mentoring] ([Id])
GO
ALTER TABLE [dbo].[MentoringReport]  WITH CHECK ADD  CONSTRAINT [FK_MentoringReport_Mentoring] FOREIGN KEY([ReporterId])
REFERENCES [dbo].[Mentoring] ([Id])
GO
ALTER TABLE [dbo].[MentoringReport] CHECK CONSTRAINT [FK_MentoringReport_Mentoring]
GO
ALTER TABLE [dbo].[News]  WITH CHECK ADD FOREIGN KEY([CategoryId])
REFERENCES [dbo].[NewsCategory] ([Id])
GO
ALTER TABLE [dbo].[News]  WITH CHECK ADD  CONSTRAINT [FK__News__CreatedBy__5441852A] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[News] CHECK CONSTRAINT [FK__News__CreatedBy__5441852A]
GO
ALTER TABLE [dbo].[News]  WITH CHECK ADD  CONSTRAINT [FK__News__ModifiedBy__5535A963] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[News] CHECK CONSTRAINT [FK__News__ModifiedBy__5535A963]
GO
ALTER TABLE [dbo].[NewsCategory]  WITH CHECK ADD  CONSTRAINT [FK__NewsCateg__Creat__4E88ABD4] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[NewsCategory] CHECK CONSTRAINT [FK__NewsCateg__Creat__4E88ABD4]
GO
ALTER TABLE [dbo].[NewsCategory]  WITH CHECK ADD  CONSTRAINT [FK__NewsCateg__Modif__4F7CD00D] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[NewsCategory] CHECK CONSTRAINT [FK__NewsCateg__Modif__4F7CD00D]
GO
ALTER TABLE [dbo].[Post]  WITH CHECK ADD FOREIGN KEY([GroupId])
REFERENCES [dbo].[Group] ([Id])
GO
ALTER TABLE [dbo].[Post]  WITH CHECK ADD  CONSTRAINT [FK_Post_PostCategory] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[PostCategory] ([Id])
GO
ALTER TABLE [dbo].[Post] CHECK CONSTRAINT [FK_Post_PostCategory]
GO
ALTER TABLE [dbo].[Post]  WITH CHECK ADD  CONSTRAINT [FK_Post_User] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Post] CHECK CONSTRAINT [FK_Post_User]
GO
ALTER TABLE [dbo].[Post]  WITH CHECK ADD  CONSTRAINT [FK_Post_User1] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Post] CHECK CONSTRAINT [FK_Post_User1]
GO
ALTER TABLE [dbo].[PostField]  WITH CHECK ADD FOREIGN KEY([FieldId])
REFERENCES [dbo].[Field] ([Id])
GO
ALTER TABLE [dbo].[PostField]  WITH CHECK ADD FOREIGN KEY([PostId])
REFERENCES [dbo].[Post] ([Id])
GO
ALTER TABLE [dbo].[PostTag]  WITH CHECK ADD FOREIGN KEY([PostId])
REFERENCES [dbo].[Post] ([Id])
GO
ALTER TABLE [dbo].[PostTag]  WITH CHECK ADD FOREIGN KEY([TagId])
REFERENCES [dbo].[Tag] ([Id])
GO
ALTER TABLE [dbo].[PostUser]  WITH CHECK ADD FOREIGN KEY([PostId])
REFERENCES [dbo].[Post] ([Id])
GO
ALTER TABLE [dbo].[PostUser]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Staff]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK__User__RoleId__3A81B327] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK__User__RoleId__3A81B327]
GO
ALTER TABLE [dbo].[UserGroup]  WITH CHECK ADD  CONSTRAINT [FK__UserGroup__Group__73BA3083] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Group] ([Id])
GO
ALTER TABLE [dbo].[UserGroup] CHECK CONSTRAINT [FK__UserGroup__Group__73BA3083]
GO
ALTER TABLE [dbo].[UserGroup]  WITH CHECK ADD  CONSTRAINT [FK__UserGroup__UserI__74AE54BC] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[UserGroup] CHECK CONSTRAINT [FK__UserGroup__UserI__74AE54BC]
GO
USE [master]
GO
ALTER DATABASE [FPT_Alumni] SET  READ_WRITE 
GO
